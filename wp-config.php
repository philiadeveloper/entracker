<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Entracker');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tEM]o4@Q<wK&Oy4=Nm9gu~f(bP~S%p|Vngm(R/2!LiTTIBcPu{jsHegs+,DQi@uo');
define('SECURE_AUTH_KEY',  'm$&7cV1%,OD<WexHkixd~QTg} kA45?ZBmH5H]0x[N&:.vqh--r3xm J4`!WBr^2');
define('LOGGED_IN_KEY',    'dVV0zzu-JFokK%Ot`zaS*Psal;fGBGRqwf2a8uZv-CI~[q?KmnHr rN@-tWS$%<K');
define('NONCE_KEY',        '69.?TtS(gx)ES$BfECF-<crr+3iMr5v~ERT=4`rzg@/M,c3<:<)J7VH0aPjr6b,e');
define('AUTH_SALT',        'H[?H,Vin,gDnj7}D(baY9{v0cJS/J56?XmeGj:}|j[:AQfi3FN1Hj]C(8IZlm+w@');
define('SECURE_AUTH_SALT', ':z.(NC2vQph,Gnt4qCG%QtoV$3?>C:u^PXC38cnrL!wt8op/uD^g~};HvpKI5DIb');
define('LOGGED_IN_SALT',   '$?(I~wX{nS8}Q-#a>9oc0QXY9N}plx<)m=].K.z6Ge!vyc!u4(}4TXagtRn*U$}K');
define('NONCE_SALT',       'jN=MMO %&xDBWI<^PjhhG,k*I]XPKvQ|;eLF$3qx/*+<%zF~s;cHO~JO}$Zz@...');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
