<?php
ob_start();/*
Plugin Name: Entracker Setup
Description: Entracker Setup
Author: (Phillia Solutions)
Version: 0.1
*/
add_action('admin_menu', 'test_plugin_setup_menu');
function test_plugin_setup_menu(){
add_menu_page( 'Entracker Setup', 'Entracker Setup', 'manage_options', 'philia-entracker', 'philia_entracker' );
	//}
}
function philia_entracker(){ ?>
<?php
global $wpdb;
$settings = $wpdb->get_results("SELECT * FROM entracker_config WHERE id=1",ARRAY_A);
?>
<div class="wrap">
	<?php if(isset($_SESSION['success'])){ ?>
	<div class="updated notice">
		<p>Slider Inserted Successfully.</p>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION['error'])){ ?>
	<div class="error notice">
		<p>Empty Fields.</p>
	</div>
	<?php } ?>
	<h1 class="wp-heading-inline">Entracker Website Setup</h1>
	<hr class="wp-header-end">
	<form id="posts-filter" method="POST" action="" enctype="multipart/form-data">
		<p class="search-box" style="margin-bottom: 10px;">
			<input type="submit" name="entracker_setup_top_settings" id="search-submit" class="button button-primary button-large" value="Submit">
		</p>
		<br>
		<br>
		<br>
		<h3>Top Slider Settings</h3>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th width="20%" scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Title</th>
					<th scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Fields</th>
				</tr>
			</thead>
			<tbody id="the-list" class="ui-sortable">
				<tr>
					<td>Top Slider</td>
					<td>
						<select name="top_slider_type" id="top_slider_type">
							<option <?php echo ($settings[0]['top_slider_type']=='Latest Posts')?'selected':''?>>Latest Posts</option>
							<option <?php echo ($settings[0]['top_slider_type']=='Category Wise')?'selected':''?>>Category Wise</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Categories</td>
					<td>
						<select name="top_slider_category" id="top_slider_category" <?php echo ($settings[0]['top_slider_type']=='Category Wise')?'':'disabled="disabled"'?>>
							<?php
							$cats = get_categories();
							foreach ($cats as $cat) {
							?>
							<option <?php echo (($settings[0]['top_slider_type']=='Category Wise') AND $settings[0]['top_slider_category']==$cat->term_id)?'selected':''?> value="<?php echo $cat->term_id;?>"><?php echo $cat->name;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Top Slider (Limit)</td>
					<td>
						<input value="<?php echo ($settings[0]['top_slider_limit']!='')?$settings[0]['top_slider_limit']:'3';?>" type="text" name="top_slider_limit" size="30" autocomplete="off">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<form id="posts-filter" method="POST" action="" enctype="multipart/form-data" style="margin-top: 20px;">
		<p class="search-box" style="margin-bottom: 10px;">
			<input type="submit" name="entracker_setup_ads_settings" id="search-submit" class="button button-primary button-large" value="Submit">
		</p>
		<h3>Ads Settings</h3>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th width="20%" scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Title</th>
					<th scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Fields</th>
				</tr>
			</thead>
			<tbody id="the-list" class="ui-sortable">
				<tr>
					<td>Ads Enable/Disable</td>
					<td>
						<select name="pAdsStatus" id="pAdsStatus">
							<option value="1" <?php echo ($settings[0]['pAdsStatus']==1)?'selected':''?>>Enable</option>
							<option value="0" <?php echo ($settings[0]['pAdsStatus']==0)?'selected':''?>>Disable</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Adv/Banner Code (Head)</td>
					<td>
						<textarea name="add_banner_code_head" style="width: 100%;height:100px;"><?php echo ($settings[0]['add_banner_code_head']!='')?$settings[0]['add_banner_code_head']:'';?></textarea>
					</td>
				</tr>
				<tr>
					<td>Adv/Banner Code (Body)(Header) 1</td>
					<td>
						<textarea name="add_banner_code_body1" style="width: 100%;height:100px;"><?php echo ($settings[0]['add_banner_code_body1']!='')?$settings[0]['add_banner_code_body1']:'';?></textarea>
					</td>
				</tr>
				<tr>
					<td>Adv/Banner Code (Body)(Middle) 2</td>
					<td>
						<textarea name="add_banner_code_body2" style="width: 100%;height:100px;"><?php echo ($settings[0]['add_banner_code_body2']!='')?$settings[0]['add_banner_code_body2']:'';?></textarea>
					</td>
				</tr>
				<tr>
					<td>Repeat Middle Adv After n Posts</td>
					<td>
						<input value="<?php echo ($settings[0]['repeat_middle_adv']!='')?$settings[0]['repeat_middle_adv']:'5';?>" type="text" name="repeat_middle_adv" size="30" autocomplete="off">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<form id="posts-filter" method="POST" action="" enctype="multipart/form-data" style="margin-top: 20px;">
		<p class="search-box" style="margin-bottom: 10px;">
			<input type="submit" name="entracker_setup_center_box_settings" id="search-submit" class="button button-primary button-large" value="Submit">
		</p>
		<h3>Center Box</h3>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th width="20%" scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Title</th>
					<th scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Fields</th>
				</tr>
			</thead>
			<tbody id="the-list" class="ui-sortable">
				<tr>
					<td>Categories</td>
					<td>
						<select name="center_box_category">
							<?php
							$cats = get_categories();
							foreach ($cats as $cat) {
							?>
							<option <?php echo ($settings[0]['center_box_category']==$cat->term_id)?'selected':''?> value="<?php echo $cat->term_id;?>"><?php echo $cat->name;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Show After n Posts</td>
					<td>
						<input value="<?php echo ($settings[0]['show_after_n_post']!='')?$settings[0]['show_after_n_post']:'5';?>" type="text" name="show_after_n_post" size="30" autocomplete="off">
					</td>
				</tr>
				<tr>
					<td>Post Count (Limit)</td>
					<td>
						<input value="<?php echo ($settings[0]['center_box_post_count']!='')?$settings[0]['center_box_post_count']:'3';?>" type="text" name="center_box_post_count" size="30" autocomplete="off">
					</td>
				</tr>
				<tr>
					<td>Bootstrap Columns (Seprated By Comma)</td>
					<td>
						<input value="<?php echo ($settings[0]['center_box_columns']!='')?$settings[0]['center_box_columns']:'6,6,12';?>" type="text" name="center_box_columns" size="30" autocomplete="off">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<form id="posts-filter" method="POST" action="" enctype="multipart/form-data" style="margin-top: 20px;">
		<p class="search-box" style="margin-bottom: 10px;">
			<input type="submit" name="entracker_setup_generel_settings" id="search-submit" class="button button-primary button-large" value="Submit">
		</p>
		<h3>General Settings</h3>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th width="20%" scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Title</th>
					<th scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Fields</th>
				</tr>
			</thead>
			<tbody id="the-list" class="ui-sortable">
				<tr>
					<td>Front Page Post Count (Limit)</td>
					<td>
						<input value="<?php echo ($settings[0]['front_page_post_count']!='')?$settings[0]['front_page_post_count']:'3';?>" type="text" name="front_page_post_count" size="30" autocomplete="off">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<form id="posts-filter" method="POST" action="" enctype="multipart/form-data" style="margin-top: 20px;">
		<p class="search-box" style="margin-bottom: 10px;">
			<input type="submit" name="entracker_setup_snippets_settings" id="search-submit" class="button button-primary button-large" value="Submit">
		</p>
		<h3>Snippets</h3>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th width="20%" scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Title</th>
					<th scope="col" id="sh_post_thumbs" class="manage-column column-sh_post_thumbs">Fields</th>
				</tr>
			</thead>
			<tbody id="the-list" class="ui-sortable">
				
				<tr>
					<td>Categories</td>
					<td>
						<select name="snippets_box_category" id="snippets_box_category">
							<?php
							$cats = get_categories();
							foreach ($cats as $cat) {
							?>
							<option <?php echo ($settings[0]['snippets_box_category']==$cat->term_id)?'selected':''?> value="<?php echo $cat->term_id;?>"><?php echo $cat->name;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Adv/Banner Code</td>
					<td>
						<textarea name="add_banner_code_snippet" style="width: 100%;height:100px;"><?php echo ($settings[0]['add_banner_code_snippet']!='')?$settings[0]['add_banner_code_snippet']:'';?></textarea>
					</td>
				</tr>
				<tr>
					<td>Repeat Adv After n Posts</td>
					<td>
						<input value="<?php echo ($settings[0]['snippets_ads_repeat']!='')?$settings[0]['snippets_ads_repeat']:'5';?>" type="text" name="snippets_ads_repeat" size="30" autocomplete="off">
					</td>
				</tr>
				<tr>
					<td>Post Count (Limit)</td>
					<td>
						<input value="<?php echo ($settings[0]['snippets_box_post_count']!='')?$settings[0]['snippets_box_post_count']:'3';?>" type="text" name="snippets_box_post_count" size="30" autocomplete="off">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<script type="text/javascript">
jQuery(document).on('change', '#top_slider_type', function () {
	var value = jQuery(this).val();
		if(value=="Category Wise"){
		jQuery("#top_slider_category").prop("disabled",false);
		}else{
		jQuery("#top_slider_category").prop('disabled', true);
		}
});
</script>
<?php
if(isset($_POST['entracker_setup_top_settings'])){
global $wpdb;
$wpdb->query("UPDATE entracker_config SET
top_slider_type = '".$_POST['top_slider_type']."',
top_slider_category = '".$_POST['top_slider_category']."',
top_slider_limit = '".$_POST['top_slider_limit']."' WHERE id=1
");
$_SESSION['success'] = "success";
echo "<script>window.location.replace('admin.php?page=philia-entracker');</script>";
}else{
$_SESSION['error'] = "error";
}
if(isset($_POST['entracker_setup_ads_settings'])){
global $wpdb;
$wpdb->query("UPDATE entracker_config SET
pAdsStatus = '".$_POST['pAdsStatus']."',
add_banner_code_head = '".$_POST['add_banner_code_head']."',
add_banner_code_body1 = '".$_POST['add_banner_code_body1']."',
add_banner_code_body2 = '".$_POST['add_banner_code_body2']."',
repeat_middle_adv = '".$_POST['repeat_middle_adv']."'
WHERE id=1
");
$_SESSION['success'] = "success";
echo "<script>window.location.replace('admin.php?page=philia-entracker');</script>";
}else{
$_SESSION['error'] = "error";
}
if(isset($_POST['entracker_setup_center_box_settings'])){
global $wpdb;
$wpdb->query("UPDATE entracker_config SET
center_box_category = '".$_POST['center_box_category']."',
center_box_post_count = '".$_POST['center_box_post_count']."',
show_after_n_post = '".$_POST['show_after_n_post']."',
center_box_columns = '".$_POST['center_box_columns']."'
WHERE id=1
");
$_SESSION['success'] = "success";
echo "<script>window.location.replace('admin.php?page=philia-entracker');</script>";
}else{
$_SESSION['error'] = "error";
}
if(isset($_POST['entracker_setup_generel_settings'])){
global $wpdb;
$wpdb->query("UPDATE entracker_config SET
front_page_post_count = '".$_POST['front_page_post_count']."'
WHERE id=1
");
$_SESSION['success'] = "success";
echo "<script>window.location.replace('admin.php?page=philia-entracker');</script>";
}else{
$_SESSION['error'] = "error";
}
if(isset($_POST['entracker_setup_snippets_settings'])){
global $wpdb;
$wpdb->query("UPDATE entracker_config SET
snippets_box_category = '".$_POST['snippets_box_category']."',
add_banner_code_snippet = '".$_POST['add_banner_code_snippet']."',
snippets_ads_repeat = '".$_POST['snippets_ads_repeat']."',
snippets_box_post_count = '".$_POST['snippets_box_post_count']."'
WHERE id=1
");
$_SESSION['success'] = "success";
echo "<script>window.location.replace('admin.php?page=philia-entracker');</script>";
}else{
$_SESSION['error'] = "error";
}
}
ob_end_flush();
?>