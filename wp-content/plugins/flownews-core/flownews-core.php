<?php
/*
Plugin Name: Flownews Core Functions
Plugin URI: http://themeforest.net/user/ad-theme/portfolio/?ref=ad-theme
Description: Flownews Core
Author: AD-THEME
Version: 1.1
Author URI: http://themeforest.net/user/ad-theme/portfolio/?ref=ad-theme
*/

// Basic plugin definitions 
define ('ADT_FC_PLG_NAME', 'flownews-core');
define( 'ADT_FC_PLG_VERSION', '1.1' );
define( 'ADT_FC_URL', WP_PLUGIN_URL . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));
define( 'ADT_FC_DIR', WP_PLUGIN_DIR . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));

// LANGUAGE
add_action('init', 'flownews_core_localization_init');
function flownews_core_localization_init() {
    $path = dirname(plugin_basename( __FILE__ )) . '/languages/';
    $loaded = load_plugin_textdomain( 'flownews-core', false, $path);
}

if ( class_exists( 'Redux' ) ) {
	// Replace {$redux_opt_name} with your opt_name.
	// Also be sure to change this function name!
	if(!function_exists('redux_register_custom_extension_loader')) :
		function redux_register_custom_extension_loader($ReduxFramework) {
			$path    = dirname( __FILE__ ) . '/extensions/';
				$folders = scandir( $path, 1 );
				foreach ( $folders as $folder ) {
					if ( $folder === '.' or $folder === '..' or ! is_dir( $path . $folder ) ) {
						continue;
					}
					$extension_class = 'ReduxFramework_Extension_' . $folder;
					if ( ! class_exists( $extension_class ) ) {
						// In case you wanted override your override, hah.
						$class_file = $path . $folder . '/extension_' . $folder . '.php';
						$class_file = apply_filters( 'redux/extension/' . $ReduxFramework->args['opt_name'] . '/' . $folder, $class_file );
						if ( $class_file ) {
							require_once( $class_file );
						}
					}
					if ( ! isset( $ReduxFramework->extensions[ $folder ] ) ) {
						$ReduxFramework->extensions[ $folder ] = new $extension_class( $ReduxFramework );
					}
				}
		}
		// Modify {$redux_opt_name} to match your opt_name
		add_action("redux/extensions/flownews_theme/before", 'redux_register_custom_extension_loader', 0);
	endif;
	
	require_once(ADT_FC_DIR . '/extensions/import-one-click.php');

}

# Load Visual Composer Addons
require_once(ADT_FC_DIR . '/vc/vc_functions.php');
require_once(ADT_FC_DIR . '/vc/class/header.php');
require_once(ADT_FC_DIR . '/vc/function/header.php');
require_once(ADT_FC_DIR . '/vc/class/posts.php');
require_once(ADT_FC_DIR . '/vc/function/posts.php');
require_once(ADT_FC_DIR . '/vc/class/posts_carousel.php');
require_once(ADT_FC_DIR . '/vc/function/posts_carousel.php');
require_once(ADT_FC_DIR . '/vc/class/posts_tab.php');
require_once(ADT_FC_DIR . '/vc/function/posts_tab.php');
require_once(ADT_FC_DIR . '/vc/class/newsticker.php');
require_once(ADT_FC_DIR . '/vc/function/newsticker.php');

# Load Shortcode
require_once(ADT_FC_DIR . '/shortcodes/shortcodes-functions.php');

?>