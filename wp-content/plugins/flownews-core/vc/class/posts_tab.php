<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */ 
 
class flownews_posts_tab_display_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_flownews_posts_tab_displayWithVC' ) );
        add_shortcode( 'flownews_posts_tab_display', array( $this, 'flownews_posts_tab_display_function' ) );	
    }
 
    public function integrate_flownews_posts_tab_displayWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		$categories = get_categories(array('orderby' => 'name','order' => 'ASC'));
		$categories_lists = array();
		$i = 0;
		foreach ($categories as $category) { 
			$categories_lists[$category->name] = $category->slug;
			$i++;
		}
		vc_map( array(
            "name" => esc_html__("Tab Posts", 'flownews-core'),
            "description" => esc_html__("Display your tabs posts", 'flownews-core'),
            "base" => "flownews_posts_tab_display",
            "class" => "flownews",
            "controls" => "full",
            "icon" => FLOWNEWS_IMG_URL . 'vc_icon.png',
            "category" => esc_html__('FlowNews', 'flownews-core'),
            "params" => array(						
				array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__("Posts Tab Name",'flownews-core'),
						"param_name" => "name",
						"admin_label" => true,				  				  			  				  
				),		
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Date Format", 'flownews-core'),
                  "param_name" => "date_format",
                  "value" => array(
									esc_html__('November 6, 2010','flownews-core')  					=> 'F j, Y',
				  					esc_html__('November 6, 2010 12:50 am','flownews-core') 			=> 'F j, Y g:i a',
				  					esc_html__('November, 2010','flownews-core') 					=> 'F, Y',
									esc_html__('12:50 am','flownews-core')  							=> 'g:i a',
				  					esc_html__('12:50:48 am','flownews-core') 						=> 'g:i:s a',
									esc_html__('Saturday, November 6th, 2010','flownews-core')  		=> 'l, F jS, Y',
				  					esc_html__('Nov 6, 2010 @ 0:50','flownews-core') 				=> 'M j, Y @ G:i',
									esc_html__('2010/11/06 at 12:50 AM','flownews-core')  			=> 'Y/m/d \a\t g:i A',
				  					esc_html__('2010/11/06 at 12:50am','flownews-core') 				=> 'Y/m/d \a\t g:ia',
									esc_html__('2010/11/06 12:50:48 AM','flownews-core')  			=> 'Y/m/d g:i:s A',
									esc_html__('2010/11/06','flownews-core')  						=> 'Y/m/d',																																														
				   ),
				),
				
				// QUERY TAB 1			
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Name Tab 1",'flownews-core'),
				  "param_name" => "name_tab_1",
				  "description" => esc_html__("If you leave empty this field tabs will be set on off",'flownews-core'),
				  "group" => "Query Tab 1"					  				  					  				  			  				  
			    ),					
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Source", 'flownews-core'),
                  "param_name" => "source",
                  "value" => array(
				  					esc_html__('Wordpress Posts','flownews-core') 		=> 'post',
									esc_html__('Custom Posts Type','flownews-core')  	=> 'post_type',
				   ),
				   "group" => "Query Tab 1"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("All Posts/Sticky posts", 'flownews-core'),
                  "param_name" => "posts_source",
                  "value" => array(
				  					esc_html__('All Posts','flownews-core') 		 		=> 'all_posts',
									esc_html__('Only Sticky Posts','flownews-core')  	=> 'sticky_posts',
				   ),
				   "group" => "Query Tab 1",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => esc_html__("Select Post Type Source",'flownews-core'),
				  "param_name" => "posts_type",
				  "group" => "Query Tab 1",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post_type' )
				  ),					  				  					  				  			  				  
			    ),
			    array(
				  "type" => "checkbox", 
				  "class" => "",
				  "heading" => esc_html__("Categories",'flownews-core'),
				  "param_name" => "categories",
				  "group" => "Query Tab 1", 
				  "value" => $categories_lists,	  
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post' )
				  ),				  					  				  					  				  			  				  
			    ),				
				
				
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Categories Posts Type",'flownews-core'),
				  "param_name" => "categories_post_type",
				  "description" => esc_html__("Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)","flownews"),
				  "group" => "Query Tab 1",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post_type' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order", 'flownews-core'),
                  "param_name" => "order",
                  "value" => array(
				  					esc_html__('DESC','flownews-core')  	=> 'DESC',
				  					esc_html__('ASC','flownews-core') 	=> 'ASC'
									
				   ),
				   "group" => "Query Tab 1"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order By", 'flownews-core'),
                  "param_name" => "orderby",
                  "value" => array(
				  					esc_html__('Date','flownews-core') 				=> 'date',
									esc_html__('ID','flownews-core')  				=> 'ID',
				  					esc_html__('Author','flownews-core') 			=> 'author',
									esc_html__('Title','flownews-core')  			=> 'title',
				  					esc_html__('Name','flownews-core') 				=> 'name',
									esc_html__('Modified','flownews-core')  			=> 'modified',
				  					esc_html__('Parent','flownews-core') 			=> 'parent',
									esc_html__('Rand','flownews-core')  				=> 'rand',
									esc_html__('Comments Count','flownews-core')  	=> 'comment_count',
									esc_html__('Views','flownews-core')				=> 'views',
									esc_html__('None','flownews-core')  				=> 'none',																																													
				   ),
				   "group" => "Query Tab 1"				   			   
				),												

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Number Posts",'flownews-core'),
				  "param_name" => "num_posts",
				  "description" => "ex 10",
				  "group" => "Query Tab 1"					  				  					  				  			  				  
			    ),	

				// QUERY TAB 2
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Name Tab 2",'flownews-core'),
				  "param_name" => "name_tab_2",
				  "description" => esc_html__("If you leave empty this field tabs will be set on off",'flownews-core'),
				  "group" => "Query Tab 2"					  				  					  				  			  				  
			    ),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Source", 'flownews-core'),
                  "param_name" => "source_tab2",
                  "value" => array(
				  					esc_html__('Wordpress Posts','flownews-core') 		=> 'post',
									esc_html__('Custom Posts Type','flownews-core')  	=> 'post_type',
				   ),
				   "group" => "Query Tab 2"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("All Posts/Sticky posts", 'flownews-core'),
                  "param_name" => "posts_source_tab2",
                  "value" => array(
				  					esc_html__('All Posts','flownews-core') 		 		=> 'all_posts',
									esc_html__('Only Sticky Posts','flownews-core')  	=> 'sticky_posts',
				   ),
				   "group" => "Query Tab 2",
				  "dependency" => array(
							'element' => 'source_tab2',
							'value' => array( 'post' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => esc_html__("Select Post Type Source",'flownews-core'),
				  "param_name" => "posts_type_tab2",
				  "group" => "Query Tab 2",
				  "dependency" => array(
							'element' => 'source_tab2',
							'value' => array( 'post_type' )
				  ),					  				  					  				  			  				  
			    ),
			    array(
				  "type" => "checkbox", 
				  "class" => "",
				  "heading" => esc_html__("Categories",'flownews-core'),
				  "param_name" => "categories_tab2",
				  "group" => "Query Tab 2", 
				  "value" => $categories_lists,	  
				  "dependency" => array(
							'element' => 'source_tab2',
							'value' => array( 'post' )
				  ),				  					  				  					  				  			  				  
			    ),				
				
				
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Categories Posts Type",'flownews-core'),
				  "param_name" => "categories_post_type_tab2",
				  "description" => esc_html__("Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)","flownews"),
				  "group" => "Query Tab 2",
				  "dependency" => array(
							'element' => 'source_tab2',
							'value' => array( 'post_type' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order", 'flownews-core'),
                  "param_name" => "order_tab2",
                  "value" => array(
				  					esc_html__('DESC','flownews-core')  	=> 'DESC',
				  					esc_html__('ASC','flownews-core') 	=> 'ASC'
									
				   ),
				   "group" => "Query Tab 2"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order By", 'flownews-core'),
                  "param_name" => "orderby_tab2",
                  "value" => array(
				  					esc_html__('Date','flownews-core') 				=> 'date',
									esc_html__('ID','flownews-core')  				=> 'ID',
				  					esc_html__('Author','flownews-core') 			=> 'author',
									esc_html__('Title','flownews-core')  			=> 'title',
				  					esc_html__('Name','flownews-core') 				=> 'name',
									esc_html__('Modified','flownews-core')  			=> 'modified',
				  					esc_html__('Parent','flownews-core') 			=> 'parent',
									esc_html__('Rand','flownews-core')  				=> 'rand',
									esc_html__('Comments Count','flownews-core')  	=> 'comment_count',
									esc_html__('Views','flownews-core')				=> 'views',
									esc_html__('None','flownews-core')  				=> 'none',																																													
				   ),
				   "group" => "Query Tab 2"				   			   
				),												

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Number Posts",'flownews-core'),
				  "param_name" => "num_posts_tab2",
				  "description" => "ex 10",
				  "group" => "Query Tab 2"					  				  					  				  			  				  
			    ),					
				
				// QUERY TAB 3
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Name Tab 3",'flownews-core'),
				  "param_name" => "name_tab_3",
				  "description" => esc_html__("If you leave empty this field tabs will be set on off",'flownews-core'),
				  "group" => "Query Tab 3"					  				  					  				  			  				  
			    ),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Source", 'flownews-core'),
                  "param_name" => "source_tab3",
                  "value" => array(
				  					esc_html__('Wordpress Posts','flownews-core') 		=> 'post',
									esc_html__('Custom Posts Type','flownews-core')  	=> 'post_type',
				   ),
				   "group" => "Query Tab 3"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("All Posts/Sticky posts", 'flownews-core'),
                  "param_name" => "posts_source_tab3",
                  "value" => array(
				  					esc_html__('All Posts','flownews-core') 		 		=> 'all_posts',
									esc_html__('Only Sticky Posts','flownews-core')  	=> 'sticky_posts',
				   ),
				   "group" => "Query Tab 3",
				  "dependency" => array(
							'element' => 'source_tab3',
							'value' => array( 'post' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => esc_html__("Select Post Type Source",'flownews-core'),
				  "param_name" => "posts_type_tab3",
				  "group" => "Query Tab 3",
				  "dependency" => array(
							'element' => 'source_tab3',
							'value' => array( 'post_type' )
				  ),					  				  					  				  			  				  
			    ),
			    array(
				  "type" => "checkbox", 
				  "class" => "",
				  "heading" => esc_html__("Categories",'flownews-core'),
				  "param_name" => "categories_tab3",
				  "group" => "Query Tab 3", 
				  "value" => $categories_lists,	  
				  "dependency" => array(
							'element' => 'source_tab3',
							'value' => array( 'post' )
				  ),				  					  				  					  				  			  				  
			    ),				
				
				
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Categories Posts Type",'flownews-core'),
				  "param_name" => "categories_post_type_tab3",
				  "description" => esc_html__("Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)","flownews"),
				  "group" => "Query Tab 3",
				  "dependency" => array(
							'element' => 'source_tab3',
							'value' => array( 'post_type' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order", 'flownews-core'),
                  "param_name" => "order_tab3",
                  "value" => array(
				  					esc_html__('DESC','flownews-core')  	=> 'DESC',
				  					esc_html__('ASC','flownews-core') 	=> 'ASC'
									
				   ),
				   "group" => "Query Tab 3"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order By", 'flownews-core'),
                  "param_name" => "orderby_tab3",
                  "value" => array(
				  					esc_html__('Date','flownews-core') 				=> 'date',
									esc_html__('ID','flownews-core')  				=> 'ID',
				  					esc_html__('Author','flownews-core') 			=> 'author',
									esc_html__('Title','flownews-core')  			=> 'title',
				  					esc_html__('Name','flownews-core') 				=> 'name',
									esc_html__('Modified','flownews-core')  			=> 'modified',
				  					esc_html__('Parent','flownews-core') 			=> 'parent',
									esc_html__('Rand','flownews-core')  				=> 'rand',
									esc_html__('Comments Count','flownews-core')  	=> 'comment_count',
									esc_html__('Views','flownews-core')				=> 'views',
									esc_html__('None','flownews-core')  				=> 'none',																																													
				   ),
				   "group" => "Query Tab 3"				   			   
				),												

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Number Posts",'flownews-core'),
				  "param_name" => "num_posts_tab3",
				  "description" => "ex 10",
				  "group" => "Query Tab 3"					  				  					  				  			  				  
			    ),							
				
		)		
		) // CLOSE VC MAP
	  );
    }					
}
// Finally initialize code
new flownews_posts_tab_display_class();
?>