<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */ 
 
class flownews_newsticker_display_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_flownews_newsticker_displayWithVC' ) );
        add_shortcode( 'flownews_newsticker_display', array( $this, 'flownews_newsticker_display_function' ) );	
    }
 
    public function integrate_flownews_newsticker_displayWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		$categories = get_categories(array('orderby' => 'name','order' => 'ASC'));
		$categories_lists = array();
		$i = 0;
		foreach ($categories as $category) { 
			$categories_lists[$category->name] = $category->slug;
			$i++;
		}
		vc_map( array(
            "name" => esc_html__("News Ticker", 'flownews-core'),
            "description" => esc_html__("Display your posts", 'flownews-core'),
            "base" => "flownews_newsticker_display",
            "class" => "flownews",
            "controls" => "full",
            "icon" => FLOWNEWS_IMG_URL . 'vc_icon.png',
            "category" => esc_html__('FlowNews', 'flownews-core'),
            "params" => array(						
				array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__("News Ticker Name",'flownews-core'),
						"param_name" => "name",
						"admin_label" => true,				  				  			  				  
				),		
                array(		
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => esc_html__("Type", 'flownews-core'),
					  "param_name" => "type",
					  "value" => array(
										esc_html__('Type 1','flownews-core') 				=> 'type1',
										esc_html__('Type 2','flownews-core')  				=> 'type2',
										esc_html__('Type 3','flownews-core')  				=> 'type3',										
					   ),			   
				),
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Autoplay (ms)",'flownews-core'),
				  "param_name" => "autoplay",
				  "description" => esc_html__("Example 2000. Leave empty for default value",'flownews-core')					  				  					  				  			  				  
			    ),
				
				// QUERY
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Source", 'flownews-core'),
                  "param_name" => "source",
                  "value" => array(
				  					esc_html__('Wordpress Posts','flownews-core') 		=> 'post',
									esc_html__('Custom Posts Type','flownews-core')  	=> 'post_type',
				   ),
				   "group" => "Query"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("All Posts/Sticky Posts", 'flownews-core'),
                  "param_name" => "newsticker_source",
                  "value" => array(
				  					esc_html__('All Posts','flownews-core') 		 		=> 'all_posts',
									esc_html__('Only Sticky Posts','flownews-core')  	=> 'sticky_posts',
				   ),
				   "group" => "Query",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => esc_html__("Select Post Type Source",'flownews-core'),
				  "param_name" => "posts_type",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post_type' )
				  ),					  				  					  				  			  				  
			    ),
			    array(
				  "type" => "checkbox", 
				  "class" => "",
				  "heading" => esc_html__("Categories",'flownews-core'),
				  "param_name" => "categories",
				  "group" => "Query", 
				  "value" => $categories_lists,	  
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post' )
				  ),				  					  				  					  				  			  				  
			    ),				
				
				
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Categories Post Type",'flownews-core'),
				  "param_name" => "categories_post_type",
				  "description" => esc_html__("Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)","flownews"),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'source',
							'value' => array( 'post_type' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order", 'flownews-core'),
                  "param_name" => "order",
                  "value" => array(
				  					esc_html__('DESC','flownews-core')  	=> 'DESC',
				  					esc_html__('ASC','flownews-core') 	=> 'ASC'
									
				   ),
				   "group" => "Query"				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => esc_html__("Order By", 'flownews-core'),
                  "param_name" => "orderby",
                  "value" => array(
				  					esc_html__('Date','flownews-core') 				=> 'date',
									esc_html__('ID','flownews-core')  				=> 'ID',
				  					esc_html__('Author','flownews-core') 			=> 'author',
									esc_html__('Title','flownews-core')  			=> 'title',
				  					esc_html__('Name','flownews-core') 				=> 'name',
									esc_html__('Modified','flownews-core')  			=> 'modified',
				  					esc_html__('Parent','flownews-core') 			=> 'parent',
									esc_html__('Rand','flownews-core')  				=> 'rand',
									esc_html__('Comments Count','flownews-core')  	=> 'comment_count',
									esc_html__('Views','flownews-core')				=> 'views',
									esc_html__('None','flownews-core')  				=> 'none',																																													
				   ),
				   "group" => "Query"				   			   
				),												

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => esc_html__("Number Posts",'flownews-core'),
				  "param_name" => "num_posts",
				  "description" => "ex 10",
				  "group" => "Query",					  				  					  				  			  				  
			    ),			
				
		)		
		) // CLOSE VC MAP
	  );
    }					
}
// Finally initialize code
new flownews_newsticker_display_class();
?>
