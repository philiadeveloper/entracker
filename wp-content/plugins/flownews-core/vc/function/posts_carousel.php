<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */ 
 
 class flownews_posts_carousel_display_class_function extends flownews_posts_carousel_display_class {
	public function flownews_posts_carousel_display_function ( $attr ) {	
		global $flownews_theme;
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"type"					=> 'type1',
					"name"					=> '',
					
					// QUERY					
					"source" 				=> 'post',
					"posts_source" 			=> 'all_posts',
					"post_type" 			=> '',
					"categories" 			=> '',									
					"categories_post_type" 	=> '',
					"pagination" 			=> 'off',
					"pagination_type" 		=> 'numeric',
					"num_posts_page" 		=> '',
					"num_posts" 			=> '', 	
					"orderby" 				=> 'date',
					"order" 				=> 'DESC',		 			
					
					// OPTIONS	
					"date_format" 			=> 'F j, Y',				

					// CAROUSEL								
					"lazy_load"	 			=> 'false',   
					"item_show"				=> '1', 
					"item_show_900"			=> '1',
					"item_show_600"			=> '1',
					"autoplay"				=> '3000',
					"navigation"			=> 'true'
					
					), 
					$attr)
		);	
		
		$return = '';

		/************************* SCRIPT LOAD **************************/
		
		wp_enqueue_style( 'flownews-carousel' );
		wp_enqueue_script( 'flownews-carousel-js' );		
		wp_enqueue_style('flownews-vc-element');	

		if($type == 'type1') { $posts_layout_type = 'flownews-posts-carousel-type1'; $margin = '25'; }
		if($type == 'type2') { $posts_layout_type = 'flownews-posts-carousel-type2'; $margin = '4'; }
		
		if($item_show == 'default') { $item_show = '4'; }
		if($item_show_900 == 'default') { $item_show_900 = '3'; }
		if($item_show_600 == 'default') { $item_show_600 = '1'; }
		
		/* RTL */	
		if ($flownews_theme['rtl']) :  $rtl = 'rtl:true,'; else : $rtl = ''; endif;  
		/* #RTL */
		
		if($flownews_theme['flownews_lazy_load']) : $lazyLoad = 'lazyLoad:true,'; else : $lazyLoad = ''; endif;
		
		$return = '<script type="text/javascript">
					jQuery(document).ready(function($){
					$(\'.flownews-vc-element-posts-carousel-'.$instance.'\').owlCarousel({
						loop:true,
						smartSpeed: 2000,
						margin:'.$margin.',						
						nav:'.$navigation.',
						'.$lazyLoad.'
						'.$rtl.'
						dots:'.$navigation.',';					
		if(!empty($autoplay) || $autoplay != '') { 
				$return .= 'autoplay: true,
				autoplayTimeout: '.$autoplay.',';
		}
		$return .= 'navText: [\'<i class="flownewsicon fa-angle-left"></i>\',\'<i class="flownewsicon fa-angle-right"></i>\'],
						responsive:{
							0:{
								items:'.$item_show_600.'
							},
							600:{
								items:'.$item_show_600.'
							},
							700:{
								items:'.$item_show_600.'
							},
							800:{
								items:'.$item_show_900.'
							},
							900:{
								items:'.$item_show_900.'
							},
							1000:{
								items:'.$item_show_900.'
							},
							1200:{
								items:'.$item_show.'
							}
							
						}
					});
				});		
				</script>';	

		// LOOP QUERY
		$query = flownews_vc_query( $source,
								    $posts_source, 
								    $post_type, 
								    $categories,
								    $categories_post_type, 
								    $order, 
									$orderby, 
									$pagination, 
									$pagination_type,
									$num_posts, 
									$num_posts_page );
								
		$return .= '<div class="wpmp-clear"></div>';
		
		$count = 0;
		
		if($name) :
			$return .= '<div class="flownews-vc-element-posts-carousel-title-box title-box-'.$instance.'"><h2>'.$name.'</h2></div>';
		endif;
		
		$return .= '<div class="flownews-vc-element-posts-carousel '.$posts_layout_type.' flownews-vc-element-posts-carousel-'.$instance.' flownews-vc-element-posts-carousel-item-show-'.$item_show.' element-no-padding">';		
				
		$loop = new WP_Query($query);
		
		if($loop) :
			while ( $loop->have_posts() ) : $loop->the_post();
		
				$id_post = get_the_id();
				$link = get_permalink(); 

				/**********************************************************************/
				/******************************** TYPE 1 ******************************/
				/**********************************************************************/
				
				if($type == 'type1') :

						$return .= '<article class="item-posts first-element-posts">';
							$return .= '<div class="article-image">';
								if($item_show == '1') :
									$return .= flownews_vc_thumbs_nll('flownews-vc-header');
								else :
									$return .= flownews_vc_thumbs_nll('flownews-vc-header');
								endif;
								$return .= flownews_check_format();
								$return .= '<div class="article-category">'.flownews_vc_category($source,$post_type).'
																			<a href="'.$link.'"><i class="flownewsicon fa-mail-forward"></i></a>
											</div>';
							$return .= '</div>';
							$return .= '<div class="article-info">';
								$return .= '<div class="article-info-top">';
										$return .= '<div class="article-data"><i class="flownewsicon fa-calendar-o"></i>'.get_the_date($date_format).'</div>';
										$return .= '<div class="article-separator">|</div>';
										$return .= '<div class="article-comments"><i class="flownewsicon fa-comment-o"></i>'.flownews_vc_get_num_comments().'</div>';
								$return .= '<div class="flownews-clear"></div></div>';
								$return .= '<div class="article-info-bottom">';		
										$return .= '<h3 class="article-title"><a href="'.$link.'">'.get_the_title().'</a></h3>';
										$return .= '<div class="flownews-clear"></div>';	
								$return .= '</div>';
							$return .= '</div>';
						$return .= '</article>';
					
				endif;		
				
				/**********************************************************************/
				/******************************** TYPE 2 ******************************/
				/**********************************************************************/
				
				if($type == 'type2') :				

					$return .= '<article class="item-posts first-element-header col-xs-12">';
						if($item_show == '1') :
							$return .= flownews_vc_thumbs_nll('flownews-preview-post');
						else :
							$return .= flownews_vc_thumbs_nll('flownews-vc-header');
						endif;						
						$return .= flownews_check_format();
						$return .= '<div class="article-info">
							<div class="article-info-top">
								<h2 class="article-title"><a href="'.$link.'">'.get_the_title().'</a></h2>
								<div class="article-category">'.flownews_vc_category($source,$post_type).'</div>
								<div class="flownews-clear"></div>
							</div>	
							<div class="article-info-bottom">
								<div class="article-data"><i class="flownewsicon fa-calendar-o"></i>'.get_the_date().'</div>
								<div class="article-separator">|</div>
								<div class="article-comments"><i class="flownewsicon fa-comment-o"></i>'.flownews_vc_get_num_comments().'</div>
								<div class="flownews-clear"></div>
							</div>	
						</div>
						<a href="'.$link.'" class="header-pattern"></a>
					</article>';				
				
				endif;
				
			$count++;
			endwhile;
		endif;	
		
		$return .= '</div>';		
		
		$return .= '<div class="flownews-clear"></div>';
		wp_reset_query();
		return $return;
		
	}
 }
 
 new flownews_posts_carousel_display_class_function();		