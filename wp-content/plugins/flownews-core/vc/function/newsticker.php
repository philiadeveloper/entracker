<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */ 
 
 class flownews_newsticker_display_class_function extends flownews_newsticker_display_class {
	public function flownews_newsticker_display_function ( $attr ) {	
		global $flownews_theme;		
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"type"					=> 'type1',
					"name"					=> '',
					
					// QUERY					
					"source" 				=> 'post',
					"posts_source" 			=> 'all_posts',
					"post_type" 			=> '',
					"categories" 			=> '',									
					"categories_post_type" 	=> '',
					"pagination" 			=> 'off',
					"num_posts" 			=> '', 	
					"orderby" 				=> 'date',
					"order" 				=> 'DESC',		 			
					
					// OPTIONS					
					"autoplay"				=> ''						
					), 
					$attr)
		);	
		
		$return = '';
	
		/* RTL */	
		if ($flownews_theme['rtl']) :  $rtl = 'rtl:true,'; else : $rtl = ''; endif;  
		/* #RTL */		
	
		wp_enqueue_style( 'flownews-carousel' );
		wp_enqueue_script( 'flownews-carousel-js' );		
		if($autoplay == '') : $autoplay = '2000'; endif;
		
		/************************* SCRIPT LOAD **************************/
		
		wp_enqueue_style('flownews-vc-element');	

		if($type == 'type1') { 					
					if(empty($num_posts)) : 
						$num_posts = 6; 
					endif;		
					$newsticker_layout_type = 'flownews-newsticker-type1'; }
		if($type == 'type2') { 
					if(empty($num_posts)) : 
						$num_posts = 3; 
					endif;	
					$newsticker_layout_type = 'flownews-newsticker-type2'; 					
		}
		if($type == 'type3') { 
					if(empty($num_posts)) : 
						$num_posts = 2; 
					endif;				
					$newsticker_layout_type = 'flownews-newsticker-type3'; 
		}	
		$container_class = '';

		$post_id = get_the_id(); 
			
		// LOOP QUERY
		$query = flownews_vc_query( $source,
								    $posts_source, 
								    $post_type, 
								    $categories,
								    $categories_post_type, 
								    $order, 
									$orderby, 
									'no', 
									'',
									$num_posts, 
									'' );
		
		$return .= '<div class="wpmp-clear"></div>';

		$loop = new WP_Query($query);
		
		if($type != 'type3') :
		
			if($loop->post_count === 1) :
				$return = '';
			else :
				$return = '<script type="text/javascript">jQuery(document).ready(function($){
							$(\'.flownews-ticker-addon-'.$instance.'\').owlCarousel({
								loop:true,
								margin:0,
								nav:true,
								lazyLoad: false,
								dots:false,
								autoplay: true,
								smartSpeed: 2000,
								'.$rtl.'
								navText: [\'<i class="flownewsicon fa-angle-left"></i>\',\'<i class="flownewsicon fa-angle-right"></i>\'],
								autoplayTimeout: '.$autoplay.',
								responsive:{
										0:{
											items:1
										}							
									}
								});
							});</script>';
			endif;
		
			$return .= '<div class="flownews-top-news-ticker-addon '.$newsticker_layout_type.' flownews-ticker-addon-'.$instance.'">';
		
			if($loop) :
				while ( $loop->have_posts() ) : $loop->the_post();
			
					$id_post = get_the_id();
					$link = get_permalink(); 	
					
					/**********************************************************************/
					/******************************** TYPE 1 ******************************/
					/**********************************************************************/				
					if($type == 'type1') :
						$return .= '<div class="news-ticker-item">';
						
							$return .= '<div class="news-ticker-item-category">'.flownews_category(1).'</div>';
							$return .= '<div class="news-ticker-item-title"><a href="'.$link.'">'.get_the_title().'</a></div>';
							
						$return .= '</div>';
					endif;

					/**********************************************************************/
					/******************************** TYPE 2 ******************************/
					/**********************************************************************/				
					if($type == 'type2') :
						$return .= '<div class="news-ticker-item">';
						
							$return .= '<div class="news-ticker-item-category">'.flownews_category(1).'</div>';
							$return .= '<div class="news-ticker-item-title"><a href="'.$link.'">'.get_the_title().'</a></div>';
							
						$return .= '</div>';
					endif;
				
				endwhile;
			endif;	
	
			$return .= '</div>';
		

					/**********************************************************************/
					/******************************** TYPE 3 ******************************/
					/**********************************************************************/		
		
		else :

			wp_enqueue_script('flownews-newsticker-js', FLOWNEWS_JS_URL . 'newsticker.js', array('jquery'), '', true);
			$return = '<script type="text/javascript">jQuery(document).ready(function($){
				$(\'#flownews-top-news-ticker\').ticker({
														titleText: \''.esc_html__('Trending','flownews-core').'\',
														'.$rtl.'
														pauseOnItems: '.$autoplay.'														
														});
			});</script>';		

			$return .= '<div class="flownews-top-news-ticker flownews-newsticker-type3"><ol id="flownews-top-news-ticker" class="ticker">';
		
			if($loop) :
				while ( $loop->have_posts() ) : $loop->the_post();
			
					$id_post = get_the_id();
					$link = get_permalink(); 	
					
					$return .= '<li>';
					
						$return .= '<a class="news-ticker-item-title" href="'.$link.'">'.get_the_title().'</a>';
						
					$return .= '</li>';
		
				endwhile;
			endif;		
		
			$return .= '</ol></div>';		
			
		endif;

		wp_reset_query();
		return $return;	
	}
 }
 
 new flownews_newsticker_display_class_function();		