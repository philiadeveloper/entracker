jQuery(document).ready(function($){
// Custom popup box
	 $("#flownews-generator-button").click(function(){
	  $("#flownews-generator-wrap, #flownews-generator-overlay").show();
	 });
	 
	 $("#flownews-generator-close").click(function(){
	  $("#flownews-generator-wrap, #flownews-generator-overlay").hide();
	 });

	 $("#flownews_shortcode_type").on('click', function(event) {
		 
		 var type = $('#flownews_shortcode_type').val();

		 if(type == 'flownews_columns') {
			$("#flownews_columns").css("display", "block");
		 } else {
			$("#flownews_columns").css("display", "none");
		 }

		 if(type == 'flownews_typography') {
			$("#flownews_typography").css("display", "block");
		 } else {
			$("#flownews_typography").css("display", "none");
		 }

		 if(type == 'flownews_dropcaps') {
			$("#flownews_dropcaps").css("display", "block");
		 } else {
			$("#flownews_dropcaps").css("display", "none");
		 }		 
		 
		 if(type == 'flownews_blockquotes') {
			$("#flownews_blockquotes").css("display", "block");
		 } else {
			$("#flownews_blockquotes").css("display", "none");
		 }
		 
	 });
	 	 
	 // Insert shortcode
	 $('#flownews-generator-insert').on('click', function(event) {
		
		var type = $('#flownews_shortcode_type').val();
		
		if(type == 'flownews_lists_line') {
			
				var shortcode = '[flownews_list type="line"][flownews_list_item]Your Item Text[/flownews_list_item][flownews_list_item]Your Item Text[/flownews_list_item][flownews_list_item]Your Item Text[/flownews_list_item][/flownews_list]';			
			
		}
		
		if(type == 'flownews_lists_arrow') {
			
				var shortcode = '[flownews_list type="arrow"][flownews_list_item]Your Item Text[/flownews_list_item][flownews_list_item]Your Item Text[/flownews_list_item][flownews_list_item]Your Item Text[/flownews_list_item][/flownews_list]';			
			
		}

		if(type == 'flownews_blockquotes') {
			
				var blockquotes_align 		= $('#flownews_blockquotes_align').val();
				var blockquotes_background 	= $('#flownews_blockquotes_background').val();
				var blockquotes_color 		= $('#flownews_blockquotes_color').val();
			
				var shortcode = '[flownews_blockquotes align="' + blockquotes_align + '" background="' + blockquotes_background + '" color="' + blockquotes_color + '"]Your Blockquores Text[/flownews_blockquotes]';			
			
		}

		if(type == 'flownews_dropcaps') {
			
				var dropcaps_align 		= $('#flownews_dropcaps_align').val();
				var dropcaps_background = $('#flownews_dropcaps_background').val();
				var dropcaps_color 		= $('#flownews_dropcaps_color').val();
			
				var shortcode = '[flownews_dropcaps align="' + dropcaps_align + '" background="' + dropcaps_background + '" color="' + dropcaps_color + '"]Your Dropcaps Text[/flownews_dropcaps]';			
			
		}
		
		if(type == 'flownews_typography') {
			
				var typography_background 	= $('#flownews_typography_background').val();
				var typography_color 		= $('#flownews_typography_color').val();
			
				var shortcode = '[flownews_typography background="' + typography_background + '" color="' + typography_color + '"]Your Item Text[/flownews_typography]';			
			
		}		
		
		// COLUMNS
		if(type == 'flownews_columns') {
		
			var columns_numbers 	= $('#flownews-columns-value').val(); 
			
			if(columns_numbers == 'col_2') {
				var shortcode = '[flownews_open_row][flownews_one_half]Your content goes here.....[/flownews_one_half]' +
								'[flownews_one_half]Your content goes here.....[/flownews_one_half][flownews_close_row]';
			}
		
			if(columns_numbers == 'col_3') {
				var shortcode = '[flownews_open_row][flownews_one_third]Your content goes here.....[/flownews_one_third]' + 
    						    '[flownews_one_third]Your content goes here.....[/flownews_one_third]' +
								'[flownews_one_third]Your content goes here.....[/flownews_one_third][flownews_close_row]';
			}		
		
			if(columns_numbers == 'col_4') {
				var shortcode = '[flownews_open_row][flownews_one_fourth]Your content goes here.....[/flownews_one_fourth]' + 
    						    '[flownews_one_fourth]Your content goes here.....[/flownews_one_fourth]' +
								'[flownews_one_fourth]Your content goes here.....[/flownews_one_fourth]' +
								'[flownews_one_fourth]Your content goes here.....[/flownews_one_fourth][flownews_close_row]';
			}		

			if(columns_numbers == 'col_6') {
				var shortcode = '[flownews_open_row][flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' + 
    						    '[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' +
								'[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' +
								'[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' +
								'[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' +
								'[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth][flownews_close_row]';
			}			

			if(columns_numbers == 'col_13_23') {
				var shortcode = '[flownews_open_row][flownews_one_third]Your content goes here.....[/flownews_one_third]' + 
    						    '[flownews_two_third]Your content goes here.....[/flownews_two_third][flownews_close_row]';
			}			

			if(columns_numbers == 'col_23_13') {
				var shortcode = '[flownews_open_row][flownews_two_third]Your content goes here.....[/flownews_two_third]' + 
    						    '[flownews_one_third]Your content goes here.....[/flownews_one_third][flownews_close_row]';
			}

			if(columns_numbers == 'col_14_34') {
				var shortcode = '[flownews_open_row][flownews_one_fourth]Your content goes here.....[/flownews_one_fourth]' + 
    						    '[flownews_three_fourth]Your content goes here.....[/flownews_three_fourth][flownews_close_row]';
			}

			if(columns_numbers == 'col_34_14') {
				var shortcode = '[flownews_open_row][flownews_three_fourth]Your content goes here.....[/flownews_three_fourth]' + 
    						    '[flownews_one_fourth]Your content goes here.....[/flownews_one_fourth][flownews_close_row]';
			}

			if(columns_numbers == 'col_16_56') {
				var shortcode = '[flownews_open_row][flownews_one_sixth]Your content goes here.....[/flownews_one_sixth]' + 
    						    '[flownews_five_sixth]Your content goes here.....[/flownews_five_sixth][flownews_close_row]';
			}

			if(columns_numbers == 'col_56_16') {
				var shortcode = '[flownews_open_row][flownews_five_sixth]Your content goes here.....[/flownews_five_sixth]' + 
    						    '[flownews_one_sixth]Your content goes here.....[/flownews_one_sixth][flownews_close_row]';
			}
										
		}	

		shortcode += '&nbsp;'; 		
		window.send_to_editor(shortcode);
		$("#flownews-generator-wrap, #flownews-generator-overlay").hide();
	});
});