<?php
function flownews_button( $page = null, $target = null ) {
  
  $return = '<script>
  function flownews_open_shortcodes_popup() {
	  jQuery(document).ready(function($){
		  
		  $("#flownews-generator-wrap, #flownews-generator-overlay").show();
		 
		 $("#flownews-generator-close").click(function(){
		  $("#flownews-generator-wrap, #flownews-generator-overlay").hide();
		 });
	  });	 
  }
  </script>';
  $return .= '<a href="#" onclick="flownews_open_shortcodes_popup()" class="button" title="Flownews SHORTCODES"><span class="flownews-button-portfolio"></span>'.__('Flownews SHORTCODES','flownews-core').'</a>';	

  echo $return;

}

add_action( 'media_buttons', 'flownews_button', 100 );

function flownews_generator() {

	?>

<div id="flownews-generator-overlay" class="flownews-overlay-bg" style="display:none"></div>

  <div id="flownews-generator-wrap" style="display:none">

   <div id="flownews-generator">

    <a href="#" id="flownews-generator-close"><span class="flownews-close">x</span></a>

	<div class="flownews_shortcodes_type">

	 <h1 class="flownews_main_title"><?php _e('Flownews Shortcodes','flownews-core'); ?></h1>


     <p class="wpmp-title"><?php _e('Select your shortcode','flownews-core'); ?></p>
     
     <select id="flownews_shortcode_type" name="flownews_shortcode_type" class="">
         	 <option value="flownews_lists_line">List Type 1 (Line)</option>              
         	 <option value="flownews_lists_arrow">List Type 2 (Arrow)</option>              
         	 <option value="flownews_blockquotes">Blockquotes</option>              
         	 <option value="flownews_typography">Typography</option>              
         	 <option value="flownews_dropcaps">Dropcaps</option>              
         	 <option value="flownews_columns">Columns</option>              
     </select>

	<div id="flownews_blockquotes" style="display:none">
		
		<div class="wpmp-field">
			<select id="flownews_blockquotes_align" name="flownews_blockquotes_align" class="">
				 <option value="center">Center</option>              
				 <option value="left">Left</option>              
				 <option value="right">Right</option>             
			</select>
		</div>
		
		<div class="wpmp-field">
			<p class="title"><?php _e('Main Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_blockquotes_background" /> 
		</div>

		<div class="wpmp-field">
			<p class="title"><?php _e('Secondary Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_blockquotes_color" /> 
		</div>

	</div>	 
	 
	<div id="flownews_typography" style="display:none">
	
		<div class="wpmp-field">
			<p class="title"><?php _e('Main Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_typography_background" /> 
		</div>

		<div class="wpmp-field">
			<p class="title"><?php _e('Secondary Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_typography_color" /> 
		</div>

	</div>	

	<div id="flownews_dropcaps" style="display:none">
		
		<div class="wpmp-field">
			<select id="flownews_dropcaps_align" name="flownews_dropcaps_align" class="">             
				 <option value="left">Left</option>              
				 <option value="right">Right</option>             
			</select>
		</div>
		
		<div class="wpmp-field">
			<p class="title"><?php _e('Main Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_dropcaps_background" /> 
		</div>

		<div class="wpmp-field">
			<p class="title"><?php _e('Secondary Color','flownews-core'); ?></p>
			<input type="text" value="#eeeeee" class="wp-color-picker-field" data-default-color="#eeeeee" id="flownews_dropcaps_color" /> 
		</div>

	</div>	
	 
     <div id="flownews_columns" style="display:none"> 
         
         <p class="title"><?php _e('Columns','flownews-core'); ?></p>
     
         <select id="flownews-columns-value">
    
             <option value="col_2">2</option>
             <option value="col_3">3</option>
             <option value="col_4">4</option>
             <option value="col_6">6</option>
             <option value="col_13_23">1/3 + 2/3</option>
             <option value="col_23_13">2/3 + 1/3</option>
             <option value="col_14_34">1/4 + 3/4</option>
             <option value="col_34_14">3/4 + 1/4</option>
             <option value="col_16_56">1/6 + 5/6</option>
             <option value="col_56_16">5/6 + 1/6</option>        
                               
         </select>
      
      
      </div>
            
	</div>
     
      
      
      
        
     <div class="flownews_clear_box"></div>
        
     <br />
     <br />
            
     <input name="flownews-generator-insert" type="submit" class="button button-primary button-large" id="flownews-generator-insert" value="<?php _e('Insert Shortcode','flownews-core'); ?>">
       
   </div> <!-- #flownews-generator -->

</div> <!-- #flownews-generator-wrap -->


   
	<?php

}


add_action( 'admin_footer', 'flownews_generator' );
