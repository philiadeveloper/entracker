<?php
/* LISTS */

function flownews_list ( $attr, $content = null ) {	
		
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"type"	=> ''
				), 
				$attr)
		);	
		$return = '<ul class="flownews_'.$type.'">' . do_shortcode($content) . '</ul>';
		
		return $return;
}

add_shortcode("flownews_list", "flownews_list");

function flownews_list_item ( $attr, $content = null ) {	
	$return = '<li>' . do_shortcode($content) . '</li>';
	return $return;
}

add_shortcode("flownews_list_item", "flownews_list_item");

function flownews_typography ( $attr, $content = null ) {	
		
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"background" => '',
					"color" => ''
				), 
				$attr)
		);	
		$return = '<span style="margin:5px;padding:5px;background:'.$background.';color:'.$color.';">' . do_shortcode($content) . '</span>';
		
		return $return;
}

add_shortcode("flownews_typography", "flownews_typography");

function flownews_blockquotes ( $attr, $content = null ) {	
		
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"background" => '',
					"color" => '',
					"align" => '',
				), 
				$attr)
		);	
		$return = '<blockquotes class="flownews-blockquotes flownews-blockquotes-'.$align.'" style="background:'.$background.';color:'.$color.';border-left-color:'.$color.';">' . do_shortcode($content) . '</blockquotes>';
		
		return $return;
}

add_shortcode("flownews_blockquotes", "flownews_blockquotes");

function flownews_dropcaps ( $attr, $content = null ) {	
		
		static $instance = 0;
		$instance++;	
				
		extract(
			shortcode_atts(
				array(
					"background" => '',
					"color" => '',
					"align" => '',
				), 
				$attr)
		);	
		$return = '<div class="flownews-dropcaps flownews-dropcaps-'.$align.'"><span class="flownews-dropcaps-element" style="background:'.$background.';color:'.$color.';">'.$content[0].'</span>' . do_shortcode(substr($content, 1)) . '</div>';
		
		return $return;
}

add_shortcode("flownews_dropcaps", "flownews_dropcaps");

/* COLUMNS */

function flownews_open_row( $atts, $content = null ) {
    return '<div class="flownews-columns-row">';
}
add_shortcode('flownews_open_row', 'flownews_open_row');

function flownews_close_row( $atts, $content = null ) {
    return '<div class="flownews-clear"></div></div>';
}
add_shortcode('flownews_close_row', 'flownews_close_row');

function flownews_one_third( $atts, $content = null ) {
    return '<div class="col-xs-4">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_one_third', 'flownews_one_third');
     
function flownews_two_third( $atts, $content = null ) {
    return '<div class="col-xs-8">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_two_third', 'flownews_two_third');
     
function flownews_one_half( $atts, $content = null ) {
    return '<div class="col-xs-6">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_one_half', 'flownews_one_half');
     
function flownews_one_fourth( $atts, $content = null ) {
    return '<div class="col-xs-3">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_one_fourth', 'flownews_one_fourth');
     
function flownews_three_fourth( $atts, $content = null ) {
    return '<div class="col-xs-9">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_three_fourth', 'flownews_three_fourth');
     
function flownews_one_sixth( $atts, $content = null ) {
    return '<div class="col-xs-2">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_one_sixth', 'flownews_one_sixth');
     
function flownews_five_sixth( $atts, $content = null ) {
    return '<div class="col-xs-10">' . do_shortcode($content) . '</div>';
}
add_shortcode('flownews_five_sixth', 'flownews_five_sixth');

?>