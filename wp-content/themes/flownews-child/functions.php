<?php
/**
 * Flownews Theme Child
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 function flownews_childtheme_enqueue_styles() {
	
	// enqueue parent styles
	wp_enqueue_style('flownews', get_template_directory_uri() .'/assets/css/style.css');
	
	// enqueue child styles
	wp_enqueue_style('flownews-child', get_stylesheet_directory_uri() .'/style.css', array('flownews'));
	
 }
 add_action('wp_enqueue_scripts', 'flownews_childtheme_enqueue_styles');