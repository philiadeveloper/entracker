<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */
 
 global $flownews_theme;

 
 ?>
 
 <!-- start:wp_footer -->
 <footer class="flownews-footer-wrap">
 
	<div class="flownews-header-wrap-container">
	

		
		<?php if(esc_html($flownews_theme['footer-top-active']) == true) : ?>
	
			<div class="flownews-footer-top">
			
				<?php if(esc_html($flownews_theme['advertisement-footer']) == true) : ?>							
					<div class="flownews-banner-footer">
						<div class="flownews-wrap-container">
							<?php echo flownews_banner_footer(); ?>
						</div>
					</div>								
				<?php endif; ?>			
			
				<div class="flownews-wrap-container element-no-padding">
				
					<?php if(esc_html($flownews_theme['footer-top-widget']) == 'footer-top-widget-3') : ?>

						<div class="footer-widget col-xs-4">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-1' ) ) :
									dynamic_sidebar('flownews-footer-1'); 
								endif;
							?>
						</div>
						<div class="footer-widget col-xs-4">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-2' ) ) :
									dynamic_sidebar('flownews-footer-2'); 
								endif;
							?>
						</div>
						<div class="footer-widget col-xs-4">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-3' ) ) :
									dynamic_sidebar('flownews-footer-3'); 
								endif;
							?>
						</div>
					
					<?php elseif(esc_html($flownews_theme['footer-top-widget']) == 'footer-top-widget-2') : ?>
					
						<div class="footer-widget col-xs-6">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-1' ) ) :
									dynamic_sidebar('flownews-footer-1'); 
								endif;
							?>
						</div>
						<div class="footer-widget col-xs-6">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-2' ) ) :
									dynamic_sidebar('flownews-footer-2'); 
								endif;
							?>
						</div>					
					
					<?php else : ?>
					
						<div class="footer-widget col-xs-12">
							<?php 
								if ( is_active_sidebar( 'flownews-footer-1' ) ) :
									dynamic_sidebar('flownews-footer-1'); 
								endif;
							?>
						</div>				
					
					<?php endif; ?>
						
						<div class="flownews-clear"></div>
				</div>
				<?php if($flownews_theme['footer-image-background-active'] == true) : ?>		
					<div class="footer-top-pattern"></div>
				<?php endif; ?>
			</div>
	
		<?php endif; ?>
	
		<?php if(esc_html($flownews_theme['footer-bottom-active']) == true) : ?>
		
			<div class="flownews-footer-bottom">
				<div class="flownews-wrap-container">		
		
					<?php
						$footer_bottom_elements = $flownews_theme['footer-bottom-type']['enabled'];					
						$count_elements = count($footer_bottom_elements);
						if($count_elements == 4) : $footer_class_span = '4'; endif;
						if($count_elements == 3) : $footer_class_span = '6'; endif;
						if($count_elements == 2) : $footer_class_span = '12'; endif;
						
						if ($footer_bottom_elements): foreach ($footer_bottom_elements as $key=>$value) {
						 
							switch($key) {

								case 'text':
									if ( ! defined( 'POLYLANG_VERSION' ) ) :

										echo '<div class="col-xs-'.$footer_class_span.'">
												<span class="copyright">'.esc_attr($flownews_theme['footer-text']).'</span>
											  </div>';
									else :

										echo '<div class="col-xs-'.$footer_class_span.'">
												<span class="copyright">'.pll__('footer text','flownews').'</span>
											  </div>';
									
									endif; 
								break;
						 
								case 'social':
								echo '<div class="col-xs-'.$footer_class_span.'">'.flownews_footer_social().'</div>';
								break;
						 
								case 'menu':
								echo '<div class="col-xs-'.$footer_class_span.'">';
									if(esc_html($flownews_theme['footer-top-menu']) != '') :
										echo flownews_get_menu_array(esc_html($flownews_theme['footer-top-menu']));
									endif;
								echo '</div>';
								break;

							}
						 
						}
						 
						endif;
					
					?>							 
					<div class="flownews-clear"></div>
				</div>	
			</div>
	
		<?php endif; ?>
	
	</div>
	
 </footer>	
 <!-- end:wp_footer -->

 <?php
	## Add Action Custom Code			
	if(!empty($flownews_theme['css-custom-code'])) :
		flownews_css_custom_code($flownews_theme);	
		add_action( 'wp_enqueue_scripts', 'flownews_css_custom_code' );
	endif;
 ?> 
 
 
 </div>
 <!-- end:outer wrap -->
 
 <?php if($flownews_theme['back-to-top'] == true) : ?>
		<!-- start:footer text -->
		<span class="backtotop"><i class="flownewsicon fa-angle-up"></i></span>
		<!-- end:footer text -->
 <?php endif; ?>	
 
 <?php wp_footer(); ?> 
 </body>
 </html>