<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 global $flownews_theme;
 ?>

 <div class="flownews-sidebar col-xs-3">
	<?php 
	
		if(is_single()) :
			 $flownews_sidebar_name = get_post_meta( get_the_id(), 'flownews-sidebar-name', true ); 
			 if(!isset($flownews_sidebar_name) || $flownews_sidebar_name == '') : $flownews_sidebar_name = $flownews_theme['flownews_panel_post_sidebar_name']; endif; 
			 if($flownews_sidebar_name == 'sidebar-default-panel-name') :
				$flownews_sidebar_name = $flownews_theme['flownews_panel_post_sidebar_name']; 
			 endif;	
		else :
			$flownews_sidebar_name 	= get_post_meta($post->ID, "flownews-sidebar-name", true);
			
			if(!isset($flownews_sidebar_name) || $flownews_sidebar_name == '') : 
				$flownews_sidebar_name = 'flownews-default'; 
			else : 
				$flownews_sidebar_name 	= get_post_meta($post->ID, "flownews-sidebar-name", true);
			endif;
		endif;
		
		if(is_category()) : $flownews_sidebar_name = 'flownews-default'; endif;
		
		if(is_attachment()) : $flownews_sidebar_name = 'flownews-default'; endif;
		
		if ( class_exists( 'bbPress' ) ) {
			if(is_bbpress()) {
				$flownews_sidebar_name = $flownews_theme['flownews_bbpress_sidebar_name'];
			}
		}
		
		if ( class_exists( 'BuddyPress' ) ) {
			if(is_buddypress()) {
				$flownews_sidebar_name = $flownews_theme['flownews_buddypress_sidebar_name'];
			}
		}		
		
		if ( is_active_sidebar( $flownews_sidebar_name ) ) {
			
			$checkWidget = wp_get_sidebars_widgets();
			if (!empty($checkWidget[$flownews_sidebar_name])) {               
				dynamic_sidebar($flownews_sidebar_name);                
			}
			
		} else {
			
			echo esc_html__('This is a Sidebar position. Add your widgets in this position using Default Sidebar or a custom sidebar.','flownews');
		
		}
    ?>
 </div>