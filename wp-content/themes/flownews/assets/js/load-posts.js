jQuery(document).ready(function($) {

	// The number of the next page to load (/page/x/).
	var pageNum = parseInt(fnwp_.startPage) + 1;
	
	// The maximum number of pages the current query can return.
	var max = parseInt(fnwp_.maxPages);
	
	// The link of the next page of posts.
	var nextLink 	= fnwp_.nextLink;
	var readmore 	= fnwp_.readtext;
	var loading 	= fnwp_.loading;
	var nomoreposts = fnwp_.nomoreposts;
	
	/**
	 * Replace the traditional navigation with our own,
	 * but only if there is at least one page of new posts to load.
	 */
	if(pageNum <= max) {
		// Insert the "More Posts" link.
		$('.flownews-load-more')
			.append('<div class="flownews-placeholder-'+ pageNum +'"></div><div class="flownews-clear"></div>')
			.append('<div id="flownews-load-posts"><a href="#">'+ readmore + '</a></div>');
	}
	
	
	/**
	 * Load new posts when the link is clicked.
	 */
	$('#flownews-load-posts a').click(function() {
	
		// Are there more posts to load?
		if(pageNum <= max) {
		
			// Show that we're working.
			$(this).text(loading);
			
			$('.flownews-placeholder-'+ pageNum).load(nextLink + ' .flownews-item-load-more',
				function() {
					// Update page number and nextLink.
					pageNum++;
					nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/'+ pageNum);
					
					// Add a new placeholder, for when user clicks again.
					$('#flownews-load-posts')
						.before('<div class="flownews-placeholder-'+ pageNum +'"></div><div class="flownews-clear"></div>')
					
					// Update the button message.
					if(pageNum <= max) {
						$('#flownews-load-posts a').text(readmore);
					} else {
						$('#flownews-load-posts a').text(nomoreposts);
					}
				}
			);
		} else {
			$('#flownews-load-posts a').append('.');
		}	
		
		return false;
	});
});