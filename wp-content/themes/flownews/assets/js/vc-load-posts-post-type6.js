jQuery(document).ready(function($) {
	
	var pageNum = parseInt(fnwp_.startPage) + 1;
	var max = parseInt(fnwp_.maxPages);
	var nextLink = fnwp_.nextLink;
	var readmore = fnwp_.readtext;
	var loading = fnwp_.loading;
	var nomoreposts = fnwp_.nomoreposts;	
	
	if(pageNum <= max) {
		$('.flownews-load-more-type6 .flownews-vc-element-posts-article-container')
			.append('<div class="flownews-load-more-container flownews-placeholder-'+ pageNum +'-type6"></div><div class="flownews-clear"></div>')
			.append('<div id="flownews-load-posts" class="flownews-load-posts-type6"><a href="#">'+ readmore + '</a></div>');
	}
	
	$('.flownews-load-posts-type6 a').click(function() {	
		if(pageNum <= max) {		
			$(this).text(loading);			
			$('.flownews-placeholder-'+ pageNum +'-type6').load(nextLink + ' .flownews-item-load-more-type6',
				function() {
					pageNum++;
					nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/'+ pageNum);
					$('.flownews-load-posts-type6')
						.before('<div class="flownews-load-more-container flownews-placeholder-'+ pageNum +'-type6"></div><div class="flownews-clear"></div>')
					if(pageNum <= max) {
						$('.flownews-load-posts-type6 a').text(readmore);
					} else {
						$('.flownews-load-posts-type6 a').text(nomoreposts);
					}
				}
			);
		} else {
			$('.flownews-load-posts-type6 a').append('.');
		}	
		
		return false;
	});
	
});