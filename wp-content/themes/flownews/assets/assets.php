<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */



//********************************************************************************//
// CSS
//********************************************************************************//


// Frontend
add_action( 'wp_enqueue_scripts', 'flownews_frontend_styles' );

function flownews_frontend_styles() {
	global $flownews_theme; 
	
	if($flownews_theme['flownews_min_assets']) :
	
		wp_enqueue_style( 'flownews-style',  FLOWNEWS_CSS_URL . 'style.min.css' );
		wp_enqueue_style( 'flownews-carousel',  FLOWNEWS_CSS_URL . 'owl.carousel.min.css' );
		wp_register_style( 'flownews-vc-element',  FLOWNEWS_CSS_URL . 'vc_element.min.css' );
		
	else :
	
		wp_enqueue_style( 'bootstrap',  FLOWNEWS_CSS_URL . 'bootstrap.css' );
		wp_enqueue_style( 'flownews-style',  FLOWNEWS_CSS_URL . 'style.css' );		
		wp_enqueue_style( 'flownews-fonts',  FLOWNEWS_CSS_URL . 'fonts.css' );					
		wp_enqueue_style( 'flownews-carousel',  FLOWNEWS_CSS_URL . 'owl.carousel.css' );
		wp_register_style( 'flownews-vc-element',  FLOWNEWS_CSS_URL . 'vc_element.css' );
		
	endif;
	
	wp_enqueue_style( 'flownews-dynamic',  FLOWNEWS_CSS_URL . 'dynamic.css' );
	
	
	if ( class_exists( 'WooCommerce' ) ) {
		wp_enqueue_style( 'flownews-woocommerce',  FLOWNEWS_CSS_URL . 'woocommerce.css' );
	}
	if ( class_exists( 'bbPress' ) ) {
		wp_enqueue_style( 'flownews-bbPress',  FLOWNEWS_CSS_URL . 'bbpress.css' );
	}
	if ( class_exists( 'BuddyPress' ) ) {
		wp_enqueue_style( 'flownews-buddypress',  FLOWNEWS_CSS_URL . 'buddypress.css' );
	}	
}

//********************************************************************************//
// JS
//********************************************************************************//


// Frontend
add_action('wp_enqueue_scripts', 'flownews_frontend_scripts');

function flownews_frontend_scripts() {
	
	global $flownews_theme;
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-masonry');
	wp_enqueue_script('flownews-main-js', FLOWNEWS_JS_URL . 'main.min.js', array('jquery'), '', true);
	wp_enqueue_script('flownews-carousel-js', FLOWNEWS_JS_URL . 'owl.carousel.min.js', array('jquery'), '', true);
	if($flownews_theme['flownews_lazy_load']) :
		wp_enqueue_script('flownews-lazyload', FLOWNEWS_JS_URL . 'jquery.lazyload.min.js', array('jquery'), '', true);
		$lazyload_script = 'jQuery(document).ready(function($){
					$("img.flownews-lazy-load").lazyload({
						effect : "fadeIn"
					});
		
					$(document).on(\'ajaxStop\', function() {
						$("img.flownews-lazy-load").lazyload({
							effect: \'fadeIn\'
						});		
					});	

					$(\'.menu-item-object-category\').on(\'mouseover\', function() {
						$(".menu-item-object-category img.flownews-lazy-load").lazyload({
							delay:0
						});		
					});
					
		});';
		wp_add_inline_script( 'flownews-lazyload', $lazyload_script );
		
	endif;
	wp_localize_script('flownews-main-js', 'ptajax', array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
	));
	
	/* RTL */	
	if ($flownews_theme['rtl']) :  $rtl = 'rtl:true,'; else : $rtl = ''; endif;  
	/* #RTL */		
	if($flownews_theme['flownews_lazy_load']) : $lazyLoad = 'lazyLoad:true,'; else : $lazyLoad = ''; endif;
	
	$script = 'jQuery(document).ready(function($){
		$(\'.related-item-container\').owlCarousel({
				loop:true,
				margin:20,
				nav:true,
				'.$lazyLoad.'
				dots:true,
				autoplay: true,
				autoplayTimeout: 2000,
				speed:2000,
				smartSpeed: 2000,
				'.$rtl.'				
				navText: [\'<i class="flownewsicon fa-angle-left"></i>\',\'<i class="flownewsicon fa-angle-right"></i>\'],
				responsive:{
							0:{
								items:1
							},
							480:{
								items:2
							}							
				}
			});
		});';
   wp_add_inline_script( 'flownews-carousel-js', $script );
   
}