<?php
	//prevent direct access
	header('Content-type: text/css');
	global $flownews_theme;
	
	/* Color Preset */
	$preset = $flownews_theme['preset'];
	
	if($preset == 'default') : 
		$main_color = '#e7685d';
		$secondary_color = '#c9564c';
		
		$header_top_background 	= '#000000';
		$header_top_text 		= '#FFFFFF';
		$header_top_line 		= '#333333';
		
		$header_bottom_background 	= '#282828';		
		$header_bottom_line 		= '#333333';		
		
		$header_bottom_text_menu 			= '#FFFFFF';
		$header_bottom_main_text_menu 		= '#FFFFFF';
		$header_bottom_text_submenu 		= '#333333';
		$header_bottom_background_submenu 	= '#FFFFFF';
		$header_bottom_border_submenu		= '#f4f4f4';
		
		$content_background 			= '#FFFFFF';
		$content_title 					= '#333333';
		$content_text					= '#747474';
		$content_text_info 				= '#646464';		
		$content_navigation_background 	= '#f4f4f4';		
		$content_post 					= '#ffffff';		
		
		$footer_top_background			= '#282828';
		$footer_top_title				= '#FFFFFF';
		$footer_top_text				= '#949494';
		$footer_top_line				= '#333333';
		
		$footer_bottom_background		= '#000000';
		$footer_bottom_text				= '#b7b7b7';
		$footer_bottom_line				= '#333333';
		
	endif;

	if($preset == 'dark') : 
		$main_color = '#304868';
		$secondary_color = '#1D2947';
		
		$header_top_background 	= '#0F0F0F';
		$header_top_text 		= '#FFFFFF';
		$header_top_line 		= '#161616';
		
		$header_bottom_background 	= '#0F0F0F';		
		$header_bottom_line 		= '#161616';		
		
		$header_bottom_text_menu 			= '#FFFFFF';
		$header_bottom_main_text_menu 		= '#FFFFFF';
		$header_bottom_text_submenu 		= '#FFFFFF';
		$header_bottom_background_submenu 	= '#0F0F0F';
		$header_bottom_border_submenu		= '#161616';
		
		$content_background 			= '#0F0F0F';
		$content_title 					= '#FFFFFF';
		$content_text					= '#B5B5B5';
		$content_text_info 				= '#CCCCCC';		
		$content_navigation_background 	= '#161616';		
		$content_post 					= '#FFFFFF';		
		
		$footer_top_background			= '#000000';
		$footer_top_title				= '#FFFFFF';
		$footer_top_text				= '#B5B5B5';
		$footer_top_line				= '#161616';
		
		$footer_bottom_background		= '#0F0F0F';
		$footer_bottom_text				= '#B5B5B5';
		$footer_bottom_line				= '#161616';		
	endif;

	if($preset == 'turquoise') : 
		$main_color = '#00858C';
		$secondary_color = '#005F70';
		
		$header_top_background 	= '#EFEFEF';
		$header_top_text 		= '#262626';
		$header_top_line 		= '#E2E2E2';
		
		$header_bottom_background 	= '#262626';		
		$header_bottom_line 		= '#1C1C1C';		
		
		$header_bottom_text_menu 			= '#FFFFFF';
		$header_bottom_main_text_menu 		= '#FFFFFF';
		$header_bottom_text_submenu 		= '#262626';
		$header_bottom_background_submenu 	= '#FFFFFF';
		$header_bottom_border_submenu		= '#F4F4F4';
		
		$content_background 			= '#FFFFFF';
		$content_title 					= '#262626';
		$content_text					= '#747474';
		$content_text_info 				= '#646464';		
		$content_navigation_background 	= '#F4F4F4';		
		$content_post 					= '#FFFFFF';		
		
		$footer_top_background			= '#262626';
		$footer_top_title				= '#FFFFFF';
		$footer_top_text				= '#747474';
		$footer_top_line				= '#333333';
		
		$footer_bottom_background		= '#004E59';
		$footer_bottom_text				= '#FFFFFF';
		$footer_bottom_line				= '#00373A';		
	endif;

	if($preset == 'orange') : 
		$main_color = '#F39C12';
		$secondary_color = '#E67E22';
		
		$header_top_background 	= '#000000';
		$header_top_text 		= '#FFFFFF';
		$header_top_line 		= '#191919';
		
		$header_bottom_background 	= '#FFFFFF';		
		$header_bottom_line 		= '#FFFFFF';		
		
		$header_bottom_text_menu 			= '#FFFFFF';
		$header_bottom_main_text_menu 		= '#000000';
		$header_bottom_text_submenu 		= '#000000';
		$header_bottom_background_submenu 	= '#FFFFFF';
		$header_bottom_border_submenu		= '#F4F4F4';
		
		$content_background 			= '#FFFFFF';
		$content_title 					= '#000000';
		$content_text					= '#747474';
		$content_text_info 				= '#646464';		
		$content_navigation_background 	= '#F4F4F4';		
		$content_post 					= '#FFFFFF';		
		
		$footer_top_background			= '#111111';
		$footer_top_title				= '#FFFFFF';
		$footer_top_text				= '#747474';
		$footer_top_line				= '#0E0E0E';
		
		$footer_bottom_background		= '#000000';
		$footer_bottom_text				= '#FFFFFF';
		$footer_bottom_line				= '#000000';		
	endif;	
	
	if($preset == 'custom') :
		$main_color = $flownews_theme['main-color']; // #e7685d
		$secondary_color = $flownews_theme['secondary-color']; // ##c9564c
		
		$header_top_background 	= $flownews_theme['header_top_background'];
		$header_top_text 		= $flownews_theme['header_top_text'];
		$header_top_line 		= $flownews_theme['header_top_line'];
		
		$header_bottom_background 	= $flownews_theme['header_bottom_background'];		
		$header_bottom_line 		= $flownews_theme['header_bottom_line'];		
		
		$header_bottom_text_menu 			= $flownews_theme['header_bottom_text_menu'];
		$header_bottom_main_text_menu 		= $flownews_theme['header_bottom_main_text_menu'];
		$header_bottom_text_submenu 		= $flownews_theme['header_bottom_text_submenu'];
		$header_bottom_background_submenu 	= $flownews_theme['header_bottom_background_submenu'];
		$header_bottom_border_submenu		= $flownews_theme['header_bottom_border_submenu'];
		$header_bottom_text_menu 			= $flownews_theme['header_bottom_text_menu'];
		
		
		$content_background 			= $flownews_theme['content_background'];
		$content_title 					= $flownews_theme['content_title'];
		$content_text					= $flownews_theme['content_text'];
		$content_text_info 				= $flownews_theme['content_text_info'];		
		$content_navigation_background 	= $flownews_theme['content_navigation_background'];		
		$content_post 					= $flownews_theme['content_post'];		
		
		$footer_top_background			= $flownews_theme['footer_top_background'];
		$footer_top_title				= $flownews_theme['footer_top_title'];
		$footer_top_text				= $flownews_theme['footer_top_text'];
		$footer_top_line				= $flownews_theme['footer_top_line'];
		
		$footer_bottom_background		= $flownews_theme['footer_bottom_background'];
		$footer_bottom_text				= $flownews_theme['footer_bottom_text'];
		$footer_bottom_line				= $flownews_theme['footer_bottom_line'];
		
	endif;
	
	
	
?>	
	<?php if($flownews_theme['bg-types'] == 'image') : ?>
    
        body {
            background-image:url('<?php echo esc_url($flownews_theme['bg-image']['url']);?>');
        	background-attachment:fixed;
			background-size:cover;
        }
    
	<?php 
	elseif ($flownews_theme['bg-types'] == 'color') : 
	
	if(empty($flownews_theme['bg-color']['rgba'])) : $flownews_theme['bg-color']['rgba'] = '#F6F6F6'; endif;
	
	?>
    body {
    	background-color:<?php echo $flownews_theme['bg-color']['rgba']; ?>;
    }	
	
	<?php else : ?>
	
    body {
    	background-image:url(<?php echo FLOWNEWS_URL . 'assets/img/patterns/'.$flownews_theme['bg-pattern'].''; ?>);
    	background-repeat:repeat;
    }
	
	<?php endif; ?>
	

    
    <?php // Font Family ?>
    body,
	.comment-name {
    	font-family:<?php echo $flownews_theme['main-typography']['font-family']; ?>;
    }
	p,
	.flownews-footer-top .widget_text .textwidget,
	.author-post-container .author-description,
	ul.flownews_arrow,
	.flownews-post ul,
	.flownews-post ol,
	.flownews-page ul,
	.flownews-page ol	{
		font-family:<?php echo $flownews_theme['p-typography']['font-family']; ?>;
	}
	
	/* Header Middle Background */
	<?php if($flownews_theme['header-middle-bg-types'] == 'image') : ?>
    
        .flownews-header-middle {
            background-image:url('<?php echo esc_url($flownews_theme['header-middle-bg-image']['url']);?>');
			background-size:cover;
        }
		.flownews-header-middle .flownews-wrap-container {
			background:none;
		}
    
	<?php 
	elseif ($flownews_theme['header-middle-bg-types'] == 'color') : 
	if(empty($flownews_theme['header-middle-bg-color']['rgba'])) : $flownews_theme['header-middle-bg-color']['rgba'] = '#FFFFFF'; endif;
	
	?>
    .flownews-header-middle,
	.flownews-header-middle .flownews-wrap-container {
    	background-color:<?php echo $flownews_theme['header-middle-bg-color']['rgba']; ?>;
    }	
	
	<?php else : ?>
	
    .flownews-header-middle {
    	background-image:url(<?php echo FLOWNEWS_URL . 'assets/img/patterns/'.$flownews_theme['header-middle-bg-pattern'].''; ?>);
    	background-repeat:repeat;
    }
	
	<?php endif; ?>
	
	
.flownews-wrap-container {
	background:<?php echo $content_background; ?>;
}
.flownews-header-top,
.flownews-header-top .flownews-wrap-container {
	background:<?php echo $header_top_background; ?>;
}
.flownews-header-bottom,
.flownews-header-bottom .flownews-wrap-container {
	background:<?php echo $header_bottom_background; ?>;
	border-bottom: 2px solid <?php echo $main_color; ?>;
}
body,
p {
	color:<?php echo $content_text; ?>;
}
h1, h2, h3, h4, h5, h6 {
	color:<?php echo $content_title; ?>;
}
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
	color:<?php echo $content_title; ?>;
}
a {
	color:<?php echo $secondary_color; ?>;
}
a:hover,
a:focus {
	color:<?php echo $main_color; ?>;
}
#preloader-container {
	background:<?php echo $content_background; ?>;
}
.cssload-thecube .cssload-cube:before {
	background:<?php echo $main_color; ?>;
}
.flownews-top-menu li a {
    color: <?php echo $header_top_text; ?>;
}
.flownews-top-menu li a:hover {
    color: <?php echo $main_color; ?>;
}
.header-mobile .flownews-logo {
	background:<?php echo $header_bottom_background; ?>;
}
.header-mobile .news-ticker-item .news-ticker-item-title a {
    color: <?php echo $header_top_text; ?>;
}
.header-mobile .flownews-ticker {
    background: <?php echo $header_top_background; ?>;
    border-top: 1px solid <?php echo $header_top_line; ?>;
}
.flownews-header-sticky {
	background: <?php echo $header_bottom_background; ?>;
	border-bottom: 2px solid <?php echo $main_color; ?>;
}
.flownews-header-sticky .flownews-wrap-container {
	background:<?php echo $header_bottom_background; ?>;
}
.flownews-header-sticky nav > ul {
	border-right:1px solid <?php echo $header_bottom_line; ?>;
}
nav > ul {
	border-left: 1px solid <?php echo $header_bottom_line; ?>;
}
.flownews-rtl nav > ul {
	border-right: 1px solid <?php echo $header_bottom_line; ?>;
}
nav ul li.current_page_item,
nav ul li.current-menu-item,
nav li ul.submenu li.current-menu-item,
nav ul li.current-menu-ancestor,
nav li ul.submenu li.current-menu-ancestor {
    background: <?php echo $main_color; ?>;	
}
nav ul li.current_page_item > a,
nav ul li.current-menu-item > a,
nav li ul.submenu li.current-menu-item > a,
nav ul li.current-menu-ancestor > a,
nav li ul.submenu li.current-menu-ancestor > a {
	color:<?php echo $header_bottom_text_menu; ?>;
}
nav ul li a {
	color:<?php echo $header_bottom_main_text_menu; ?>;
}
nav ul li a:hover {
	color:<?php echo $header_bottom_text_menu; ?>;
	background: <?php echo $main_color; ?>;
}
ul.submenu,
.submenu {
	border:1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-top:2px solid <?php echo $main_color; ?>;
}
ul.submenu .submenu {
	border-top:1px solid <?php echo $header_bottom_border_submenu; ?>;
}
ul.submenu li:first-child .submenu {
	border-top:1px solid <?php echo $main_color; ?>;
}
nav li ul.submenu li a {
	color:<?php echo $header_bottom_text_submenu; ?>;
}
nav li ul.submenu li a:hover {
	color:<?php echo $header_bottom_text_menu; ?>;
}
nav li ul.submenu li {
	background:<?php echo $header_bottom_background_submenu; ?>;
	border-bottom:1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.open-menu-responsive,
.close-menu-responsive {
	color:<?php echo $header_bottom_main_text_menu; ?>;
	background:<?php echo $main_color; ?>;
}
.menu-responsive > ul {
	border-bottom:0;
}
.menu-responsive li a {
	color:<?php echo $header_bottom_text_submenu; ?>;
}
.menu-responsive li a:hover {
	color:<?php echo $main_color; ?>;
}
.header-mobile .menu-responsive-container .submenu {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-left:0;
	border-right:0;
}
.menu-responsive li {
	background: <?php echo $header_bottom_border_submenu; ?>;
    border-bottom: 1px solid <?php echo $header_bottom_background_submenu; ?>;
}
.header-mobile .menu-responsive-container .submenu > li.menu-item-has-children > a:after {
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-menu .menu-item-object-category .flownews-mega-menu {
	background:<?php echo $header_bottom_background_submenu; ?>;
	border-bottom: 2px solid <?php echo $main_color; ?>;
}
.flownews-mega-menu .flownews-menu-element-posts-container .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-mega-menu .flownews-menu-category-all-category-posts i {
	background:<?php echo $main_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews-menu-category-all-category-posts {
	border-top:1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-menu-category-all-category-posts .flownews-link-menu-category a {
	color:<?php echo $header_bottom_text_submenu; ?>;
}
.flownews-menu-category-all-category-posts .flownews-link-menu-category:hover a:first-child {
	color:<?php echo $main_color; ?>;
}
.flownews-mega-menu .flownews-menu-category.owl-carousel.owl-theme .owl-controls .owl-nav [class*="owl-"] {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	background:none;
	color: <?php echo $header_bottom_text_submenu; ?>;
}
.flownews-mega-menu .flownews-element-posts.owl-carousel.owl-theme article .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-mega-menu .flownews-menu-category.owl-carousel.owl-theme .owl-controls .owl-nav .owl-prev:hover,
.flownews-mega-menu .flownews-menu-category.owl-carousel.owl-theme .owl-controls .owl-nav .owl-next:hover {
	background:<?php echo $main_color; ?>;
	border:1px solid <?php echo $main_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews-menu .flownews-element-posts .article-title a {
	color:<?php echo $header_bottom_text_submenu; ?>;
}
.flownews-search button {
	background: <?php echo $main_color; ?>;
}
.flownews-search button:hover {
	background: <?php echo $secondary_color; ?>;
}
.flownewsicon.fa-search {
	color: <?php echo $header_bottom_main_text_menu; ?>;
}
.flownewsicon.fa-search:hover, .flownewsicon.fa-close {
    color:<?php echo $header_bottom_text_menu; ?>;
}
 .flownewsicon.fa-close {
    background:<?php echo $main_color; ?>;
}
.flownewsicon.fa-search:hover {
	background:<?php echo $main_color; ?>;
}
.flownewsicon.fa-search:focus {
	background:<?php echo $main_color; ?>;
}
.flownews-search-menu-button {
    border-right: 1px solid <?php echo $header_bottom_line; ?>;
}
.flownews-rtl .flownews-search-menu-button {
    border-left: 1px solid <?php echo $header_bottom_line; ?>;
}
.flownews-search form {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	background:<?php echo $header_bottom_background_submenu; ?>;
}
.flownews-search form {
	border-top:0;
}
.form-group-search {
    background: <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-title-page-container {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $main_color; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
 }
.flownews-title-page {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-category-description {
	background: <?php echo $content_navigation_background; ?>;
}
.author-post-container .author-post {
    background: <?php echo $content_navigation_background; ?>;
}
.author-post-container .author-name a {
    color: <?php echo $content_title; ?>;
}
.author-post-container .author-description,
.flonews-login-register .flonews-login-register-logged a,
.flonews-login-register .flonews-login-register-logout a {
	color: <?php echo $content_text; ?>;
}
.flonews-login-register .flonews-login-register-logged a:hover,
.flonews-login-register .flonews-login-register-logout a:hover {
	color: <?php echo $main_color; ?>;
}
.author-post-container .author-social i {
    color: <?php echo $content_title; ?>;
}
.author-post-container .author-social i:hover,
.author-post-container .author-name a:hover {
    color: <?php echo $main_color; ?>;
}
.not-found input {
    border: none;
}
.not-found input.search-submit {
    background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.not-found input.search-submit:hover {
    background: <?php echo $main_color; ?> !important;
}
.search-not-found input {
	border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
/*
.flownews-posts-content-wrap .article-title a,
.flownews-posts-image-wrap .article-title a {
    color: <?php echo $content_post; ?>;
}
.flownews-posts-content-wrap .article-title a:hover,
.flownews-posts-image-wrap .article-title a:hover {
	color:<?php echo $main_color; ?>;
}	
.flownews-posts-content-wrap .article-category a,
.flownews-posts-image-wrap .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $content_post; ?>;
}
.flownews-posts-content-wrap .article-category a:hover,
.flownews-posts-image-wrap .article-category a:hover {
	background:<?php echo $main_color; ?>;
}*/
.article-info.flownews-post-title-page .article-info-top h2 {
	color:<?php echo $content_title; ?>;
}
.flownews-wrap-container .article-info-top h2 {
    color: <?php echo $content_post; ?>;
}
.flownews-posts-content-wrap .article-info-bottom,
.flownews-posts-image-wrap .article-info-bottom {
    color: <?php echo $content_post; ?>;
}
.article-info.flownews-post-title-page .article-info-bottom {
    color: <?php echo $content_text_info; ?>;
}
.flownews-posts-content-wrap .article-info-bottom a,
.flownews-posts-image-wrap .article-info-bottom a {
	color:<?php echo $content_post; ?>;
}
.article-info.flownews-post-title-page .article-info-bottom a {
	color:<?php echo $content_title; ?>;
}
.flownews-posts-content-wrap .article-info-bottom a:hover,
.flownews-posts-image-wrap .article-info-bottom a:hover,
.article-info.flownews-post-title-page .article-info-bottom a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-post .social-post a {
    border: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $main_color; ?>;
}
.navigation-post .prev-post {
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.navigation-post .next-post {
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.navigation-post .prev-post-text,
.navigation-post .next-post-text {
	color:<?php echo $content_text_info; ?>;
}
.navigation-post .prev-post-text:hover,
.navigation-post .next-post-text:hover {
	color:<?php echo $content_title; ?>;
}
.navigation-post .prev-post-text i {
    color: <?php echo $content_title; ?>;
}
.navigation-post .next-post-text i {
    color: <?php echo $content_title; ?>;
}
.navigation-post .name-post {
    color: <?php echo $content_title; ?>;
}
.navigation-post .name-post:hover {
    color: <?php echo $main_color; ?>;
}
@media screen and (min-width: 500px) and (max-width: 700px) {
	.fnwp-widget.flownews_widget.fnwp_mega_posts .box_post:last-child,
	.widget.fnwp-widget.flownews_widget.fnwp_tab .box_post:last-child {
		border-bottom:1px solid <?php echo $content_navigation_background; ?>;
	}
	.footer-widget .fnwp-widget.flownews_widget.fnwp_mega_posts .box_post,
	.footer-widget .widget.fnwp-widget.flownews_widget.fnwp_tab .box_post {
		border-bottom:0;
	}
}
@media screen and (max-width: 1024px) {
	.flownews-header-wrap-container.header-mobile {
		background: <?php echo $header_bottom_background; ?>;
	}
	.flownews-menu > li.menu-item-has-children > a::after {
		color: <?php echo $header_bottom_text_submenu; ?>;
	}
	.flownews-wrap-container .flownews-top-menu.col-sm-3,
	.flownews-wrap-container .flownews-date.col-sm-3 {
		border-top:1px solid <?php echo $header_bottom_line; ?>;
	}
	ul.submenu li:first-child .submenu {
		border-top:1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
@media screen and (max-width: 700px) {
	.flownews-footer-top .flownews-wrap-container .footer-widget.col-xs-4 {
		border-bottom: 1px solid <?php echo $footer_top_line; ?>;
	}
	.flownews-header-wrap-container .flownews-footer-bottom .flownews-wrap-container .col-xs-4 {
		border-bottom: 1px solid <?php echo $footer_bottom_line; ?>;
	}
	.flownews-container .flownews-element-posts.flownews-posts-layout2.flownews-blog-2-col .item-posts.first-element-posts:nth-child(2) {
		border-top: 1px solid <?php echo $footer_top_line; ?>;
	}
}
.comment-form-email.col-xs-4 input, 
.comment-form-url.col-xs-4 input, 
.comment-form-author.col-xs-4 input {
    background: <?php echo $content_navigation_background; ?>;
}
.comment-form-comment textarea {
    background: <?php echo $content_navigation_background; ?>;
}
.comment-form .submit,
.wpcf7-submit {
    background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.comment-form .submit:hover,
.wpcf7-submit:hover {
    background: <?php echo $secondary_color; ?>;
}
.comment-form-title,
.comment-reply-title {
    border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
    border-right: 1px solid <?php echo $main_color; ?>;
    border-top: 1px solid <?php echo $content_navigation_background; ?>;
}
.comment-form-title h3,
.comment-reply-title .title-leave-a-comment {
    background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.comments-list {
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.children .comments-list:before {
  border-top: 1px solid <?php echo $main_color; ?>;
}
.comment-date {
	color: <?php echo $main_color; ?>;
}
.comment-description i {
	color: <?php echo $main_color; ?>;
}
.comment-description i:hover {
	color: <?php echo $main_color; ?>;
}
.comment-edit-link {
    color: <?php echo $main_color; ?>;
}
.comment-edit-link:hover {
    color: <?php echo $main_color; ?>;
}
#commentform .logged-in-as a {
	color: <?php echo $content_title; ?>;
}
#commentform .logged-in-as a:hover {
	color: <?php echo $main_color; ?>;
}
.news-ticker-item .news-ticker-item-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu;; ?>;
}
.news-ticker-item .news-ticker-item-category a:hover {
	background:<?php echo $secondary_color; ?>;
}
.news-ticker-item .news-ticker-item-title a {
	color: <?php echo $header_top_text; ?>;
}
.news-ticker-item .news-ticker-item-title a:hover {
	color: <?php echo $main_color; ?>;
}
.news-ticker-item .news-ticker-item-date {
	color: <?php echo $header_top_text; ?>;
}
.flownews-top-news-ticker.owl-theme .owl-controls .owl-nav [class*="owl-"] {
    color: <?php echo $header_top_text; ?>;
}
.flownews-top-news-ticker.owl-theme .owl-controls .owl-nav [class*="owl-"]:hover {
    color: <?php echo $main_color; ?>;
}
.flownews-header-top .col-sm-1, .flownews-header-top .col-sm-2, 
.flownews-header-top .col-sm-3, .flownews-header-top .col-sm-4, 
.flownews-header-top .col-sm-5, .flownews-header-top .col-sm-6, 
.flownews-header-top .col-sm-7, .flownews-header-top .col-sm-8, 
.flownews-header-top .col-sm-9, .flownews-header-top .col-sm-10, 
.flownews-header-top .col-sm-11, .flownews-header-top .col-sm-12 {
	border-right:1px solid <?php echo $header_top_line; ?>;
}
.flownews-header-top > div:first-child {
	border-left:1px solid <?php echo $header_top_line; ?>;
}
<?php if($flownews_theme['header-top-align'] != 'default') : ?>
	.flownews-header-top .flownews-wrap-container > div {
		text-align:<?php echo $flownews_theme['header-top-align']; ?>;
	}
<?php endif; ?>
.flownews-date,
.flonews-login-register .flonews-login-register-logged {
    color: <?php echo $header_top_text; ?>;
}
.flownews-social .flownews-header-top-social a {
    color: <?php echo $header_top_text; ?>;
}
.flownews-social .flownews-header-top-social a:hover {
	color:<?php echo $main_color; ?>;
}
.tags-container a,
.tagcloud a {
	color: <?php echo $content_title; ?>;
}
.tags-container a:hover,
.tagcloud a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-sidebar .widget.widget_archive li, 
.flownews-sidebar .widget.widget_categories li,
.flownews-sidebar .widget.widget_nav_menu li,
.wpb_wrapper .widget.widget_nav_menu li,
.flownews-sidebar .widget.widget_meta li,
.wpb_wrapper .widget.widget_meta li,
.flownews-sidebar .widget.widget_pages li,
.wpb_wrapper .widget.widget_pages li,
.flownews-sidebar .widget.widget_recent_comments li,
.wpb_wrapper .widget.widget_recent_comments li,
.flownews-sidebar .widget.widget_recent_entries li,
.wpb_wrapper .widget.widget_recent_entries li,
.flownews-sidebar .widget.widget_rss li,
.wpb_wrapper .widget.widget_rss li {
	border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-sidebar .widget.widget_archive li a:hover, 
.flownews-sidebar .widget.widget_categories li a:hover,
.flownews-sidebar .widget.widget_nav_menu li a:hover,
.wpb_wrapper .widget.widget_nav_menu li a:hover,
.flownews-sidebar .widget.widget_meta li a:hover,
.wpb_wrapper .widget.widget_meta li a:hover,
.flownews-sidebar .widget.widget_pages li a:hover,
.wpb_wrapper .widget.widget_pages li a:hover,
.flownews-sidebar .widget.widget_recent_comments li a:hover,
.wpb_wrapper .widget.widget_recent_comments li a:hover,
.flownews-sidebar .widget.widget_recent_entries li a:hover,
.wpb_wrapper .widget.widget_recent_entries li a:hover,
.flownews-sidebar .widget.widget_rss li a:hover,
.wpb_wrapper .widget.widget_rss li a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-sidebar .widget.widget_rss li a,
.wpb_wrapper .widget.widget_rss li a {
	color:<?php echo $content_title; ?>;
}
.flownews-sidebar .widget.widget_recent_entries li .post-date,
.wpb_wrapper .widget.widget_recent_entries li .post-date {
	color:<?php echo $content_text_info; ?>;
}
.widget li a,
.widget_rss .rsswidget:hover {
	color:<?php echo $content_title; ?>;
}
.widget li a:hover,
.widget_rss .rsswidget {
	color:<?php echo $main_color; ?>;
}
.widget_rss .widget-title .rsswidget {
	color:<?php echo $header_bottom_text_menu; ?>;
}
.widget.widget_recent_comments .comment-author-link {
    color: <?php echo $content_text_info; ?>;
}
.widget select {
    border: 1px solid <?php echo $content_navigation_background; ?>;	
}
.widget_search input {
	border: 0;		
}
.widget_search label input {
	border-color: <?php echo $content_navigation_background; ?>;
	background: <?php echo $content_navigation_background; ?>;
}
.flownews-post-sticky .article-info {
	background: <?php echo $content_navigation_background; ?>;
}
.flownews-sidebar .search-form .search-submit,
.wpb_wrapper .search-form .search-submit {
    background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-sidebar .search-form .search-submit:hover,
.wpb_wrapper .search-form .search-submit:hover {
    background: <?php echo $secondary_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.widget_search input.search-submit:hover {
	background:<?php echo $secondary_color; ?>;	
}
.post-password-form input[type="submit"]:hover,
.not-found input[type="submit"]:hover {
	background:<?php echo $secondary_color; ?>;		
}
#wp-calendar #prev a {
	color:<?php echo $main_color; ?>;
}
#wp-calendar tr #today {
	color:<?php echo $main_color; ?>;
}
#wp-calendar #prev a:hover {
	color:<?php echo $secondary_color; ?>;	
}
.widget ul > li.menu-item-has-children > a:after {
    color: <?php echo $content_title; ?>;	
}
.fnwp-widget h4.widget-title,
.fnwp-widget h3.widget-title,
.widget h3 {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $main_color; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.fnwp-title-widget {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
/*
@media only screen and (min-width : 801px) and (max-width : 1200px) {
	.flownews_widget.fnwp_advertisement .active {
		border: 1px solid;
	}
	.flownews_widget.fnwp_advertisement .mini-post.small-post.ad_one_third {
		border: 1px solid;
	}
	.flownews_widget.fnwp_advertisement .mini-post.small-post.ad_one_third.fourth {
		border-bottom: 1px solid !important;
	}
}
@media only screen and (max-width : 800px) {
	.flownews_widget.fnwp_advertisement .mini-post.small-post.ad_one_third {
		border:1px solid;
	}
	.flownews_widget.fnwp_advertisement .mini-post.small-post.ad_one_third.fourth {
		border-bottom: 1px solid !important;
	}
}*/
.flownews_widget.fnwp_archivies .box_archivies .box_archivies_item {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_archivies a,
.comment-name {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_archivies .box_archivies_item:hover a {
	color:<?php echo $main_color; ?>;	
}
.flownews_widget.fnwp_archivies .box_archivies .box_archivies_item:hover {
	color:<?php echo $main_color; ?>;
}
.flownews_widget.fnwp_archivies .number-post {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_archivies .box_archivies .box_archivies_item:hover {
	color:<?php echo $main_color; ?>;
}
.flownews_widget.fnwp_categories .box_categories .cat-item {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_categories a {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_categories .box_categories > li.cat-item:hover,
.flownews_widget.fnwp_categories .box_categories > li.cat-item:hover > a {
	color:<?php echo $main_color; ?>;
}
.flownews_widget.fnwp_categories .number-post {
    color: <?php echo $content_title; ?>;
}
.box_categories li .children .cat-item a:hover {
	color:<?php echo $main_color; ?>;
}
.box_categories li .children .cat-item:hover {
	color:<?php echo $main_color; ?>;
}
.flownews_widget.fnwp_mega_posts .box_post {
    border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews_widget.fnwp_mega_posts .container_post.ad_one_one .box-info h4 a {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_mega_posts .container_post.ad_one_one .box-info a:hover {
    color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_mega_posts .container_post.ad_one_one .data {
    color: <?php echo $content_text_info; ?>;
}
.flownews_widget.fnwp_mega_posts .container_post.ad_one_one .data i {
	color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_mega_posts .container_post.ad_one_one .icon-calendar {
    color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_slider_posts .category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_slider_posts .category a:hover {
	background:<?php echo $secondary_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_slider_posts .box-text h3 a {
	color: <?php echo $content_post; ?>;
}
.flownews_widget.fnwp_slider_posts .box-text h3 a:hover {
	color:<?php echo $main_color; ?> ;
}
.flownews_widget.fnwp_slider_posts .data {
	color: <?php echo $content_post; ?>;
}
.flownews_widget.fnwp_slider_posts .data:hover {
	color: <?php echo $content_post; ?>;
}
.flownews_widget.fnwp_slider_posts .icon-calendar {
	color: <?php echo $content_post; ?>;
}
.flownews_widget.fnwp_slider_posts .flownews_widget-item.ad_one_one.ad_last.big-post i {
	color: <?php echo $content_post; ?>;
}
.flownews_widget.fnwp_slider_posts .owl-theme .owl-controls .owl-nav [class*="owl-"] {
	background: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_slider_posts .owl-theme .owl-controls .owl-nav [class*="owl-"] {
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_slider_posts .owl-theme .owl-controls .owl-nav [class*="owl-"]:hover {
	background: <?php echo $secondary_color; ?>;
}
.mc4wp-form-fields input[type="submit"] {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.mc4wp-form-fields input[type="email"] {
	border:1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.mc4wp-form-fields input[type="submit"]:hover {
    background: <?php echo $secondary_color; ?>;
}
.flownews-widget-social-style2 a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-widget-social-style2 a:hover {
	background:<?php echo $secondary_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_tab .widget-title .fnwp-title-widget span:hover {
	background:<?php echo $secondary_color; ?>;	
}
.flownews_widget.fnwp_tab .fnwp_tab_active {
	background:<?php echo $secondary_color; ?> !important;	
}
.fnwp_tab .widget-title .fnwp-title-widget span {
    background: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tab .box_post {
    border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews_widget.fnwp_tab .container_post.ad_one_one .box-info h4 a {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tab .container_post.ad_one_one .box-info a:hover {
    color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tab .container_post.ad_one_one .data {
    color: <?php echo $content_text_info; ?>;
}
.flownews_widget.fnwp_tab .container_post.ad_one_one .icon-calendar {
    color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tab .content_tag {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tab .content_tag a {
	color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tab .content_tag:hover {
    background: <?php echo $main_color; ?>;
	border: 1px solid <?php echo $main_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_tab .content_tag:hover a {
	color:<?php echo $header_bottom_text_menu; ?>;
}
h4.widget-title span:hover {
	background:<?php echo $secondary_color; ?>;	
}
.fnwp_tag .widget-title .fnwp-title-widget span {
    background: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tag .box_post {
    border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
}
.flownews_widget.fnwp_tag .container_post.ad_one_one .box-info h4 a {
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tag .container_post.ad_one_one .box-info a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tag .container_post.ad_one_one .data {
    color: <?php echo $content_text_info; ?>;
}
.flownews_widget.fnwp_tag .container_post.ad_one_one .icon-calendar {
    color: <?php echo $main_color; ?>;
}
.flownews_widget.fnwp_tag .content_tag {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tag .content_tag a {
	color: <?php echo $content_title; ?>;
}
.flownews_widget.fnwp_tag .content_tag:hover {
    background: <?php echo $main_color; ?>;
	border: 1px solid <?php echo $main_color; ?>;
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews_widget.fnwp_tag .content_tag:hover a {
	color:<?php echo $header_bottom_text_menu; ?>;
}
.flownews-element-posts .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-element-posts .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-element-posts .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
}
.flownews-element-posts .article-data {
	color: <?php echo $content_text_info; ?>;
}
.flownews-element-posts .article-comments {
	color: <?php echo $content_text_info; ?>;	
}
.flownews-element-posts .article-comments a {
	color: <?php echo $content_text_info; ?>;
}
.flownews-element-posts .article-comments a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-element-posts .article-info-bottom .article-excerpt a {
	color:<?php echo $main_color; ?>;
}
.flownews-element-posts .article-info-bottom .article-excerpt a:hover {
	color:<?php echo $secondary_color; ?>;
}
.flownews-element-posts-title-box {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $main_color; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-element-posts-title-box h2 {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-element-posts #flownews-load-posts a {
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-element-posts #flownews-load-posts a:hover {
    background: <?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.flownews-pagination a {
    border: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-pagination a:hover {
    background: <?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.fnwp-numeric-pagination .current {
	background:<?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.fnwp-numeric-pagination i {
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.fnwp-numeric-pagination i:hover {
	background:<?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.flownews-element-posts.flownews-posts-layout2 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $content_navigation_background; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-element-posts.flownews-posts-layout2 .flownews-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-3-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-4-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-3-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-3-col .item-posts.first-element-posts:nth-child(4),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (min-width: 580px) and (max-width: 950px) {
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout2.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}
}
.flownews-element-posts.flownews-posts-layout3 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $content_navigation_background; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-element-posts.flownews-posts-layout3 .flownews-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-2-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-3-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-3-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (min-width: 580px) and (max-width: 950px) {
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}	
}
.flownews-element-posts.flownews-posts-layout4 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $content_navigation_background; ?>;
}
@media screen and (max-width: 800px) {
	.flownews-element-posts.flownews-posts-layout4 article.item-posts.first-element-posts:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-2-col .item-posts.first-element-posts:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (max-width: 700px) {
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-3-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-3-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (min-width: 701px) and (max-width: 950px) {
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (max-width: 640px) {
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-3-col .item-posts.first-element-posts:nth-child(2),
	.flownews-element-posts.flownews-posts-layout4.flownews-blog-3-col .item-posts.first-element-posts:nth-child(3),
	.flownews-container .flownews-element-posts.flownews-posts-layout4.flownews-blog-4-col .item-posts.first-element-posts:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (min-width: 580px) and (max-width: 950px) {
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(3),
	.flownews-element-posts.flownews-posts-layout3.flownews-blog-4-col .item-posts.first-element-posts:nth-child(4) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}
}
.flownews-element-posts.flownews-posts-layout5 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $content_navigation_background; ?>;
}
@media screen and (max-width: 1000px) {
	.flownews-element-posts.flownews-posts-layout5.flownews-blog-2-col article.item-posts.first-element-posts:nth-child(5) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (max-width: 700px) {
	.flownews-element-posts.flownews-posts-layout5 article.item-posts.first-element-posts:nth-child(2) {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.flownews-element-posts.flownews-posts-layout5.flownews-blog-2-col  article.item-posts.first-element-posts:nth-child(5) {
		border-top:1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (max-width: 500px) {
	.flownews-element-posts.flownews-posts-layout5 article.item-posts.first-element-posts.other-rows {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
@media screen and (max-width: 900px) {
	.flownews-element-posts.flownews-posts-layout5.flownews-blog-3-col  article.item-posts.first-element-posts.first-row,
	.flownews-element-posts.flownews-posts-layout5.flownews-blog-3-col  article.item-posts.first-element-posts.other-rows {
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
	}
}
.flownews-element-top-content .article-title a,
.flownews-vc-element-posts-carousel.flownews-posts-carousel-type2 .article-title a {
    color: <?php echo $content_post; ?>;
}
.flownews-element-top-content .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-element-top-content .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-element-top-content .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-element-top-content .article-info-bottom,
.flownews-vc-element-posts-carousel.flownews-posts-carousel-type2 .article-data,
.flownews-vc-element-posts-carousel.flownews-posts-carousel-type2 .article-info-bottom,
.flownews-vc-element-posts-carousel.flownews-posts-carousel-type2 .article-comments {
    color: <?php echo $content_post; ?>;
}
.flownews-element-top-content .article-info-bottom a {
	color:<?php echo $content_post; ?>;
}
.flownews-element-top-content .article-info-bottom a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-top-content-layout1 .article-title a {
    color: <?php echo $content_post; ?>;
}
.flownews-top-content-layout1 .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-top-content-layout1 .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-top-content-layout1 .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-top-content-layout1 .article-info-bottom {
    color: <?php echo $content_post; ?>;
}
.flownews-top-content-layout1 .article-info-bottom a {
	color:<?php echo $content_post; ?>;
}
.flownews-top-content-layout1 .article-info-bottom a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-top-content-layout3 .others-element-header {
	background:<?php echo $content_navigation_background; ?>;
}
.flownews-top-content-layout3 .others-element-header .article-title a {
	color:<?php echo $content_title; ?>;	
}
.flownews-top-content-layout3 .others-element-header .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-top-content-layout4 .owl-controls i {
	background: <?php echo $content_background; ?>;
	color: <?php echo $content_title; ?>;
}
.flownews-top-content-layout4 .owl-controls i:hover {
	color: <?php echo $main_color; ?>;
}
<?php if($flownews_theme['footer-image-background-active'] == true) : ?>
	.flownews-footer-wrap .flownews-footer-top {
		background: url('<?php echo $flownews_theme['footer-bg-image']['url']; ?>');
		background-size:cover;
		position:relative;
		background-position: center;
	}
	.flownews-footer-wrap .flownews-footer-top .flownews-wrap-container {
		background: none;
		position:relative;
		z-index:2;
	}
	.flownews-banner-footer {
		position:relative;
		z-index:2;		
	}
	.flownews-footer-wrap .flownews-footer-top .footer-top-pattern {
		background: <?php echo $flownews_theme['footer-pattern-color']['rgba']; ?>;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index:1;
	}
<?php else : ?>
	.flownews-footer-wrap .flownews-footer-top,
	.flownews-footer-wrap .flownews-footer-top .flownews-wrap-container {
		background: <?php echo $footer_top_background; ?>;
	}
<?php endif; ?>
.flownews-footer-top h3.widget-title,
.flownews-footer-top h3.widget-title a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_archivies .box_archivies .box_archivies_item,
.flownews-footer-top .flownews_widget.fnwp_categories .box_categories .cat-item,
.flownews-footer-top .widget.widget_nav_menu .menu-item,
.flownews-footer-wrap .flownews-footer-top .widget li,
.flownews-footer-top .flownews_widget_contact .flownews-widget-contact-text {
	border-bottom:1px solid <?php echo $footer_top_line; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_archivies a,
.flownews-footer-top .flownews_widget.fnwp_archivies .box_archivies .box_archivies_item,
.flownews-footer-top .flownews_widget.fnwp_categories a,
.flownews-footer-top .flownews_widget.fnwp_categories .box_categories .cat-item,
.flownews-footer-top .widget.widget_nav_menu .menu-item a,
.flownews-footer-wrap .flownews-footer-top .widget li a,
.flownews-footer-top .widget ul > li.menu-item-has-children > a:after {
	color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_archivies a:hover,
.flownews-footer-top .flownews_widget.fnwp_archivies .box_archivies .box_archivies_item:hover,
.flownews-footer-top .widget.widget_nav_menu .menu-item a:hover,
.flownews-footer-wrap .flownews-footer-top .widget li a:hover,
.flownews-footer-top .widget ul > li.menu-item-has-children:hover > a:after  {
	color: <?php echo $main_color; ?>;
}
.flownews-footer-top #wp-calendar {
	color:<?php echo $footer_top_title; ?>;
}
.flownews-footer-top caption {
    border: 1px solid <?php echo $footer_top_line; ?>;
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_mega_posts .box_post,
.flownews-footer-top .flownews_widget.fnwp_tab .box_post {
    border-bottom: 1px solid <?php echo $footer_top_line; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_mega_posts .container_post.ad_one_one .box-info h4,
.flownews-footer-top .flownews_widget.fnwp_mega_posts .container_post.ad_one_one .box-info h4 a,
.flownews-footer-top .flownews_widget.fnwp_tab .container_post.ad_one_one .box-info h4 a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_mega_posts .container_post.ad_one_one .box-info h4 a:hover,
.flownews-footer-top .flownews_widget.fnwp_tab .container_post.ad_one_one .box-info h4 a:hover {
    color: <?php echo $main_color; ?>;
}
.flownews-footer-top,
.flownews-footer-top p,
.flownews-footer-top .flownews_widget.fnwp_mega_posts .container_post.ad_one_one .data {
    color: <?php echo $footer_top_text; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_tag .content_tag,
.flownews-footer-top .flownews_widget.fnwp_tab .content_tag {
    border: 1px solid <?php echo $footer_top_line; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_tag .content_tag a,
.flownews-footer-top .flownews_widget.fnwp_tab .content_tag a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_tag .content_tag:hover a,
.flownews-footer-top .flownews_widget.fnwp_tab .content_tag:hover a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_tag .content_tag:hover,
.flownews-footer-top .flownews_widget.fnwp_tab .content_tag:hover {
    border:1px solid <?php echo $main_color; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_social .box-icon-social a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .widget.widget_meta li a {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .widget.widget_meta li a:hover {
    color: <?php echo $main_color; ?>;
}
.flownews-footer-top .widget.widget_meta ul li {
    border-bottom: 1px solid <?php echo $footer_top_line; ?>;
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .fnwp_tab h3.widget-title {
    border: 1px solid <?php echo $footer_top_line; ?>;
}
.flownews-footer-top .fnwp_tab .fnwp_title_recent,
.flownews-footer-top .fnwp_tab .fnwp_title_popular,
.flownews-footer-top .fnwp_tab .fnwp_title_tag {
    border-right: 1px solid <?php echo $footer_top_line; ?>;
}
.flownews-footer-top .fnwp_tab span:hover {
	background:<?php echo $main_color; ?>;
}
.flownews-footer-top .flownews_widget.fnwp_tab .fnwp_tab_active {
	background:<?php echo $main_color; ?> !important;
	color:<?php echo $footer_top_title; ?> !important;	
}
.flownews-footer-top .widget.widget_recent_comments {
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top .widget label {
	background:<?php echo $footer_top_title; ?>;
}
.flownews-footer-top .widget select {
	background: <?php echo $footer_top_title; ?>;
    border: 1px solid <?php echo $footer_top_line; ?>;	
}
.flownews-footer-top  .search-form .search-submit {
    background: <?php echo $main_color; ?>;
    color: <?php echo $footer_top_title; ?>;
}
.flownews-footer-top  .search-form .search-submit:hover {
    background: <?php echo $secondary_color; ?>!important;
}
.flownews-footer-top .widget_search input.search-submit:hover {
	color:<?php echo $footer_top_title; ?>;	
}
.flownews-footer-top .widget_tag_cloud .tagcloud a {
	color:<?php echo $footer_top_title; ?>;
}
.flownews-footer-top .widget_text .textwidget {
	color: <?php echo $footer_top_text; ?>;
}
.flownews-footer-bottom .flownews-wrap-container {
	background:<?php echo $footer_bottom_background; ?>;
}
.flownews-footer-bottom {
    background: <?php echo $footer_bottom_background; ?>;
}
.flownews-footer-bottom .flownews-footer-social a,
.flownews-footer-bottom .copyright {
    color: <?php echo $footer_bottom_text; ?>;
}
.flownews-footer-bottom .col-xs-4:nth-child(2) {
	color: <?php echo $footer_bottom_text; ?>;
	border-left: 1px solid <?php echo $footer_bottom_line; ?>;
    border-right: 1px solid <?php echo $footer_bottom_line; ?>;
}
.flownews-footer-bottom .col-xs-4:nth-child(3) {
	color: <?php echo $footer_bottom_text; ?>;
}
.flownews-footer-bottom .flownews-top-menu li a {
    color: <?php echo $footer_bottom_text; ?>;
}
.flownews-footer-bottom a:hover {
    color: <?php echo $main_color; ?> !important;
}
.backtotop .flownewsicon.fa-angle-up {
    background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.backtotop .flownewsicon.fa-angle-up:hover {
    background: <?php echo $secondary_color; ?>;
}




.flownews-vc-element-header .article-title a {
    color: <?php echo $content_post; ?>;
}
.flownews-vc-element-header .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-header .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-header .article-category a:hover {
	background: <?php echo $secondary_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-header .article-info-bottom {
    color: <?php echo $content_post; ?>;
}
.flownews-vc-element-header .article-info-bottom a {
	color:<?php echo $content_post; ?>;
}
.flownews-vc-element-header .article-info-bottom a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-header-type1 .article-title a {
    color: <?php echo $content_post; ?>;
}
.flownews-header-type1 .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-header-type1 .article-category a {
	background: <?php echo $main_color; ?>;
    color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-header-type1 .article-category a:hover {
	background: <?php echo $secondary_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-header-type1 .article-info-bottom {
    color: <?php echo $content_post; ?>;
}
.flownews-header-type1 .article-info-bottom i {
	margin-right:10px;
}
.flownews-header-type1 .article-info-bottom a {
	color:<?php echo $content_post; ?>;
}
.flownews-header-type1 .article-info-bottom a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-header-type3 .others-element-header {
	background:<?php echo $content_navigation_background; ?>;
}
.flownews-header-type3 .others-element-header .article-title a {
	color:<?php echo $content_title; ?>;
}
.flownews-header-type3 .others-element-header .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-header-type4 .owl-controls i {
	background: <?php echo $content_background; ?>;
	color: <?php echo $content_title; ?>;
}
.flownews-header-type4 .owl-controls i:hover {
	color: <?php echo $main_color; ?>;
}





.flownews-vc-element-posts-carousel .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts-carousel .article-category a {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts-carousel .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
}
.flownews-vc-element-posts-carousel .article-data {
	color: <?php echo $content_text_info; ?>;	
}
.flownews-vc-element-posts-carousel .article-comments {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-carousel .article-comments a {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-carousel .article-comments a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-vc-element-posts-carousel .article-info-bottom .article-excerpt a {
	color:<?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-carousel .article-info-bottom .article-excerpt a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts-carousel-title-box {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $main_color; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-carousel-title-box h2 {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts-carousel #flownews-load-posts a {
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-vc-element-posts-carousel #flownews-load-posts a:hover {
    background: <?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.flownews-vc-element-posts-carousel.owl-theme .owl-controls .owl-nav [class*="owl-"] {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-vc-element-posts-carousel .owl-dot.active {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-carousel .owl-dot {
    border: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-carousel.owl-theme .owl-dots .owl-dot span {
    background: <?php echo $content_title; ?>;
}
.flownews-vc-element-posts-carousel.owl-theme .owl-dots .owl-dot.active span,
.flownews-vc-element-posts-carousel.owl-theme .owl-dots .owl-dot:hover span {
    background: <?php echo $main_color; ?>;
}
.flownews-vc-element-posts-carousel.owl-theme .owl-controls .owl-nav [class*="owl-"]:hover {
	background:<?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-vc-element-posts-carousel.flownews-posts-carousel-type1 .flownews-vc-element-posts-carousel-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts-tab article {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-tab .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts-tab .article-category a {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts-tab .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
}
.flownews-vc-element-posts-tab .article-data {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-tab .article-comments {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-tab .article-comments a {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-tab .article-comments a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-vc-element-posts-tab .article-info-bottom .article-excerpt a {
	color:<?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts-tab .article-info-bottom .article-excerpt a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts-tab-title-box {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-tab-title-box h2 {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts-tab-title-tabs span {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts-tab-title-tabs .fnwp_tab_active,
.flownews-vc-element-posts-tab-title-tabs span:hover  {
	background:<?php echo $secondary_color; ?>;
}
@media screen and (max-width: 800px) {
	.flownews-vc-element-posts-tab .fnwp-vc-element-posts-tab-container article:nth-child(2),
	.flownews-vc-element-posts-tab .fnwp-vc-element-posts-tab-container article:nth-child(3) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts .article-title a:hover {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts .article-category a {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts .article-category a:hover {
	background:<?php echo $secondary_color; ?>;
}
.flownews-vc-element-posts .article-data {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts .article-comments {
	color: <?php echo $content_text_info; ?>;	
}
.flownews-vc-element-posts .article-comments a {
	color: <?php echo $content_text_info; ?>;
}
.flownews-vc-element-posts .article-comments a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-vc-element-posts .article-info-bottom .article-excerpt a,
.flownews-vc-element-posts .article-excerpt a {
	color:<?php echo $main_color; ?>;
}
.flownews-vc-element-posts .article-info-bottom .article-excerpt a:hover,
.flownews-vc-element-posts .article-excerpt a:hover {
	color:<?php echo $secondary_color; ?>;
}
.flownews-vc-element-posts-title-box {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $main_color; ?>;
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-vc-element-posts-title-box h2 {
	background: <?php echo $main_color; ?>;
	color: <?php echo $header_bottom_text_menu; ?>;
}
.flownews-vc-element-posts #flownews-load-posts a {
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-vc-element-posts #flownews-load-posts a:hover {
    background: <?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.flownews-vc-pagination a {
    border: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-vc-pagination a:hover {
    background: <?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.fnwp-numeric-pagination .current {
	background:<?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.fnwp-numeric-pagination i {
	border:1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.fnwp-numeric-pagination i:hover {
	background:<?php echo $content_navigation_background; ?>;
	border:1px solid <?php echo $content_navigation_background; ?>;
}
.flownews-posts-type1 article.col-xs-4 {
	border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-right: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	border-left: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-posts-type1 article.col-xs-8 {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
.flownews-posts-type1 article.col-xs-4:nth-child(6) {
	border-bottom: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 900px) {
	.flownews-posts-type1 article.col-xs-4 {
		border-bottom:1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
	.flownews-posts-type1 article.col-xs-4 {
		border-left: 1px solid <?php echo $header_bottom_border_submenu; ?>;
		border-right: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts.flownews-posts-type3 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-vc-element-posts.flownews-posts-type3 .flownews-vc-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts.flownews-posts-type4 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-vc-element-posts.flownews-posts-type4 .flownews-vc-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts.flownews-posts-type5 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 800px) {
	.flownews-vc-element-posts.flownews-posts-type5 .flownews-vc-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
.flownews-vc-element-posts.flownews-posts-type6 .item-posts.first-element-posts {
    border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
}
@media screen and (max-width: 700px) {
	.flownews-vc-element-posts.flownews-posts-type6 .flownews-vc-element-posts-article-container article:nth-child(2) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
@media screen and (max-width: 400px) {
	.flownews-vc-element-posts.flownews-posts-type6 .flownews-vc-element-posts-article-container article:nth-child(3), 
	.flownews-vc-element-posts.flownews-posts-type6 .flownews-vc-element-posts-article-container article:nth-child(4) {
		border-top: 1px solid <?php echo $header_bottom_border_submenu; ?>;
	}
}
#flownews-user-modal .modal-content {
	border: 3px solid <?php echo $main_color; ?>;
	background: <?php echo $content_background; ?>;
}
#flownews_login_form label,
#flownews_login_form .alignright,
.flownews-register-footer,
#flownews-user-modal .close,
.flownews-register-footer > a,
#flownews_login_form a {
	color: <?php echo $content_title; ?>;
}
.flownews-register-footer > a:hover,
#flownews_login_form a:hover {
	color: <?php echo $main_color; ?>;
}
.tags-links > a {
	color: <?php echo $content_title; ?>
}
.tags-links > a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-post .social-post a:hover {
    border: 1px solid <?php echo $main_color; ?>;
    background: <?php echo $main_color; ?>;	
}
.flownews-post .social-post .container-social:before, .flownews-post .social-post .container-social:after {
	border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
}
ul.flownews_line li:before { 
	border-bottom:1px solid <?php echo $main_color; ?>;
}










<?php if ( class_exists( 'WooCommerce' ) ) { ?>
	.flownews-woocommerce-add-to-cart-container.col-sm-1:hover a,
	.flownews-woocommerce-add-to-cart-container.col-sm-1:hover a i {
		color:<?php echo $main_color; ?>;
	}
	.flownews-woocommerce-menu a i {
		color: <?php echo $content_post; ?>;
	}
	.flownews-woocommerce-menu a {
		color: <?php echo $content_post; ?>;
	}
	.woocommerce ul.products li.product .woocommerce-loop-product__title,
	.woocommerce ul.products li.product .price {
		color: <?php echo $content_title; ?>;
	}
	.woocommerce .star-rating {
		color: <?php echo $main_color; ?>;
	}
	.woocommerce #respond input#submit,
	.woocommerce a.button,
	.woocommerce button.button,
	.woocommerce input.button {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce input.button:hover {
		background: <?php echo $secondary_color; ?>;
		color:<?php echo $content_post; ?>;
	}
	.woocommerce span.onsale {
		background-color: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce span.onsale:hover {
		background: <?php echo $secondary_color; ?>;
	}
	.woocommerce nav.woocommerce-pagination ul li a:focus,
	.woocommerce nav.woocommerce-pagination ul li a:hover,
	.woocommerce nav.woocommerce-pagination ul li span.current {
		background: <?php echo $content_navigation_background; ?>;
		color: <?php echo $content_title; ?>;
	}
	.woocommerce nav.woocommerce-pagination ul li a,
	.woocommerce nav.woocommerce-pagination ul li span {
		color: <?php echo $content_title; ?>;
	}
	.single-product.woocommerce .woocommerce-review-link {
		color: <?php echo $content_text; ?>;
	}
	.single-product.woocommerce ins .woocommerce-Price-amount.amount {
		color: <?php echo $content_title; ?>;
	}
	.single-product.woocommerce div.product p.price,
	.single-product.woocommerce div.product span.price {
		color: <?php echo $content_title; ?>;
	}
	.single-product.woocommerce #respond input#submit.alt,
	.single-product.woocommerce a.button.alt,
	.single-product.woocommerce button.button.alt,
	.single-product.woocommerce input.button.alt {
		background-color: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.single-product.woocommerce #respond input#submit.alt:hover,
	.single-product.woocommerce a.button.alt:hover,
	.single-product.woocommerce button.button.alt:hover,
	.single-product.woocommerce input.button.alt:hover {
		background-color: <?php echo $secondary_color; ?>;
	}
	.single-product.woocommerce .quantity .qty {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.woocommerce div.product .woocommerce-tabs ul.tabs li.active {
		background:<?php echo $secondary_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce div.product .woocommerce-tabs ul.tabs li {
		background:<?php echo $main_color; ?>;
	}
	.woocommerce div.product .woocommerce-tabs ul.tabs li:hover {
		background:<?php echo $secondary_color; ?>;
	}
	.woocommerce div.product .woocommerce-tabs ul.tabs li a {
		color: <?php echo $content_post; ?>;
	}
	.woocommerce div.product .woocommerce-tabs ul.tabs li:hover a {
		color: <?php echo $content_post; ?>;
	}
	.single-product.woocommerce div.product .woocommerce-tabs ul.tabs {
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
		border-right:1px solid <?php echo $main_color; ?>;
	}
	.summary.entry-summary .woocommerce-Price-amount.amount {
		color: <?php echo $content_title; ?>;
	}
	.woocommerce #reviews #comments ol.commentlist li .comment-text {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.comment-reply-title {
		border-left:1px solid <?php echo $content_navigation_background; ?>;
		border-bottom:1px solid <?php echo $content_navigation_background; ?>;
		border-top:1px solid <?php echo $content_navigation_background; ?>;
		border-right:1px solid <?php echo $main_color; ?>;
	}
	.woocommerce-message {
		border-top-color:<?php echo $main_color; ?>;
	}
	.woocommerce-message::before {
		color:<?php echo $main_color; ?>;
	}
	.woocommerce table.shop_table td {
		border-top: 1px dashed <?php echo $content_navigation_background; ?>;
	}
	.woocommerce table.shop_table th {
		border-bottom: 2px solid <?php echo $content_navigation_background; ?>;
	}
	.woocommerce .product-name a {
		color: <?php echo $content_title; ?>;
	}
	.woocommerce .product-name a:hover {
		color: <?php echo $main_color; ?>;
	}
	.woocommerce .input-text.qty.text {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.woocommerce a.button.alt {
		background-color:<?php echo $main_color; ?>;
	}
	.woocommerce a.button.alt:hover {
		background-color:<?php echo $secondary_color; ?>;
	}
	.woocommerce .checkout .input-text {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.woocommerce-info::before {
		color: <?php echo $main_color; ?>;
	}
	.woocommerce-info {
		border-top-color: <?php echo $main_color; ?>;
	}
	.woocommerce #respond input#submit.alt,
	.woocommerce a.button.alt,
	.woocommerce button.button.alt,
	.woocommerce input.button.alt {
		background-color: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce #respond input#submit.alt:hover,
	.woocommerce a.button.alt:hover,
	.woocommerce button.button.alt:hover,
	.woocommerce input.button.alt:hover {
		background-color: <?php echo $secondary_color; ?>;
	}
	.woocommerce-account .flownews-post .flownews-content li,
	.woocommerce-account .flownews-post .flownews-content li,
	.woocommerce-account .flownews-page .flownews-content li,
	.woocommerce-account .flownews-page .flownews-content li {
		background: <?php echo $main_color; ?>;
	}
	.woocommerce-account .flownews-post .flownews-content li:hover,
	.woocommerce-account .flownews-post .flownews-content li:hover,
	.woocommerce-account .flownews-page .flownews-content li:hover,
	.woocommerce-account .flownews-page .flownews-content li:hover {
		background: <?php echo $secondary_color; ?>;
	}
	.woocommerce.widget_product_search input {
		border: 1px solid <?php echo $content_navigation_background; ?>;		
	}
	.woocommerce.widget_product_search input[type="submit"] {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce.widget_product_search input[type="submit"]:hover {
		background: <?php echo $secondary_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.woocommerce ul.product_list_widget li {
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.woocommerce .product_list_widget .product-title {
		color: <?php echo $content_title; ?>;
	}
	.widget.woocommerce .product_list_widget .product-title:hover {
		color:<?php echo $main_color; ?>;
	}
	.widget.woocommerce .product_list_widget del,
	.widget.woocommerce .product_list_widget ins {
		color: <?php echo $content_title; ?>;
	}
	.widget.woocommerce .product_list_widget .woocommerce-Price-amount.amount {
		color: <?php echo $content_title; ?>;	
	}
	.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content {
		background-color:<?php echo $main_color; ?>;
	}
	.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
	.woocommerce .widget_price_filter .ui-slider .ui-slider-range {
		background-color:<?php echo $secondary_color; ?>;
	}
	.woocommerce.widget_recent_reviews ul.product_list_widget li a {
		color: <?php echo $content_title; ?>;
	}
	.woocommerce div.product p.price,
	.woocommerce div.product .stock {
		color:<?php echo $content_title; ?>;
	}
	.woocommerce .single-product div.product .woocommerce-tabs ul.tabs {
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
		border-top: 1px solid <?php echo $content_navigation_background; ?>;
		border-right:1px solid <?php echo $main_color; ?>;
	}
	.woocommerce .single-product div.product .woocommerce-tabs ul.tabs li.active {
		background:<?php echo $secondary_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.footer-widget .widget.woocommerce .product_list_widget .product-title {
		color: <?php echo $content_post; ?>;
	}
	.footer-widget .widget.woocommerce .product_list_widget .woocommerce-Price-amount.amount {
		color: <?php echo $content_text; ?>;
	}
	.footer-widget .widget.woocommerce .product_list_widget .product-title:hover {
		color:<?php echo $main_color; ?>;
	}
	.footer-widget .widget.woocommerce.widget_product_tag_cloud .tagcloud a {
		color: <?php echo $content_text; ?>;
	}
	.footer-widget .widget.woocommerce.widget_product_tag_cloud .tagcloud a:hover {
		color:<?php echo $main_color; ?>;
	}
<?php } ?>	


<?php if ( class_exists( 'bbPress' ) ) { ?>
	/* BBPRESS */
	#bbpress-forums .button {
		background:<?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	#bbpress-forums #bbp-search-form #bbp_search {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	#bbpress-forums div.odd, #bbpress-forums ul.odd {
		background-color: <?php echo $content_post; ?>;
	}
	#bbpress-forums li.bbp-header {
		background: <?php echo $content_navigation_background; ?>;
		color: <?php echo $content_text; ?>;
	}
	#bbpress-forums .bbp-forum-title,
	#bbpress-forums .bbp-topic-permalink {
		color: <?php echo $content_title; ?>;
	}
	#bbpress-forums .bbp-forum-title:hover,
	#bbpress-forums .bbp-topic-permalink:hover {
		color: <?php echo $main_color; ?>;
	}
	#bbpress-forums .bbp-forum-info .bbp-forum-content,
	#bbpress-forums p.bbp-topic-meta,
	#bbpress-forums .bbp-topic-started-by {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#bbpress-forums .bbp-forums-list li a {
		color:<?php echo $content_title; ?>;
	}
	#bbpress-forums .bbp-forums-list li a:hover {
		color:<?php echo $main_color; ?>;
	}
	#bbpress-forums .bbp-forum-topic-count,
	#bbpress-forums .bbp-forum-reply-count,
	#bbpress-forums .bbp-forum-freshness,
	#bbpress-forums .bbp-forum-info,
	#bbpress-forums .bbp-topic-title,
	#bbpress-forums .bbp-topic-voice-count,
	#bbpress-forums .bbp-topic-reply-count {
		color: <?php echo $content_title; ?>;
	}
	#bbpress-forums .bbp-forum-freshness > a,
	#bbpress-forums .bbp-topic-freshness > a {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#bbpress-forums .bbp-author-name {
		color: <?php echo $content_title; ?>;
	}
	#bbpress-forums .bbp-author-name:hover {
		color: <?php echo $main_color; ?>;
	}
	#bbpress-forums #bbp-single-user-details #bbp-user-navigation li.current a,
	#bbpress-forums #bbp-single-user-details #bbp-user-navigation li a:hover {
		background:<?php echo $main_color; ?>;
		color:<?php echo $content_post; ?>;
	}
	#bbpress-forums #bbp-single-user-details #bbp-user-navigation li a {
		background:<?php echo $content_navigation_background; ?>;
		color:<?php echo $content_title; ?>;
	}
	#bbpress-forums .bbp-breadcrumb .bbp-breadcrumb-home,
	#bbpress-forums .bbp-breadcrumb-sep,
	#bbpress-forums .bbp-breadcrumb-current,
	#bbpress-forums .bbp-breadcrumb-root,
	#bbpress-forums .bbp-breadcrumb-forum,
	#bbpress-forums .bbp-topic-started-in > a {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#bbpress-forums .bbp-breadcrumb .bbp-breadcrumb-home:hover,
	#bbpress-forums .bbp-breadcrumb-sep:hover,
	#bbpress-forums .bbp-breadcrumb-root:hover,
	#bbpress-forums .bbp-breadcrumb-forum:hover,
	#bbpress-forums .bbp-topic-started-in > a:hover {
		color: <?php echo $main_color; ?>;
	}
	#bbpress-forums .bbp-topics-front ul.super-sticky,
	#bbpress-forums .bbp-topics ul.super-sticky,
	#bbpress-forums .bbp-topics ul.sticky,
	#bbpress-forums .bbp-forum-content ul.sticky {
		background-color:<?php echo $content_post; ?> !important;
	}
	#bbpress-forums .status-closed,
	#bbpress-forums .status-closed a {
		color: <?php echo $content_title; ?>;
	}
	#bbpress-forums fieldset.bbp-form legend {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	#bbpress-forums fieldset.bbp-form {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	#bbpress-forums div.bbp-the-content-wrapper textarea.bbp-the-content {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	/* WIDGET BBPRESS */
	.widget.widget_display_search #bbp-search-form input#bbp_search {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.widget_display_search #bbp-search-form input.button {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.widget.widget_display_forums li,
	.widget.widget_display_topics li,
	.widget.widget_display_views li,
	.widget.widget_display_replies li {
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.bbp_widget_login .bbp-logged-in a.button.logout-link {
		color:<?php echo $footer_bottom_text; ?>;
	}
	.widget.bbp_widget_login .bbp-logged-in a.button.logout-link:hover {
		color:<?php echo $main_color; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form label {
		color: <?php echo $content_title; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form input {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form .bbp-remember-me > label {
		color: <?php echo $footer_bottom_text; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form .bbp-submit-wrapper {
		background: <?php echo $main_color; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form .bbp-submit-wrapper:hover,
	.widget.bbp_widget_login .bbp-login-form .bbp-submit-wrapper:hover .button {
		background: <?php echo $secondary_color; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form .bbp-submit-wrapper .button {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.widget.widget_display_stats dl dt {
		color: <?php echo $content_title; ?>;
	}
	.widget.widget_display_stats dl dd {
		color: <?php echo $content_title; ?>;
		border-bottom:1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.bbp_widget_login .bbp-login-form .bbp-remember-me > label {
		color: <?php echo $footer_bottom_text; ?>;
	}
<?php } ?>


<?php if ( class_exists( 'BuddyPress' ) ) { ?>
	/* BUDDYPRESS */
	#buddypress div.item-list-tabs ul li.current a,
	#buddypress div.item-list-tabs ul li.selected a,
	#buddypress div.item-list-tabs ul li a,
	#buddypress div.item-list-tabs ul li span {
		background-color: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	#buddypress div.item-list-tabs ul li.current a:hover,
	#buddypress div.item-list-tabs ul li.selected a:hover,
	#buddypress div.item-list-tabs ul li a:hover,
	#buddypress div.item-list-tabs ul li span:hover {
		background:<?php echo $secondary_color; ?>;
	}
	#buddypress div.item-list-tabs ul li a span {
		color: <?php echo $content_title; ?>;
	}
	#buddypress .comment-reply-link,
	#buddypress .generic-button a,
	#buddypress .standard-form button,
	#buddypress a.button,
	#buddypress input[type="button"],
	#buddypress input[type="reset"],
	#buddypress input[type="submit"],
	#buddypress ul.button-nav li a,
	#buddypress a.bp-title-button {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	#buddypress .comment-reply-link:hover,
	#buddypress .standard-form button:hover,
	#buddypress a.button:focus,
	#buddypress a.button:hover,
	#buddypress div.generic-button a:hover,
	#buddypress input[type="button"]:hover,
	#buddypress input[type="reset"]:hover,
	#buddypress input[type="submit"]:hover,
	#buddypress ul.button-nav li a:hover,
	#buddypress ul.button-nav li.current a {
		background: <?php echo $secondary_color; ?>;
		color:<?php echo $content_post; ?>;
	}
	#buddypress div.dir-search input[type="text"],
	#buddypress li.groups-members-search input[type="text"] {
		border: 1px solid <?php echo $content_navigation_background; ?>;
		color: <?php echo $content_text; ?>;
	}
	#buddypress .item-list-tabs #groups-order-select label,
	#buddypress .item-list-tabs #members-order-select label,
	#buddypress .item-list-tabs #activity-filter-select label {
		color: <?php echo $content_title; ?>;
	}
	#buddypress div.item-list-tabs ul li.last select {
		color:<?php echo $content_title; ?>;
	}
	#buddypress div.item-list-tabs ul li.last select option {
		color: <?php echo $content_title; ?>;
	}
	#buddypress div.pagination .pag-count {
		color: <?php echo $content_title; ?>;
	}
	#buddypress div.pagination .pagination-links a,
	#buddypress div.pagination .pagination-links span {
		color: <?php echo $content_text; ?>;
	}
	#buddypress div.pagination .pagination-links .page-numbers.current {
		color: <?php echo $content_title; ?>;
	}
	#buddypress div.pagination .pagination-links .page-numbers.current:hover {
		color: <?php echo $content_title; ?>;
	}
	#buddypress div.pagination .pagination-links .page-numbers:hover {
		color:<?php echo $main_color; ?>;
	}
	#buddypress .groups.dir-list li .item .item-title a {
		color: <?php echo $content_title; ?>;
	}
	#buddypress .groups.dir-list li .item .item-meta .activity {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#buddypress ul.item-list li div.meta {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#buddypress .members.dir-list .item .item-title a {
		color: <?php echo $content_title; ?>;
	}
	#buddypress .members.dir-list .item .item-meta .activity {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#buddypress .acomment-meta a,
	#buddypress .activity-header a,
	#buddypress .comment-meta a {
		color: <?php echo $content_title; ?>;
	}
	#buddypress a.activity-time-since {
		color: <?php echo $footer_bottom_text; ?>;
	}
	#buddypress a.activity-time-since:hover {
		color: <?php echo $main_color; ?>;
	}
	#buddypress ul.item-list li {
		border-bottom:1px solid <?php echo $content_navigation_background; ?>;
	}
	#buddypress .activity-list li.load-more a,
	#buddypress .activity-list li.load-newest a {
		border: 1px solid <?php echo $content_navigation_background; ?>;
		color: <?php echo $content_title; ?>;
	}
	#buddypress .activity-list li.load-more a:hover,
	#buddypress .activity-list li.load-newest a:hover {
		border: 1px solid <?php echo $main_color; ?>;
		background:<?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.buddypress.logged-in #buddypress #groups-personal span,
	.buddypress.logged-in #buddypress #members-personal span,
	.buddypress.logged-in #buddypress #activity-friends span,
	.buddypress.logged-in #buddypress #activity-groups span,
	.buddypress.logged-in .bp_members #buddypress #item-nav #object-nav li a span {
		background: <?php echo $content_post; ?>;
	}
	.buddypress.logged-in #buddypress form#whats-new-form textarea {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .profile .profile-fields tr td.label {
		color:<?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .profile .profile-fields tr td.data p a {
		color:<?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .profile .profile-fields tr td.data p a:hover {
		color:<?php echo $main_color; ?>;
	}
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications tr th {
		color: <?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications .notification-description a,
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications .notification-actions .mark-read.primary {
		color:<?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications .notification-description a:hover,
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications .notification-actions .mark-read.primary:hover {
		color:<?php echo $main_color; ?>;
	}
	.buddypress.logged-in #buddypress #notifications-bulk-management .notifications-options-nav select {
		color: <?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body #subnav .message-search #search-message-form input#messages_search {
		border: 1px solid <?php echo $content_navigation_background; ?>;
		color: <?php echo $content_text; ?>;
	}
	.buddypress.logged-in #buddypress #messages-bulk-management #message-threads.messages-notices thead tr th {
		color: <?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress table#message-threads tr.unread td {
		background: <?php echo $content_post; ?>;
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .messages #messages-bulk-management #message-threads tbody a {
		color:<?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .messages #messages-bulk-management #message-threads tbody a.delete {
		color:<?php echo $secondary_color; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .messages #messages-bulk-management #message-threads tbody a:hover {
		color:<?php echo $main_color; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .messages #messages-bulk-management .messages-options-nav select {
		color: <?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .members.friends .item .item-title a {
		color:<?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body .members.friends .item .item-meta .activity {
		color:<?php echo $footer_bottom_text; ?>;
	}
	.buddypress.logged-in #buddypress #item-body #settings-form.standard-form label {
		color: <?php echo $content_title; ?>;
	}
	.buddypress.logged-in #buddypress #item-body #settings-form.standard-form .submit .auto {
		background:<?php echo $main_color; ?>;
	}
	.buddypress.logged-in #buddypress #item-body #settings-form.standard-form .submit .auto:hover {
		background:<?php echo $secondary_color; ?>;
	}
	/* BUDDYPRESS WIDGET*/
	.widget.widget_bp_groups_widget.buddypress.widget #groups-list li,
	.widget.widget_bp_core_friends_widget.buddypress.widget #friends-list .vcard,
	.widget.widget_bp_core_members_widget.buddypress.widget #members-list .vcard {
		border-bottom: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.widget_bp_groups_widget.buddypress.widget #groups-list-options a,
	.widget.widget_bp_core_friends_widget.buddypress.widget #friends-list-options a,
	.widget.widget_bp_core_members_widget.buddypress.widget #members-list-options a {
		color: <?php echo $content_title; ?>;
	}
	.widget.widget_bp_groups_widget.buddypress.widget #groups-list-options a:hover,
	.widget.widget_bp_core_friends_widget.buddypress.widget #friends-list-options a:hover,
	.widget.widget_bp_core_members_widget.buddypress.widget #members-list-options a:hover,
	.widget.widget_bp_groups_widget.buddypress.widget #groups-list-options a.selected,
	.widget.widget_bp_core_members_widget.buddypress.widget #members-list-options a.selected {
		color: <?php echo $main_color; ?>;
	}
	.widget.widget_bp_groups_widget.buddypress.widget .public.group-has-avatar .item .item-meta .activity,
	.widget.widget_bp_core_friends_widget.buddypress.widget #friends-list .vcard .item .item-meta .activity,
	.widget.widget_bp_core_members_widget.buddypress.widget #members-list .vcard .item .item-meta .activity {
		color: <?php echo $footer_bottom_text; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget .bp-login-widget-user-link a {
		color: <?php echo $content_title; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget .bp-login-widget-user-links .bp-login-widget-user-logout .logout {
		color: <?php echo $footer_bottom_text; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget .bp-login-widget-user-links .bp-login-widget-user-logout .logout:hover {
		color: <?php echo $main_color; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget #bp-login-widget-form#bp-login-widget-form label {
		color: <?php echo $content_title; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget #bp-login-widget-form#bp-login-widget-form input {
		border: 1px solid <?php echo $content_navigation_background; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget #bp-login-widget-form.standard-form#bp-login-widget-form .forgetmenot label {
		color: <?php echo $footer_bottom_text; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget #bp-login-widget-form.standard-form#bp-login-widget-form input#bp-login-widget-submit {
		background: <?php echo $main_color; ?>;
		color: <?php echo $content_post; ?>;
	}
	.widget.widget_bp_core_login_widget.buddypress.widget #bp-login-widget-form.standard-form#bp-login-widget-form input#bp-login-widget-submit:hover {
		background: <?php echo $secondary_color; ?>;
	}
<?php } ?>	

.flownews-menu-style2 nav ul li a:hover {
	color: <?php echo $header_bottom_main_text_menu;?>;
}
.flownews-menu-style2 nav ul.submenu li a:hover,
.flownews-menu-style2 nav li ul.submenu li.current-menu-item > a:hover, 
.flownews-menu-style2 nav li ul.submenu li.current-menu-ancestor > a:hover {
	color: <?php echo $main_color;?>;
}
.flownews-menu-style2 nav li ul.submenu li.current-menu-item > a,  
.flownews-menu-style2 nav li ul.submenu li.current-menu-ancestor > a {
	color: <?php echo $header_bottom_text_submenu;?>;
	background: <?php echo $header_bottom_background_submenu;?>;
}
.flownews-menu-style2 nav > ul > li:hover:before,
.flownews-menu-style2 nav > ul > li.current_page_item:before, 
.flownews-menu-style2 nav > ul > li.current-menu-item:before,  
.flownews-menu-style2 nav > ul > li.current-menu-ancestor:before {
	background-color: <?php echo $main_color;?>;
}
.flownews-menu-style3 nav ul li a:hover {
	color: <?php echo $main_color;?>;
}
.flownews-menu-style3 .flownews-element-posts .article-category a:hover {
	color: <?php echo $header_bottom_main_text_menu;?>;
}
.flownews-menu-style3 nav li.current-menu-ancestor > a,
.flownews-menu-style3 nav ul.submenu li a:hover,
.flownews-menu-style3 nav li ul.submenu li.current-menu-item > a:hover, 
.flownews-menu-style3 nav li ul.submenu li.current-menu-ancestor > a:hover, 
.flownews-menu-style3 nav li ul.submenu li.current-menu-item > a,  
.flownews-menu-style3 nav li ul.submenu li.current-menu-ancestor > a {
	color: <?php echo $main_color;?>;
}
.flownews-menu-style3 nav li ul.submenu li.current-menu-item > a,  
.flownews-menu-style3 nav li ul.submenu li.current-menu-ancestor > a {
	background: <?php echo $header_bottom_background_submenu;?>;
}


/* Ticker Style 2 */
.flownews-top-news-ticker .ticker {
	background-color: <?php echo $header_top_background;?>;
}
.flownews-top-news-ticker .ticker-title {
	background-color: <?php echo $header_top_background;?>;
}
.flownews-top-news-ticker .ticker-content a {
	color: <?php echo $header_top_text;?>;
}
.flownews-top-news-ticker .ticker-content a:hover {	
	color: <?php echo $main_color;?>;
}
.flownews-top-news-ticker .ticker-swipe {
	background-color: <?php echo $header_top_background;?>;
}
.flownews-top-news-ticker .ticker-swipe span {
	background-color: <?php echo $header_top_text;?>;
}
.flownews-top-news-ticker .ticker-controls li.jnt-prev:before {
	color: <?php echo $header_top_text;?>;
}
.flownews-top-news-ticker .ticker-controls li.jnt-prev.over:before {
	color: <?php echo $main_color;?>;
}
.flownews-top-news-ticker .ticker-controls li.jnt-next:before {
	color: <?php echo $header_top_text;?>;
}
.flownews-top-news-ticker .ticker-controls li.jnt-next.over:before {
	color: <?php echo $main_color;?>;
}
.flownews-top-news-ticker .no-js-news { 
	color: <?php echo $header_top_text;?>;
}
.flownews-top-news-ticker .ticker-title span {
	background: <?php echo $main_color;?>;
    color: <?php echo $header_top_text;?>;
}

.flownews-top-news-ticker-addon .news-ticker-item .news-ticker-item-category a {
	background: <?php echo $main_color; ?>;
	color: <?php echo $content_post; ?>;
}
.flownews-top-news-ticker-addon .news-ticker-item .news-ticker-item-title a {
	color: <?php echo $content_title; ?>;
}
.flownews-top-news-ticker-addon .news-ticker-item .news-ticker-item-title a:hover {
	color: <?php echo $main_color; ?>;
}
.flownews-top-news-ticker-addon .news-ticker-item .news-ticker-item-date {
	color: <?php echo $content_title; ?>;
}
.flownews-top-news-ticker-addon.owl-theme .owl-controls .owl-nav [class*="owl-"] {
    color: <?php echo $content_title; ?>;
}

/*style 2*/

.flownews-top-news-ticker-addon.flownews-newsticker-type2.owl-theme .owl-controls .owl-nav [class*="owl-"] {
    border: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-top-news-ticker-addon.flownews-newsticker-type2.owl-theme .owl-controls .owl-nav .owl-prev:hover,
.flownews-top-news-ticker-addon.flownews-newsticker-type2.owl-theme .owl-controls .owl-nav .owl-next:hover {
    background: <?php echo $main_color; ?>;
	color:<?php echo $content_post; ?>;
	border:1px solid <?php echo $main_color; ?>;
}
.flownews-top-news-ticker-addon.flownews-newsticker-type2.owl-carousel.owl-theme.owl-loaded {
    border: 1px solid <?php echo $content_navigation_background; ?>;
}



/*style 3*/

.flownews-top-news-ticker.flownews-newsticker-type3 {
	border: 1px solid <?php echo $content_navigation_background; ?>;
}

.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-content a {
	color: <?php echo $content_title; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-swipe {
	background:<?php echo $content_background ?>;
	border-left:1px solid <?php echo $content_title; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li {
    border: 1px solid <?php echo $content_navigation_background; ?>;
    color: <?php echo $content_title; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li:hover {
    background: <?php echo $main_color; ?>;
    border: 1px solid <?php echo $main_color; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li a:hover {
	color:<?php echo $content_post; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li.jnt-prev::before,
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li.jnt-next::before {
	color:<?php echo $content_title; ?>;
}
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li.jnt-prev:hover::before,
.flownews-top-news-ticker.flownews-newsticker-type3 .ticker-controls li.jnt-next:hover::before {
	color:<?php echo $content_post; ?>;
}