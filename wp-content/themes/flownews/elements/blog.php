<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 global $post;
 global $flownews_theme;
 $title_page = get_post_meta( get_the_id(), 'flownews-title-page', true );
 if($title_page == '') : $title_page = 'no'; endif;
 
 $blog_type 	= get_post_meta($post->ID, "flownews-blog-posts-type", true);
 $blog_layout	= get_post_meta($post->ID, "flownews-blog-posts-layout", true);
 
 $columns 		= get_post_meta($post->ID, "flownews-blog-columns", true);  
 if($columns == '1') : $columns_class = 'col-xs-12'; $container_class = 'flownews-blog-1-col'; endif;
 if($columns == '2') : $columns_class = 'col-xs-6'; $container_class = 'flownews-blog-2-col'; endif;
 if($columns == '3') : $columns_class = 'col-xs-4'; $container_class = 'flownews-blog-3-col'; endif;
 if($columns == '4') : $columns_class = 'col-xs-3'; $container_class = 'flownews-blog-4-col'; endif;
 
 $category 		= get_post_meta($post->ID, "flownews-category", true);
 $orderby 		= get_post_meta($post->ID, "flownews-orderby", true);
 $orderdir 		= get_post_meta($post->ID, "flownews-orderdir", true);
 $num_posts		= get_post_meta($post->ID, "flownews-num-posts", true);
 
 $pagination 	= get_post_meta($post->ID, "flownews-pagination", true);
 
 if($category 	== '') 		: $category 	= ''; 		endif;
 if($orderby 	== 'none') 	: $orderby 		= 'none'; 	endif;
 if($orderdir 	== 'DESC') 	: $orderdir 	= 'DESC'; 	endif;
 if($pagination == '') 		: $pagination 	= 'yes'; 	endif;

 # Pagination value
 if ( get_query_var('paged') ) {	
		$paged = get_query_var('paged');	
 } elseif ( get_query_var('page') ) {	
		$paged = get_query_var('page');	
 } else {	
		$paged = 1;	
 }

 # WP Query
 if($orderby == 'meta_value_num') :

	 $query = array(
					'post_type' 		=> 'post', 
					'cat' 				=> $category,
					'orderby'			=> $orderby,
					'order'				=> $orderdir,
					'meta_key' 			=> 'wpb_post_flownews_views_count', 
					'paged' 			=> $paged,
					'posts_per_page' 	=> $num_posts					
				); 
 
 else :

	 $query = array(
					'post_type' 		=> 'post', 
					'cat' 				=> $category,
					'orderby'			=> $orderby,
					'order'				=> $orderdir,				 
					'paged' 			=> $paged,
					'posts_per_page' 	=> $num_posts					
				); 
 
 endif;
 
 $class_load_more = '';
 $class_item_load_more = '';
 if($pagination == 'load-more') :
	$class_load_more = 'flownews-load-more';
	$class_item_load_more = 'flownews-item-load-more';	
 endif;
 
 $class_item_masonry = '';
 if($blog_type == 'masonry') :
	$class_item_masonry = 'flownews-element-posts-masonry';
	wp_enqueue_script( 'masonry' );
 endif; ?>

<!-- Page Title -->
<?php if($title_page == 'yes') : ?>
	<h2 class="flownews-title-page-container">
		<span class="flownews-title-page"><?php the_title(); ?></span>
	</h2>
<?php endif; ?>
		
<div class="flownews-element-posts <?php echo $class_item_masonry; ?> <?php echo $class_load_more; ?> <?php echo $blog_layout; ?> <?php echo $container_class; ?> element-no-padding">

<?php 
 $readtext 		= esc_html__('Read More','flownews');
 $loading 		= esc_html__('Loading posts...','flownews');
 $nomoreposts 	= esc_html__('No more posts to load.','flownews');
 
 $loop = new WP_Query($query);

 // Lazy LOAD
 if($pagination == 'load-more') : 
			wp_enqueue_script(
				'flownews-load-posts',
				FLOWNEWS_JS_URL . 'load-posts.js',
				array('jquery'),
				'1.0',
				true
			);		
					
			$max = $loop->max_num_pages;
			$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
			
			// Add some parameters for the JS.
			wp_localize_script(
				'flownews-load-posts',
				'fnwp_',
				array(
					'startPage' => $paged,
					'maxPages' => $max,
					'nextLink' => next_posts($max, false),
					'readtext'		=> $readtext,
					'loading'		=> $loading,
					'nomoreposts'	=> $nomoreposts
				)
			);
 endif; 
 
 $count = 1; 
 if ( $loop ) :
 while ( $loop->have_posts() ) : $loop->the_post(); 
 $link = get_permalink(); 
 ?>
 
 <?php if($blog_layout == 'flownews-posts-layout1') :?>
 
	 <article class="item-posts first-element-posts <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
		<div class="article-image">
				<?php echo flownews_thumbs('flownews-preview-post'); ?>
				<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
						<a href="<?php echo $link; ?>" class="article-icon-link"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
				<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	 </article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout2') : ?>
 
	<article class="item-posts first-element-posts <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
		<div class="article-image">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
					<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
				<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">	
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	</article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout3') : ?>

	<article class="item-posts first-element-posts <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
		<div class="article-image">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
				<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
					<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">		
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<p class="article-excerpt"><?php echo fnwp_excerpt(150); ?></p>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	</article>	 
 
 <?php elseif($blog_layout == 'flownews-posts-layout4') : ?>
 
 	<article class="item-posts first-element-posts <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
		<div class="article-image col-xs-4">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
		</div>
		<div class="article-info col-xs-8">
			<div class="article-info-top">
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
					<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">	
				<div class="article-category"><?php echo flownews_category(2); ?>
					<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
				</div>					
				<div class="flownews-clear"></div>
			</div>
		</div>
	</article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout5') : ?>

		<?php if($count <= $columns) : ?>
				
			<article class="item-posts first-element-posts first-row <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
				<div class="article-image">
					<?php echo flownews_thumbs('flownews-preview-post'); ?>
					<?php echo flownews_check_format(); ?>
						<div class="article-category"><?php echo flownews_category(2); ?>
								<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
						</div>
				</div>
				<div class="article-info">
					<div class="article-info-top">
						<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
								<div class="article-separator">|</div>
								<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
								<div class="flownews-clear"></div>
					</div>
					<div class="article-info-bottom">	
						<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
						<p class="article-excerpt"><?php echo fnwp_excerpt(150); ?></p>
						<div class="flownews-clear"></div>
					</div>
				</div>
			</article>

		<?php else : ?>
			
			<?php $class_other_rows = ''; if($count >= $columns) : $class_other_rows = 'other-rows'; endif; ?>
			
			<article class="item-posts first-element-posts <?php echo $class_other_rows; ?> <?php echo $columns_class; ?> <?php echo $class_item_load_more; ?>">
				<div class="article-image col-xs-4">
					<?php echo flownews_thumbs('flownews-preview-post'); ?>
					<?php echo flownews_check_format(); ?>
				</div>
				<div class="article-info col-xs-8">
					<div class="article-info-top">
						<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
						<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
						<div class="flownews-clear"></div>
					</div>
					<div class="article-info-bottom">		
							<div class="article-category"><?php echo flownews_category(2); ?>
								<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
							</div>					
							<div class="flownews-clear"></div>	
					</div>
				</div>
			</article>				
										
		<?php endif; ?>				 
 
 <?php endif; ?>
 
 <?php if(($count % $columns) == 0 && $blog_type != 'masonry') : ?> <div class="flownews-clear"></div> <?php endif; ?>
 
<?php 
 $count++;
 endwhile; 
 ?>  
 
 <div class="flownews-clear"></div>
 <?php if($blog_type == 'masonry') : ?> </div> <?php endif; ?>
 <?php
 # start:pagination active
 if($pagination == 'yes') :
	 echo '<div class="flownews-clear"></div><div class="flownews-pagination">';
	 # start:pagination
	 if($flownews_theme['pagination'] == 'standard') :
		echo '<div class="flownews-pagination-normal">';
						echo get_next_posts_link( 'Older posts', $loop->max_num_pages );
						echo get_previous_posts_link( 'Newer posts' );
		echo '</div>';
	 else :
		echo flownews_posts_numeric_pagination($pages = '', $range = 2,$loop,$paged);
	 endif;
	 # end:pagination
	 echo '<div class="flownews-clear"></div></div>';	 
 endif; 
 # end:pagination active
 
 
 endif; 
 
 wp_reset_query();
?>
<?php if($blog_type != 'masonry') : ?> </div> <?php endif; ?>