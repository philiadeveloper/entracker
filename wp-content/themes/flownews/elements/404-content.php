<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 ?>
 
 <!-- start:loop 404 page -->			
 	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <!-- Page Title -->
		<h2 class="flownews-title-page-container">
			<span class="flownews-title-page"><?php printf( esc_html__( '404 - Page Not found', 'flownews' ), single_tag_title( '', false ) ); ?></span>
        </h2>	
        
        <!-- Page Content -->                
        <div class="post-text text-content">                  
            <div class="text">
                <div class="not-found">                    
					<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'flownews' ); ?></p>
    
                    <p><?php esc_html_e('You can go to the','flownews'); ?> <a href="<?php echo esc_url(get_home_url()); ?>"><?php esc_html_e('Home Page', 'flownews'); ?></a></p> 
                </div>
				<?php
				
					$query = array(
							'post_type' 		=> 'post',
							'orderby'			=> 'date',
							'order'				=> 'DESC',				 
							'paged' 			=> $paged,
							'posts_per_page' 	=> 9					
					); 
				?>
				<!-- Page Title -->
				<h2 class="flownews-title-page-container">
						<span class="flownews-title-page"><?php esc_html_e('Latest Posts','flownews'); ?></span>
				</h2>				
				
				<div class="flownews-element-posts flownews-posts-layout1 flownews-blog-3-col element-no-padding">
				<?php 
					$loop = new WP_Query($query);
					$count = 1; 
					if ( $loop ) :
						while ( $loop->have_posts() ) : $loop->the_post(); 
							$link = get_permalink(); 				
				?>
					 <article class="item-posts first-element-posts col-xs-4">
						<div class="article-image">
								<?php echo flownews_thumbs('flownews-preview-post'); ?>
								<?php echo flownews_check_format(); ?>
							<div class="article-category"><?php echo flownews_category(2); ?>
										<a href="<?php echo $link; ?>" class="article-icon-link"><i class="flownewsicon fa-mail-forward"></i></a>
							</div>
						</div>
						<div class="article-info">
							<div class="article-info-top">
								<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
								<div class="article-separator">|</div>
								<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
								<div class="flownews-clear"></div>
							</div>
							<div class="article-info-bottom">
								<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
								<div class="flownews-clear"></div>	
							</div>
						</div>
					 </article>
				<?php if(($count % 3) == 0) : ?> <div class="flownews-clear"></div> 
				<?php endif; 
					$count++;
					endwhile; 
				endif;	
				?>  
				
				</div>
 			</div>
        </div>        
 
 	</article>
 <!-- end:loop 404 page -->