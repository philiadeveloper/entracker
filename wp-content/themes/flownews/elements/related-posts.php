<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 global $flownews_theme;
 
 # Load Metabox Value
 $sidebar = get_post_meta( get_the_id(), 'flownews-sidebar', true );
 if(!isset($sidebar) || $sidebar == '') : $sidebar = 'sidebar-none'; endif; 
 
 wp_enqueue_style('flownews-carousel');
 wp_enqueue_script('flownews-carousel-js'); 
 wp_enqueue_style('flownews-vc-element'); 
 
 /* RTL */	
 if ($flownews_theme['rtl']) :  $rtl = 'rtl:true,'; else : $rtl = ''; endif;  
 /* #RTL */ 

 $orig_post = $post;
 global $post;
 $tags = wp_get_post_tags($post->ID);
   
 if ($tags) :
  
 ?>
  
 <h2 class="flownews-title-page-container">
  	<span class="flownews-title-page"><?php esc_html_e('Related Article','flownews'); ?></span>
 </h2>
	
 <?php 
 $tag_ids = array();
 foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	
 $args=array(
		  'tag__in' => $tag_ids,
		  'post__not_in' => array($post->ID),
 );
   
 $related_query = new wp_query( $args ); ?>
	
	<?php if($related_query->have_posts()) : ?>
	
		<div class="related-item-container flownews-vc-element-posts-carousel flownews-element-posts flownews-posts-layout2 element-no-padding">
		
		<?php 
		while( $related_query->have_posts() ) :
		$related_query->the_post();		
		$link = get_permalink();
		?>
	  
		<article class="item-posts first-element-posts">
			<div class="article-image">
				<?php echo flownews_related_thumbs('flownews-preview-post'); ?>
				<?php echo flownews_check_format(); ?>
				<div class="article-category"><?php echo flownews_category(1); ?>
					<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
				</div>
			</div>
			<div class="article-info">
				<div class="article-info-top">
					<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
					<div class="article-separator">|</div>
					<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
					<div class="flownews-clear"></div>
				</div>
				<div class="article-info-bottom">	
					<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
					<div class="flownews-clear"></div>	
				</div>
			</div>
		</article>
			   	   
		<?php endwhile; ?>	
	
		</div>
		
	<?php else : ?>
	
		<p><?php esc_html_e('No Related Article','flownews'); ?></p>
	
	<?php endif; ?>		
		
		
 <?php
 endif;
 $post = $orig_post;
 wp_reset_postdata();
 ?>
 
 <div class="clearfix"></div>