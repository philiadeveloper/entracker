<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 global $post;
 global $flownews_theme;
 
 $category = get_the_category(); 
 $category_id = $category[0]->cat_ID;
 $cat_data = get_option("category_$category_id"); 
 
 /* Layout */ 
 $blog_layout = $cat_data['flownews_category_layout'];
 
 if($blog_layout == '' || $blog_layout == 'default') : 
	$blog_layout = $flownews_theme['flownews_panel_category_layout'];
	if(!isset($blog_layout) || $blog_layout == '') : $blog_layout = 'flownews-posts-layout1'; endif; 
 endif;

 /* Columns */
 $columns = $cat_data['flownews_category_columns'];
 if($columns == '' || $columns == 'default') : 
	$columns = $flownews_theme['flownews_panel_category_columns'];
	if(!isset($columns) || $columns == '') : $columns = '2'; endif; 
 endif; 

 if($columns == '1') : $columns_class = 'col-xs-12'; $container_class = 'flownews-blog-1-col'; endif;
 if($columns == '2') : $columns_class = 'col-xs-6'; $container_class = 'flownews-blog-2-col'; endif;
 if($columns == '3') : $columns_class = 'col-xs-4'; $container_class = 'flownews-blog-3-col'; endif;
 if($columns == '4') : $columns_class = 'col-xs-3'; $container_class = 'flownews-blog-4-col'; endif; 
 
 /* Layout Type */
 $blog_type = $cat_data['flownews_category_layout_type'];
 
 if($blog_type == '' || $blog_type == 'default') : 
	$blog_type = $flownews_theme['flownews_panel_category_layout_type'];
	if(!isset($blog_type) || $blog_type == '') : $blog_type = 'grid'; endif; 
 endif; 
 
 # Pagination value
 if ( get_query_var('paged') ) {	
		$paged = get_query_var('paged');	
 } elseif ( get_query_var('page') ) {	
		$paged = get_query_var('page');	
 } else {	
		$paged = 1;	
 }
 
 $class_load_more = '';
 
 $class_item_masonry = '';
 if($blog_type == 'masonry') :
	$class_item_masonry = 'flownews-element-posts-masonry';
	wp_enqueue_script( 'masonry' );
 endif; ?>
 
<div class="flownews-element-posts <?php echo $class_item_masonry; ?> <?php echo $class_load_more; ?> <?php echo $blog_layout; ?> <?php echo $container_class; ?> element-no-padding">
 <?php 
 $count = 1; 
 if ( have_posts() ) :
 while ( have_posts() ) : the_post(); 
 $link = get_permalink();
 ?> 
 
  <?php if($blog_layout == 'flownews-posts-layout1') :?>
 
	 <article class="item-posts first-element-posts <?php echo $columns_class; ?>">
		<div class="article-image">
				<?php echo flownews_thumbs('flownews-preview-post'); ?>
				<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
						<a href="<?php echo $link; ?>" class="article-icon-link"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
				<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	 </article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout2') : ?>
 
	<article class="item-posts first-element-posts <?php echo $columns_class; ?>">
		<div class="article-image">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
					<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
				<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">	
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	</article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout3') : ?>

	<article class="item-posts first-element-posts <?php echo $columns_class; ?>">
		<div class="article-image">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
			<div class="article-category"><?php echo flownews_category(2); ?>
				<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
			</div>
		</div>
		<div class="article-info">
			<div class="article-info-top">
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
				<div class="article-separator">|</div>
				<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
					<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">		
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<p class="article-excerpt"><?php echo fnwp_excerpt(150); ?></p>
				<div class="flownews-clear"></div>	
			</div>
		</div>
	</article>	 
 
 <?php elseif($blog_layout == 'flownews-posts-layout4') : ?>
 
 	<article class="item-posts first-element-posts <?php echo $columns_class; ?>">
		<div class="article-image col-xs-4">
			<?php echo flownews_thumbs('flownews-preview-post'); ?>
			<?php echo flownews_check_format(); ?>
		</div>
		<div class="article-info col-xs-8">
			<div class="article-info-top">
				<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
				<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
					<div class="flownews-clear"></div>
			</div>
			<div class="article-info-bottom">	
				<div class="article-category"><?php echo flownews_category(2); ?>
					<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
				</div>					
				<div class="flownews-clear"></div>
			</div>
		</div>
	</article>
 
 <?php elseif($blog_layout == 'flownews-posts-layout5') : ?>

		<?php if($count <= $columns) : ?>
				
			<article class="item-posts first-element-posts first-row <?php echo $columns_class; ?>">
				<div class="article-image">
					<?php echo flownews_thumbs('flownews-preview-post'); ?>
					<?php echo flownews_check_format(); ?>
						<div class="article-category"><?php echo flownews_category(2); ?>
								<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
						</div>
				</div>
				<div class="article-info">
					<div class="article-info-top">
						<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
								<div class="article-separator">|</div>
								<div class="article-comments"><i class="flownewsicon fa-comment-o"></i><?php echo flownews_get_num_comments(); ?></div>
								<div class="flownews-clear"></div>
					</div>
					<div class="article-info-bottom">	
						<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
						<p class="article-excerpt"><?php echo fnwp_excerpt(150); ?></p>
						<div class="flownews-clear"></div>
					</div>
				</div>
			</article>

		<?php else : ?>
			
			<?php $class_other_rows = ''; if($count <= ($columns * 2)) : $class_other_rows = 'other-rows'; endif; ?>
			
			<article class="item-posts first-element-posts <?php echo $class_other_rows; ?> <?php echo $columns_class; ?>">
				<div class="article-image col-xs-4">
					<?php echo flownews_thumbs('flownews-preview-post'); ?>
					<?php echo flownews_check_format(); ?>
				</div>
				<div class="article-info col-xs-8">
					<div class="article-info-top">
						<h3 class="article-title"><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></h3>
						<div class="article-data"><i class="flownewsicon fa-calendar-o"></i><?php echo get_the_date(); ?></div>
						<div class="flownews-clear"></div>
					</div>
					<div class="article-info-bottom">		
							<div class="article-category"><?php echo flownews_category(2); ?>
								<a href="<?php echo $link; ?>"><i class="flownewsicon fa-mail-forward"></i></a>
							</div>					
							<div class="flownews-clear"></div>	
					</div>
				</div>
			</article>				
										
		<?php endif; ?>				 
 
 <?php endif; ?>
 
 <?php if(($count % $columns) == 0 && $blog_type != 'masonry') : ?> <div class="flownews-clear"></div> <?php endif; ?>
 
<?php 
 $count++;
 endwhile; 
 ?>  
 
 <div class="flownews-clear"></div>
 <?php if($blog_type == 'masonry') : ?> </div> <?php endif; ?>
 <?php
 $pagination = 'yes';
 # start:pagination active
 if($pagination == 'yes') :
	 echo '<div class="flownews-clear"></div><div class="flownews-pagination">';
	 # start:pagination
	 if($flownews_theme['pagination'] == 'standard') :
		echo '<div class="flownews-pagination-normal">';
						echo get_next_posts_link( 'Older posts', $wp_query->max_num_pages );
						echo get_previous_posts_link( 'Newer posts' );
		echo '</div>';
	 else :
		echo flownews_posts_numeric_pagination($pages = '', $range = 2,$wp_query,$paged);
	 endif;
	 # end:pagination
	 echo '<div class="flownews-clear"></div></div>';	 
 endif; 
 # end:pagination active
 
 
 endif; 
 
 wp_reset_query();
?>
<?php if($blog_type != 'masonry') : ?> </div> <?php endif; ?>