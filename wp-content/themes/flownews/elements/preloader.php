<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */
 global $flownews_theme;
 ?>
 
 
 <?php if($flownews_theme['preloader'] == '1') : ?>
 
         <div id="preloader-container">
                <div id="preloader-wrap">
					<div class="cssload-thecube">
						<div class="cssload-cube cssload-c1"></div>
						<div class="cssload-cube cssload-c2"></div>
						<div class="cssload-cube cssload-c4"></div>
						<div class="cssload-cube cssload-c3"></div>
					</div>
                </div>
         </div>   

 <?php endif; ?>
                            
                            
                            