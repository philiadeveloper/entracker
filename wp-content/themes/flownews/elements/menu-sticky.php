<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */
 global $flownews_theme;
 if(!isset($flownews_theme['menu-sticky'])) : $flownews_theme['menu-sticky'] = 'menu-sticky'; endif; 
 ?>
 
 <!-- start:menu desktop -->
 <nav class="menu-desktop <?php echo $flownews_theme['menu-sticky']; ?>">
     <?php
        // Top Navigation
        $defaults = array(
            'theme_location'  => 'main-menu',
            'container'       => 'ul',
            'container_class' => 'flownews-menu',
            'container_id'    => '',
            'fallback_cb'     => 'nav_fallback',
            'menu_class'      => 'flownews-menu',
            'menu_id'         => '',
            'echo'            => true,
            'depth'           => 0,
			'walker' 		  => new My_Walker_Nav_Menu_Sticky()
        );
		if ( function_exists( 'wp_nav_menu' ) && has_nav_menu('main-menu') )  {
        	wp_nav_menu( $defaults );
		}
      ?>
 </nav>	
 <!-- end:menu desktop -->