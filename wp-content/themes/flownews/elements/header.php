<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */
 global $flownews_theme;
 global $current_user;
 if(!isset($flownews_theme['logo']['url'])) : $flownews_theme['logo']['url'] = ''; endif;
 
 ?>
  
     <header class="flownews-header-wrap <?php echo esc_html__($flownews_theme['header-menu-align']); ?> <?php echo esc_html__($flownews_theme['header-menu-style']); ?>">
     
     	<div class="flownews-header-wrap-container header-desktop">

		<?php $header_elements_order = $flownews_theme['header-order']['enabled']; 
		
			if ($header_elements_order): 
			
				foreach ($header_elements_order as $key=>$value) {
				switch($key) {
				
				# CASE Header Top							
				case 'header-top': ?>
			
					<?php if(esc_html($flownews_theme['header-top-active']) == true) : ?>
				
						<?php echo flownews_header_top(); ?>
					
					<?php endif; ?> 
				
				<?php
				break;			
				# CASE Header Middle
				case 'header-middle':
				
				?>
				
					<div class="flownews-header-middle element-no-padding"> 

						<?php if(esc_html($flownews_theme['header-middle-logo-potision']) == 'left') : ?>
					
							<div class="flownews-wrap-container">
							
								<div class="flownews-logo flownews-logo-left col-sm-3">
									<?php echo flownews_logo(); ?>
								</div>
								
								<?php if(esc_html($flownews_theme['advertisement-top']) == true) : ?>
									<div class="flownews-banner-top flownews-banner-top-right col-sm-9">
										<?php echo flownews_banner_top(); ?>
									</div>
								<?php endif; ?>
								
								<div class="flownews-clear"></div>
							</div>
							
						<?php elseif(esc_html($flownews_theme['header-middle-logo-potision']) == 'right') :?>

							<div class="flownews-wrap-container">

								<?php if(esc_html($flownews_theme['advertisement-top']) == true) : ?>							
									<div class="flownews-banner-top flownews-banner-top-left col-sm-9">
										<?php echo flownews_banner_top(); ?>
									</div>								
								<?php endif; ?>		
								
								<div class="flownews-logo flownews-logo-right col-sm-3">
										<?php echo flownews_logo(); ?>
								</div>
								
								<div class="flownews-clear"></div>							
							</div>

						<?php else : ?>
						
							<div class="flownews-wrap-container">
								<div class="flownews-logo flownews-logo-center col-sm-12">
									<?php echo flownews_logo(); ?>
								</div>						
								<div class="flownews-clear"></div>						
							</div>
							
						<?php endif; ?>
							
					</div> 

				<?php
				
				break;
				
				# CASE Header Bottom
				case 'header-bottom':
				
				?>
				
					<div class="flownews-header-bottom">   
						<div class="flownews-header-bottom flownews-wrap-container">
							
							<?php if(esc_html($flownews_theme['search'] == true)) : ?>
								
								<?php if($flownews_theme['rtl']) : 
									if ( class_exists( 'woocommerce' ) && $flownews_theme['flownews_woocommerce_add_to_cart'] == true) : ?>	
										<div class="flownews-woocommerce-add-to-cart-container col-sm-1">						
											<?php echo flownews_add_to_cart(); ?>						
										</div>										
									<?php else : ?>
										<div class="flownews-search-container col-sm-1">						
											<?php echo flownews_search(); ?>						
										</div>
									<?php endif; ?>
									<div class="flownews-menu col-sm-11">
										<?php get_template_part('elements/menu'); ?>
									</div>								
								
								<?php else : ?>
								
									<div class="flownews-menu col-sm-11">
										<?php get_template_part('elements/menu'); ?>
									</div>						
									<?php if ( class_exists( 'woocommerce' ) && $flownews_theme['flownews_woocommerce_add_to_cart'] == true) : ?>	
										<div class="flownews-woocommerce-add-to-cart-container col-sm-1">						
											<?php echo flownews_add_to_cart(); ?>						
										</div>										
									<?php else : ?>
										<div class="flownews-search-container col-sm-1">						
											<?php echo flownews_search(); ?>						
										</div>
									<?php endif; ?>								
																						
								<?php endif; ?>
								
							<?php else : ?> 
							
								<div class="flownews-menu col-sm-12">
									<?php get_template_part('elements/menu'); ?>
								</div>						
							
							<?php endif; ?>

							<div class="flownews-clear"></div>
						</div>
					 </div> 
				 
		<?php
				break;
				}
			}
						 
			endif; # End Foreach Header Order
					
		?>	
				 
		</div>
			
		<?php if(esc_html($flownews_theme['header-sticky'])) : ?>
			
			<div class="flownews-header-sticky">
				<div class="flownews-header-bottom flownews-wrap-container">
					<?php if(esc_html($flownews_theme['header-sticky-logo-position'] == 'left')) : ?>
					
									<div class="flownews-logo flownews-logo-right col-sm-2">
											<?php echo flownews_logo_sticky(); ?>
									</div>			
									<div class="flownews-menu col-sm-10">
										<?php get_template_part('elements/menu-sticky'); ?>
									</div>						
		
					<?php else : ?>
					
									<div class="flownews-menu col-sm-10">
										<?php get_template_part('elements/menu-sticky'); ?>
									</div>						
									<div class="flownews-logo flownews-logo-right col-sm-2">
											<?php echo flownews_logo_sticky(); ?>
									</div>			
					
					<?php endif; ?>
					<div class="flownews-clear"></div>
				</div>				
			</div>
		
		<?php endif; ?>
		
			<div class="flownews-header-wrap-container header-mobile">
				<div class="flownews-logo col-sm-10">
					<?php echo flownews_logo_mobile(); ?>
				</div>			
				<div class="flonews-menu-mobile col-sm-2">
					<?php get_template_part('elements/menu-mobile'); ?>
				</div>
				<div class="flownews-clear"></div>
				<div class="flownews-ticker">
                    <?php echo flownews_ticker(); ?>
                </div>				
			</div>
     </header>
	 
	 
	 <?php 
	 # Slider Content
	 if(esc_html($flownews_theme['header-slider'])) : 
		# If Only Homepage
		if(esc_html($flownews_theme['header-slider-position']) == 'homepage') :
			if(is_home() || is_front_page()) :
	 ?>
				<div class="flownews-slider-container <?php echo esc_html($flownews_theme['header-slider-container']); ?>">
					<?php echo do_shortcode(esc_html($flownews_theme['header-slider-shortcode'])); ?>
				</div>
			<?php endif; ?>	
	 <?php 
		else : ?>	
			<div class="flownews-slider-container <?php echo esc_html($flownews_theme['header-slider-container']); ?>">
				<?php echo do_shortcode(esc_html($flownews_theme['header-slider-shortcode'])); ?>
			</div>	 	
	 <?php 
	    endif;
	 endif; ?>