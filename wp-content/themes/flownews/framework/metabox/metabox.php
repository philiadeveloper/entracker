<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 add_action( 'add_meta_boxes', 'flownews_add_meta_boxes' );
 function flownews_add_meta_boxes() {
	global $post;
    add_meta_box( 
        'flownews_layout',
        esc_html__( 'FlowNews Layout', 'flownews' ),
        'flownews_layout_function',
        'post' 
    );
    if(!empty($post)) {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);
        if($pageTemplate != 'page-fullwidth.php' )
        {	
			add_meta_box( 
				'flownews_layout',
				esc_html__( 'FlowNews Layout', 'flownews' ),
				'flownews_layout_function',
				'page' 
			);	
		}
	}			
 }
 
function flownews_layout_function($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>

	<!-- start:metabox container -->
    <div id="flownews-metabox">
        
        <!-- Layout Type -->
        <?php 
		global $current_screen;
		if($current_screen ->id !== "post") : ?>
        <div class="flownews-metabox-item">
            <?php $title_page_value = get_post_meta($object->ID, "flownews-title-page", true); ?>
            <label for="flownews-title-page"><?php esc_html_e('Title Page/Blog','flownews'); ?></label>
            <select name="flownews-title-page">   
				<option value="no" <?php if($title_page_value == 'no') : echo 'selected'; endif; ?>>No</option>          
            	<option value="yes" <?php if($title_page_value == 'yes') : echo 'selected'; endif; ?>>Yes</option>
            </select>
        </div>  		
		<div class="flownews-metabox-item">
            <label for="flownews-layout-type"><?php esc_html_e('Layout Type','flownews'); ?></label>
            <select id="flownews-layout-type" name="flownews-layout-type">
            <?php 
                    $blog_option_values = array(
						 		'flownews-page'	 			=> 'Page',
								'flownews-blog' 			=> 'Blog',
					);

                    foreach($blog_option_values as $key => $value) 
                    {
                        if($key == get_post_meta($object->ID, "flownews-layout-type", true))
                        {
                            ?>
                                <option selected value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
        </div>
        
        
        <!-- start:blog query -->    
		<div id="blog-query" 
			<?php if(get_post_meta($object->ID, "flownews-layout-type", true) == 'flownews-page' || 
			get_post_meta($object->ID, "flownews-layout-type", true) == '' ) : ?> 
            	style="display:none" 
			<?php endif; ?>
		>
        	<h2><?php esc_html_e('Query Blog','flownews'); ?></h2>
			
			<div class="flownews-metabox-item">
				<label for="flownews-num-posts"><?php esc_html_e('Numbers posts to load / Number posts per page','flownews'); ?></label>
				<input name="flownews-num-posts" type="text" value='<?php echo get_post_meta($object->ID, "flownews-num-posts", true); ?>'><br>
				<span class="flownews-description"><?php esc_html_e('Add int number value. Examples: 8. Leave empty for get default posts setted in general wordpress setting','flownews'); ?></span>
			</div>	   
				
            <!-- Category -->
            <div class="flownews-metabox-item">
			
					
				
                <?php     
                $categories = get_categories(array('orderby' => 'name','order' => 'ASC'));?>
                <?php 
                $categories_selected = (get_post_meta($object->ID, "flownews-category", true));
                ?>
                <label for="flownews-category"><?php esc_html_e('Category','flownews'); ?></label>
                <select multiple="multiple" name="flownews-category[]">
                <?php foreach ($categories as $category) {  ?>
                    <option  value="<?php echo $category->term_id; ?>"              
                    <?php
                    if($categories_selected != '') : 
                        foreach($categories_selected as $category_selected) : 
                            if($category_selected == $category->term_id) :
                                echo 'selected'; 
                            endif;
                        endforeach;
                    endif;
                    ?>            
                    >		
                <?php echo $category->name; ?></option>
                <?php } ?>
                </select>
            </div>    

          	<div class="flownews-metabox-item">
            	<?php $orderby_value = get_post_meta($object->ID, "flownews-orderby", true); ?>
            	<label for="flownews-orderby"><?php esc_html_e('Order By','flownews'); ?></label>
            	<select name="flownews-orderby">            
            		<option value="none" <?php if($orderby_value == 'none') : echo 'selected'; endif; ?>><?php esc_html_e('none','flownews'); ?></option>
                    <option value="comment_count" <?php if($orderby_value == 'comment_count') : echo 'selected'; endif; ?>><?php esc_html_e('Comment Count','flownews'); ?></option>
            		<option value="meta_value_num" <?php if($orderby_value == 'meta_value_num') : echo 'selected'; endif; ?>><?php esc_html_e('Views','flownews'); ?></option>
                    <option value="date" <?php if($orderby_value == 'date') : echo 'selected'; endif; ?>><?php esc_html_e('date','flownews'); ?></option>
            		<option value="modified" <?php if($orderby_value == 'modified') : echo 'selected'; endif; ?>><?php esc_html_e('modified','flownews'); ?></option>
                    <option value="title" <?php if($orderby_value == 'title') : echo 'selected'; endif; ?>><?php esc_html_e('title','flownews'); ?></option>
                    <option value="rand" <?php if($orderby_value == 'rand') : echo 'selected'; endif; ?>><?php esc_html_e('rand','flownews'); ?></option>
            	</select>
            </div> 

          	<div class="flownews-metabox-item">
            	<?php $orderdir_value = get_post_meta($object->ID, "flownews-orderdir", true); ?>
            	<label for="flownews-orderdir"><?php esc_html_e('Order dir','flownews'); ?></label>
            	<select name="flownews-orderdir">                     
                	<option value="DESC" <?php if($orderdir_value == 'DESC') : echo 'selected'; endif; ?>>DESC</option>           
            		<option value="ASC" <?php if($orderdir_value == 'ASC') : echo 'selected'; endif; ?>>ASC</option>
            	</select>
            </div> 

			<h2><?php esc_html_e('Blog Style','flownews'); ?></h2>
			
          	<div class="flownews-metabox-item">
            	<?php $blog_posts_type_value = get_post_meta($object->ID, "flownews-blog-posts-type", true); ?>
            	<label for="flownews-blog-posts-type"><?php esc_html_e('Blog Layout Type','flownews'); ?></label>
            	<select id="flownews-blog-posts-type" name="flownews-blog-posts-type">            
            		<option value="grid" <?php if($blog_posts_type_value == 'grid') : echo 'selected'; endif; ?>><?php esc_html_e('Grid','flownews'); ?></option>
                    <option value="masonry" <?php if($blog_posts_type_value == 'masonry') : echo 'selected'; endif; ?>><?php esc_html_e('Masonry','flownews'); ?></option>
             	</select>
            </div> 
          	
			<div class="flownews-metabox-item">
            	<?php $blog_posts_layout_value = get_post_meta($object->ID, "flownews-blog-posts-layout", true); ?>
            	<label for="flownews-blog-posts-layout"><?php esc_html_e('Blog Layout Style','flownews'); ?></label>
            	<select name="flownews-blog-posts-layout">            
            		<option value="flownews-posts-layout1" <?php if($blog_posts_layout_value == 'flownews-posts-layout1') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 1','flownews'); ?></option>
                    <option value="flownews-posts-layout2" <?php if($blog_posts_layout_value == 'flownews-posts-layout2') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 2','flownews'); ?></option>
            		<option value="flownews-posts-layout3" <?php if($blog_posts_layout_value == 'flownews-posts-layout3') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 3','flownews'); ?></option>
                    <option value="flownews-posts-layout4" <?php if($blog_posts_layout_value == 'flownews-posts-layout4') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 4','flownews'); ?></option>
            		<option value="flownews-posts-layout5" <?php if($blog_posts_layout_value == 'flownews-posts-layout5') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 5','flownews'); ?></option>
            	</select>
            </div> 

          	<div class="flownews-metabox-item">
            	<?php $blog_columns_value = get_post_meta($object->ID, "flownews-blog-columns", true); ?>
            	<label for="flownews-blog-columns"><?php esc_html_e('Blog Columns','flownews'); ?></label>
            	<select id="flownews-blog-columns" name="flownews-blog-columns">            
            		<option value="1" <?php if($blog_columns_value == '1') : echo 'selected'; endif; ?>><?php esc_html_e('1','flownews'); ?></option>
                    <option value="2" <?php if($blog_columns_value == '2') : echo 'selected'; endif; ?>><?php esc_html_e('2','flownews'); ?></option>
            		<option value="3" <?php if($blog_columns_value == '3') : echo 'selected'; endif; ?>><?php esc_html_e('3','flownews'); ?></option>
                    <option value="4" <?php if($blog_columns_value == '4') : echo 'selected'; endif; ?>><?php esc_html_e('4','flownews'); ?></option>
            	</select>
            </div>
			
          	<div class="flownews-metabox-item">
            	<?php $pagination_value = get_post_meta($object->ID, "flownews-pagination", true); ?>
            	<label for="flownews-pagination"><?php esc_html_e('Pagination','flownews'); ?></label>
            	<select name="flownews-pagination">            
            		<option value="yes" <?php if($pagination_value == 'yes') : echo 'selected'; endif; ?>>Yes</option>
					<option class="flownews-load-more" value="load-more" <?php if($pagination_value == 'load-more') : echo 'selected'; endif; ?> <?php if($blog_posts_type_value == 'masonry') : echo 'style="display:none"'; endif; ?>>Yes with Load More</option>
                    <option value="no" <?php if($pagination_value == 'no') : echo 'selected'; endif; ?>>No</option>
            	</select>
            </div>  
            
            
            
		</div>
        <!-- end:blog query -->
        <?php endif; ?>
        
        <!-- sidebar -->
		<div id="flownews-sidebar" class="flownews-metabox-item"
			<?php if(get_post_meta($object->ID, "flownews-blog-columns", true) == '3' || get_post_meta($object->ID, "flownews-blog-columns", true) == '4') : ?> 
            	style="display:none" 
			<?php endif; ?>        
        >
            <label for="flownews-sidebar"><?php esc_html_e('Sidebar','flownews'); ?></label>
            <select name="flownews-sidebar">
                <?php 
					if($current_screen ->id == "post") :		
						$option_values = array(
										'sidebar-panel'	=> 'Default Value',
										'sidebar-none'  => 'None',
										'sidebar-left'  => 'Left',
										'sidebar-right' => 'Right'
						);
					else :
						$option_values = array(
										'sidebar-none'  => 'None',
										'sidebar-left'  => 'Left',
										'sidebar-right' => 'Right'
						);					
					endif;
                    foreach($option_values as $key => $value) 
                    {
                        if($key == get_post_meta($object->ID, "flownews-sidebar", true))
                        {
                            ?>
                                <option selected value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
			
            <label for="flownews-sidebar-name"><?php esc_html_e('Sidebar Name','flownews'); ?></label>
            <select name="flownews-sidebar-name">
                <?php 
					global $wp_registered_sidebars;		
					if($current_screen ->id == "post") :
						$sidebar_options['sidebar-default-panel-name'] = 'Default Value';
					endif;
					foreach ($wp_registered_sidebars as $sidebar) {
						$sidebar_options[$sidebar['id']] = $sidebar['name'];
					}

                    foreach($sidebar_options as $key => $value) 
                    {
                        if($key == get_post_meta($object->ID, "flownews-sidebar-name", true))
                        {
                            ?>
                                <option selected value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>			
			
			
			
		</div>
		
        <?php 
		if($current_screen ->id == "post") : ?>  
			
			<div class="flownews-metabox-item" id="flownews_container_post_layout">
            	<?php $posts_layout_value = get_post_meta($object->ID, "flownews_post_layout", true); ?>
            	<label for="flownews_post_layout"><?php esc_html_e('Post Layout Style','flownews'); ?></label>
            	<select name="flownews_post_layout">    
            		<option value="default" <?php if($posts_layout_value == 'default') : echo 'selected'; endif; ?>><?php esc_html_e('Default (get option setted on panel)','flownews'); ?></option>					
            		<option value="flownews-post-layout1" <?php if($posts_layout_value == 'flownews-post-layout1') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 1','flownews'); ?></option>
                    <option value="flownews-post-layout2" <?php if($posts_layout_value == 'flownews-post-layout2') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 2','flownews'); ?></option>
            		<option value="flownews-post-layout3" <?php if($posts_layout_value == 'flownews-post-layout3') : echo 'selected'; endif; ?>><?php esc_html_e('Posts Layout 3','flownews'); ?></option>
            	</select>
            </div> 

		<?php endif; ?>
    </div>
    <!-- end:metabox container -->
    <?php  
}

function save_flownews_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
	
	$current_screen = get_current_screen();
	
    $flownews_sidebar_value = "";
	$flownews_sidebar_name_value = "";
	$flownews_layout_type = ""; 
	$flownews_num_posts = "";
	$flownews_category = "";
	$flownews_pagination = "";
	$flownews_title_page = "";
	$flownews_blog_posts_type = "";
	$flownews_blog_posts_layout = "";
	$flownews_blog_columns = "";
	$flownews_orderby = "";
	$flownews_orderdir = "";
	$flownews_posts_layout = "";
		
    if(isset($_POST["flownews-sidebar"]))
    {
        $flownews_sidebar_value = $_POST["flownews-sidebar"];
    }   
    update_post_meta($post_id, "flownews-sidebar", $flownews_sidebar_value);

    if(isset($_POST["flownews-sidebar-name"]))
    {
        $flownews_sidebar_name_value = $_POST["flownews-sidebar-name"];
    }   
    update_post_meta($post_id, "flownews-sidebar-name", $flownews_sidebar_name_value);
	
    if(isset($_POST["flownews-layout-type"]))
    {
        $flownews_layout_type = $_POST["flownews-layout-type"];
    }   
    update_post_meta($post_id, "flownews-layout-type", $flownews_layout_type);

    if(isset($_POST["flownews-num-posts"]))
    {
    	$flownews_num_posts = $_POST["flownews-num-posts"]; 
	} 
    update_post_meta($post_id, "flownews-num-posts", $flownews_num_posts);
	
    if(isset($_POST["flownews-category"]))
    {
    	$flownews_category = $_POST["flownews-category"]; 
	} 
    update_post_meta($post_id, "flownews-category", $flownews_category);

	
    if(isset($_POST["flownews-pagination"]))
    {
    	$flownews_pagination = $_POST["flownews-pagination"]; 
	} 
    update_post_meta($post_id, "flownews-pagination", $flownews_pagination);

	if(isset($_POST["flownews-title-page"]))
    {
    	$flownews_title_page = $_POST["flownews-title-page"]; 
	} 
    update_post_meta($post_id, "flownews-title-page", $flownews_title_page);
	
    if(isset($_POST["flownews-blog-posts-type"]))
    {
    	$flownews_blog_posts_type = $_POST["flownews-blog-posts-type"]; 
	} 
    update_post_meta($post_id, "flownews-blog-posts-type", $flownews_blog_posts_type);	
	
	
    if(isset($_POST["flownews-blog-posts-layout"]))
    {
    	$flownews_blog_posts_layout = $_POST["flownews-blog-posts-layout"]; 
	} 
    update_post_meta($post_id, "flownews-blog-posts-layout", $flownews_blog_posts_layout);	
	
	
    if(isset($_POST["flownews-blog-columns"]))
    {
    	$flownews_blog_columns = $_POST["flownews-blog-columns"]; 
	} 
    update_post_meta($post_id, "flownews-blog-columns", $flownews_blog_columns);
	
	
    if(isset($_POST["flownews-orderby"]))
    {
    	$flownews_orderby = $_POST["flownews-orderby"]; 
	} 
    update_post_meta($post_id, "flownews-orderby", $flownews_orderby);


    if(isset($_POST["flownews-orderdir"]))
    {
    	$flownews_orderdir = $_POST["flownews-orderdir"]; 
	} 
    update_post_meta($post_id, "flownews-orderdir", $flownews_orderdir);

	if($current_screen ->id == "post") :
		if(isset($_POST["flownews_post_layout"]))
		{
			$flownews_post_layout = $_POST["flownews_post_layout"];
		}
		update_post_meta($post_id, "flownews_post_layout", $flownews_post_layout);
	endif;
}

add_action("save_post", "save_flownews_meta_box", 10, 3);

/* METABOX TOP CONTENT SECTION */
 add_action( 'add_meta_boxes', 'flownews_add_meta_boxes_top_content' );
 function flownews_add_meta_boxes_top_content() {
	global $post;
    add_meta_box( 
        'flownews_top_content',
        esc_html__( 'FlowNews Top Content', 'flownews' ),
        'flownews_top_content_function',
        'post' 
    );
    if(!empty($post)) {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);
        if($pageTemplate != 'page-fullwidth.php' )
        {		
			add_meta_box( 
				'flownews_top_content',
				esc_html__( 'FlowNews Top Content', 'flownews' ),
				'flownews_top_content_function',
				'page' 
			);
		}
	}		
 }

function flownews_top_content_function($object) {
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
	<!-- start:metabox container -->
    <div id="flownews-metabox">
		<div class="flownews-metabox-item">
            <label for="flownews-top-content-active"><?php esc_html_e('Top Content Section','flownews'); ?></label>
            <select id="flownews-top-content-active" name="flownews-top-content-active">
            <?php 
                    $top_content_values = array(
						 		'off'	 			=> 'Off',
								'on' 				=> 'on'
					);

                    foreach($top_content_values as $key => $value) 
                    {
                        if($key == get_post_meta($object->ID, "flownews-top-content-active", true))
                        {
                            ?>
                                <option selected value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
        </div>		
	</div>

	<div id="flownews-top-content" 
			<?php if(get_post_meta($object->ID, "flownews-top-content-active", true) == 'off' || get_post_meta($object->ID, "flownews-top-content-active", true) == '') : ?> 
            	style="display:none" 
			<?php endif; ?>	
	
	>
	
		<!-- Layout Style -->
		<div class="flownews-metabox-item">
            <?php $top_content_layout_value = get_post_meta($object->ID, "flownews-top-content-layout-style", true); ?>
            <label for="flownews-top-content-layout-style"><?php esc_html_e('Top Content Layout Style','flownews'); ?></label>
            <select name="flownews-top-content-layout-style">            
            	<option value="flownews-top-content-layout1" <?php if($top_content_layout_value == 'flownews-top-content-layout1') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 1','flownews'); ?></option>
                <option value="flownews-top-content-layout2" <?php if($top_content_layout_value == 'flownews-top-content-layout2') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 2','flownews'); ?></option>
				<option value="flownews-top-content-layout3" <?php if($top_content_layout_value == 'flownews-top-content-layout3') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 3','flownews'); ?></option>
                <option value="flownews-top-content-layout4" <?php if($top_content_layout_value == 'flownews-top-content-layout4') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 4 (Carousel)','flownews'); ?></option>
				<option value="flownews-top-content-layout5" <?php if($top_content_layout_value == 'flownews-top-content-layout5') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 5','flownews'); ?></option>
				<option value="flownews-top-content-layout6" <?php if($top_content_layout_value == 'flownews-top-content-layout6') : echo 'selected'; endif; ?>><?php esc_html_e('Layout 6 (Single Post Featured)','flownews'); ?></option>
			</select>
        </div> 	
	
        <!-- start:blog query -->    
		<div id="top-content-query">
        	<h2><?php esc_html_e('Query Top Content','flownews'); ?></h2> 
				
            <!-- Category -->
            <div class="flownews-metabox-item">			
                <?php     
                $top_content_categories = get_categories(array('orderby' => 'name','order' => 'ASC'));?>
                <?php 
                $top_content_categories_selected = (get_post_meta($object->ID, "flownews-top-content-category", true));
                ?>
                <label for="flownews-top-content-category"><?php esc_html_e('Category','flownews'); ?></label>
                <select multiple="multiple" name="flownews-top-content-category[]">
                <?php foreach ($top_content_categories as $top_content_category) {  ?>
                    <option  value="<?php echo $top_content_category->term_id; ?>"              
                    <?php
                    if($top_content_categories_selected != '') : 
                        foreach($top_content_categories_selected as $top_content_category_selected) : 
                            if($top_content_category_selected == $top_content_category->term_id) :
                                echo 'selected'; 
                            endif;
                        endforeach;
                    endif;
                    ?>            
                    >		
                <?php echo $top_content_category->name; ?></option>
                <?php } ?>
                </select>
            </div>    

          	<div class="flownews-metabox-item">
            	<?php $top_content_orderby_value = get_post_meta($object->ID, "flownews-top-content-orderby", true); ?>
            	<label for="flownews-top-content-orderby"><?php esc_html_e('Order By','flownews'); ?></label>
            	<select name="flownews-top-content-orderby">            
            		<option value="none" <?php if($top_content_orderby_value == 'none') : echo 'selected'; endif; ?>><?php esc_html_e('none','flownews'); ?></option>
                    <option value="comment_count" <?php if($top_content_orderby_value == 'comment_count') : echo 'selected'; endif; ?>><?php esc_html_e('Comment Count','flownews'); ?></option>
            		<option value="meta_value_num" <?php if($top_content_orderby_value == 'meta_value_num') : echo 'selected'; endif; ?>><?php esc_html_e('Views','flownews'); ?></option>
                    <option value="date" <?php if($top_content_orderby_value == 'date') : echo 'selected'; endif; ?>><?php esc_html_e('date','flownews'); ?></option>
            		<option value="modified" <?php if($top_content_orderby_value == 'modified') : echo 'selected'; endif; ?>><?php esc_html_e('modified','flownews'); ?></option>
                    <option value="title" <?php if($top_content_orderby_value == 'title') : echo 'selected'; endif; ?>><?php esc_html_e('title','flownews'); ?></option>
                    <option value="rand" <?php if($top_content_orderby_value == 'rand') : echo 'selected'; endif; ?>><?php esc_html_e('rand','flownews'); ?></option>
            	</select>
            </div> 

          	<div class="flownews-metabox-item">
            	<?php $top_content_orderdir_value = get_post_meta($object->ID, "flownews-top-content-orderdir", true); ?>
            	<label for="flownews-top-content-orderdir"><?php esc_html_e('Order dir','flownews'); ?></label>
            	<select name="flownews-top-content-orderdir">                     
                	<option value="DESC" <?php if($top_content_orderdir_value == 'DESC') : echo 'selected'; endif; ?>>DESC</option>           
            		<option value="ASC" <?php if($top_content_orderdir_value == 'ASC') : echo 'selected'; endif; ?>>ASC</option>
            	</select>
            </div>            
            
		</div>
        <!-- end:blog query -->
	
	</div>
	
	
	
<?php }

function save_flownews_top_content_meta_box($post_id, $post, $update) {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

	# Top Content Active
	$flownews_top_content_value = "";
    if(isset($_POST["flownews-top-content-active"])) {
        $flownews_top_content_value = $_POST["flownews-top-content-active"];
    }   
    update_post_meta($post_id, "flownews-top-content-active", $flownews_top_content_value);	

	# Top Content Style
	$flownews_top_content_layout_style = "";
    if(isset($_POST["flownews-top-content-layout-style"])) {
        $flownews_top_content_layout_style = $_POST["flownews-top-content-layout-style"];
    }   
    update_post_meta($post_id, "flownews-top-content-layout-style", $flownews_top_content_layout_style);	

	# Top Content Query Category
	$flownews_top_content_category = "";
    if(isset($_POST["flownews-top-content-category"])) {
        $flownews_top_content_category = $_POST["flownews-top-content-category"];
    }   
    update_post_meta($post_id, "flownews-top-content-category", $flownews_top_content_category);

	# Top Content Query Orderby
	$flownews_top_content_orderby = "";
    if(isset($_POST["flownews-top-content-orderby"])) {
        $flownews_top_content_orderby = $_POST["flownews-top-content-orderby"];
    }   
    update_post_meta($post_id, "flownews-top-content-orderby", $flownews_top_content_orderby);

	# Top Content Query Orderdir
	$flownews_top_content_orderdir = "";
    if(isset($_POST["flownews-top-content-orderdir"])) {
        $flownews_top_content_orderdir = $_POST["flownews-top-content-orderdir"];
    }   
    update_post_meta($post_id, "flownews-top-content-orderdir", $flownews_top_content_orderdir);	
}	
add_action("save_post", "save_flownews_top_content_meta_box", 10, 3);

/* Format Video */
 add_action( 'add_meta_boxes', 'flownews_add_meta_boxes_video_format' );
 function flownews_add_meta_boxes_video_format() {
	global $post;
    add_meta_box( 
        'flownews_video_format',
        esc_html__( 'FlowNews Featured Video', 'flownews' ),
        'flownews_video_format_function',
        'post',
		'side'
    );	
 }

function flownews_video_format_function($object) {
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>

	<div class="flownews-metabox-item">
		<label for="flownews-url-video-embed"><?php esc_html_e('Url video','flownews'); ?></label>
		<input name="flownews-url-video-embed" type="text" value='<?php echo get_post_meta($object->ID, "flownews-url-video-embed", true); ?>'>
	</div>
	
<?php }

function save_flownews_video_format_meta_box($post_id, $post, $update) {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
	
	# Url Video Embed
	$flownews_url_video_embed = "";
    if(isset($_POST["flownews-url-video-embed"])) {
        $flownews_url_video_embed = $_POST["flownews-url-video-embed"];
    }   
    update_post_meta($post_id, "flownews-url-video-embed", $flownews_url_video_embed);	
}	
add_action("save_post", "save_flownews_video_format_meta_box", 10, 3);	

/* Format Audio */
 add_action( 'add_meta_boxes', 'flownews_add_meta_boxes_audio_format' );
 function flownews_add_meta_boxes_audio_format() {
	global $post;
    add_meta_box( 
        'flownews_audio_format',
        esc_html__( 'FlowNews Featured Audio', 'flownews' ),
        'flownews_audio_format_function',
        'post',
		'side'
    );	
 }

function flownews_audio_format_function($object) {
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>

	<div class="flownews-metabox-item">
		<label for="flownews-url-audio-embed"><?php esc_html_e('Url audio','flownews'); ?></label>
		<input name="flownews-url-audio-embed" type="text" value='<?php echo get_post_meta($object->ID, "flownews-url-audio-embed", true); ?>'>
	</div>
	
<?php }

function save_flownews_audio_format_meta_box($post_id, $post, $update) {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
	
	# Url Audio Embed
	$flownews_url_audio_embed = "";
    if(isset($_POST["flownews-url-audio-embed"])) {
        $flownews_url_audio_embed = $_POST["flownews-url-audio-embed"];
    }   
    update_post_meta($post_id, "flownews-url-audio-embed", $flownews_url_audio_embed);	
}	
add_action("save_post", "save_flownews_audio_format_meta_box", 10, 3);	