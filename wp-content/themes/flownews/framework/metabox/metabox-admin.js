/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 jQuery(document).ready(function($){
		"use strict";	
 		
		$('#flownews-layout-type').on("click", function(){
 			var layout_type = $( "#flownews-layout-type" ).val();
			if(layout_type != 'flownews-page') {
				$('#blog-query').css('display','block');	
			} else {
				$('#blog-query').css('display','none');						
			}		
		});
		
		$('#flownews-blog-columns').on("click", function(){
			var blog_columns = $( "#flownews-blog-columns" ).val();
			if(blog_columns == '3' || blog_columns == '4') {
				$('#flownews-sidebar').css('display','none');
			} else {
				$('#flownews-sidebar').css('display','block');
			}
		});	
		
		$('#flownews-blog-posts-type').on("click", function(){
			var blog_type = $( "#flownews-blog-posts-type" ).val();
			if(blog_type == 'masonry') {
				$('.flownews-load-more').css('display','none');
			} else {
				$('.flownews-load-more').css('display','block');
			}
		});			
		
		$('#flownews-top-content-active').on("click", function(){
			var flownews_top_content = $( "#flownews-top-content-active" ).val();
			if(flownews_top_content == 'on') {
				$('#flownews-top-content').css('display','block');
			} else {
				$('#flownews-top-content').css('display','none');
			}		
		});

		$('#page_template').on('change', function() {
		   var template_value = $('#page_template').val(); 
		   if(template_value == 'page-fullwidth.php') {
			   $('#flownews_layout').css('display','none');
			   $('#flownews_top_content').css('display','none');
		   } else {
			   $('#flownews_layout').css('display','block');
			   $('#flownews_top_content').css('display','block');
		   }
		});
		
 });		