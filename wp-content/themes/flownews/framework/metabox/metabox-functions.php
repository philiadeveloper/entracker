<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */
 
 # Function Metabox Enqueue
 if ( ! function_exists( 'flownews_metabox_enqueue' ) ) {
	 function flownews_metabox_enqueue() {
		global $typenow;
		if( $typenow == 'post' || $typenow == 'page') {		
			wp_enqueue_script( 'flownews-metabox-js', FLOWNEWS_URL . '/framework/metabox/metabox-admin.js' );
			wp_enqueue_style( 'flownews-metabox-css',  FLOWNEWS_URL . '/framework/metabox/metabox-admin.css' );		
		}
	}
	add_action( 'admin_enqueue_scripts', 'flownews_metabox_enqueue' );
 }