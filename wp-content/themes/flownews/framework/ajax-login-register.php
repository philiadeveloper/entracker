<?php
function flownews_login_register_modal() {

		// only show the registration/login form to non-logged-in members
	if( ! is_user_logged_in() ){ 
?>
		<div class="modal fade flownews-user-modal" id="flownews-user-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" data-active-tab="">
				<div class="modal-content">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<?php

							if( get_option('users_can_register') ){ ?>

								<!-- Register form -->
								<div class="flownews-register">
							 
									<h3><?php printf( __('Join %s', 'flownews'), get_bloginfo('name') ); ?></h3>
									<hr>

									<form id="flownews_registration_form" action="<?php echo esc_url(home_url( '/' )); ?>" method="POST">

										<div class="form-field">
											<label><?php _e('Username', 'flownews'); ?></label>
											<input class="form-control input-lg required" name="flownews_user_login" type="text"/>
										</div>
										<div class="form-field">
											<label for="flownews_user_email"><?php _e('Email', 'flownews'); ?></label>
											<input class="form-control input-lg required" name="flownews_user_email" id="flownews_user_email" type="email"/>
										</div>

										<div class="form-field">
											<input type="hidden" name="action" value="flownews_register_member"/>
											<button class="btn btn-theme btn-lg" data-loading-text="<?php _e('Loading...', 'flownews') ?>" type="submit"><?php _e('Sign up', 'flownews'); ?></button>
										</div>
										<?php wp_nonce_field( 'ajax-login-nonce', 'register-security' ); ?>
									</form>
									<div class="flownews-errors"></div>
								</div>

								<!-- Login form -->
								<div class="flownews-login">
							 
									<h3><?php printf( __('Login to %s', 'flownews'), get_bloginfo('name') ); ?></h3>
									<hr>
							 
									<form id="flownews_login_form" action="<?php echo esc_url(home_url( '/' )); ?>" method="post">

										<div class="form-field">
											<label><?php _e('Username', 'flownews') ?></label>
											<input class="form-control input-lg required" name="flownews_user_login" type="text"/>
										</div>
										<div class="form-field">
											<label for="flownews_user_pass"><?php _e('Password', 'flownews')?></label>
											<input class="form-control input-lg required" name="flownews_user_pass" id="flownews_user_pass" type="password"/>
										</div>
										<div class="form-field">
											<input type="hidden" name="action" value="flownews_login_member"/>
											<button class="btn btn-theme btn-lg" data-loading-text="<?php _e('Loading...', 'flownews') ?>" type="submit"><?php _e('Login', 'flownews'); ?></button> <a class="alignright" href="#flownews-reset-password"><?php _e('Lost Password?', 'flownews') ?></a>
										</div>
										<?php wp_nonce_field( 'ajax-login-nonce', 'login-security' ); ?>
									</form>
									<div class="flownews-errors"></div>
								</div>

								<!-- Lost Password form -->
								<div class="flownews-reset-password">
							 
									<h3><?php _e('Reset Password', 'flownews'); ?></h3>
                                    <p>Enter the username or e-mail you used in your profile. A password reset link will be sent to you by email.</p>
									<hr>
							 
									<form id="flownews_reset_password_form" action="<?php echo esc_url(home_url( '/' )); ?>" method="post">
										<div class="form-field">
											<label for="flownews_user_or_email"><?php _e('Username or E-mail', 'flownews') ?></label>
											<input class="form-control input-lg required" name="flownews_user_or_email" id="flownews_user_or_email" type="text"/>
										</div>
										<div class="form-field">
											<input type="hidden" name="action" value="flownews_reset_password"/>
											<button class="btn btn-theme btn-lg" data-loading-text="<?php _e('Loading...', 'flownews') ?>" type="submit"><?php _e('Get new password', 'flownews'); ?></button>
										</div>
										<?php wp_nonce_field( 'ajax-login-nonce', 'password-security' ); ?>
									</form>
									<div class="flownews-errors"></div>
								</div>

								<div class="flownews-loading">
									<p><i class="fa fa-refresh fa-spin"></i><br><?php _e('Loading...', 'flownews') ?></p>
								</div><?php

							} else {
								echo '<h3>'.__('Login access is disabled', 'flownews').'</h3>';
							} ?>
					</div>
					<div class="modal-footer">
							<span class="flownews-register-footer"><?php _e('Don\'t have an account?', 'flownews'); ?> <a href="#flownews-register"><?php _e('Sign Up', 'flownews'); ?></a></span>
							<span class="flownews-login-footer"><?php _e('Already have an account?', 'flownews'); ?> <a href="#flownews-login"><?php _e('Login', 'flownews'); ?></a></span>
					</div>				
				</div>
			</div>
		</div>
<?php
	}
}
add_action('wp_footer', 'flownews_login_register_modal');




# 	
# 	AJAX FUNCTION
# 	========================================================================================
#   These function handle the submitted data from the login/register modal forms
# 	========================================================================================
# 		

// LOGIN
function flownews_login_member(){

  		// Get variables
		$user_login		= $_POST['flownews_user_login'];	
		$user_pass		= $_POST['flownews_user_pass'];


		// Check CSRF token
		if( !check_ajax_referer( 'ajax-login-nonce', 'login-security', false) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Session token has expired, please reload the page and try again', 'flownews').'</div>'));
		}
	 	
	 	// Check if input variables are empty
	 	elseif( empty($user_login) || empty($user_pass) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Please fill all form fields', 'flownews').'</div>'));
	 	} else { // Now we can insert this account

	 		$user = wp_signon( array('user_login' => $user_login, 'user_password' => $user_pass), false );

		    if( is_wp_error($user) ){
				echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.$user->get_error_message().'</div>'));
			} else{
				echo json_encode(array('error' => false, 'message'=> '<div class="alert alert-success">'.__('Login successful, reloading page...', 'flownews').'</div>'));
			}
	 	}

	 	wp_die();
}
add_action('wp_ajax_nopriv_flownews_login_member', 'flownews_login_member');



// REGISTER
function flownews_register_member(){

  		// Get variables
		$user_login	= $_POST['flownews_user_login'];	
		$user_email	= $_POST['flownews_user_email'];
		
		// Check CSRF token
		if( !check_ajax_referer( 'ajax-login-nonce', 'register-security', false) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Session token has expired, please reload the page and try again', 'flownews').'</div>'));
			die();
		}
	 	
	 	// Check if input variables are empty
	 	elseif( empty($user_login) || empty($user_email) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Please fill all form fields', 'flownews').'</div>'));
			die();
	 	}
		
		$errors = register_new_user($user_login, $user_email);	
		
		if( is_wp_error($errors) ){

			$registration_error_messages = $errors->errors;

			$display_errors = '<div class="alert alert-danger">';
			
				foreach($registration_error_messages as $error){
					$display_errors .= '<p>'.$error[0].'</p>';
				}

			$display_errors .= '</div>';

			echo json_encode(array('error' => true, 'message' => $display_errors));

		} else {
			echo json_encode(array('error' => false, 'message' => '<div class="alert alert-success">'.__( 'Registration complete. Please check your e-mail.', 'flownews').'</p>'));
		}
	 

	 	wp_die();
}
add_action('wp_ajax_nopriv_flownews_register_member', 'flownews_register_member');


// RESET PASSWORD
function flownews_reset_password(){

		
  		// Get variables
		$username_or_email = $_POST['flownews_user_or_email'];

		// Check CSRF token
		if( !check_ajax_referer( 'ajax-login-nonce', 'password-security', false) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Session token has expired, please reload the page and try again', 'flownews').'</div>'));
		}		

	 	// Check if input variables are empty
	 	elseif( empty($username_or_email) ){
			echo json_encode(array('error' => true, 'message'=> '<div class="alert alert-danger">'.__('Please fill all form fields', 'flownews').'</div>'));
	 	} else {

			$username = is_email($username_or_email) ? sanitize_email($username_or_email) : sanitize_user($username_or_email);

			$user_forgotten = flownews_lostPassword_retrieve($username);
			
			if( is_wp_error($user_forgotten) ){
			
				$lostpass_error_messages = $user_forgotten->errors;

				$display_errors = '<div class="alert alert-warning">';
				foreach($lostpass_error_messages as $error){
					$display_errors .= '<p>'.$error[0].'</p>';
				}
				$display_errors .= '</div>';
				
				echo json_encode(array('error' => true, 'message' => $display_errors));
			}else{
				echo json_encode(array('error' => false, 'message' => '<p class="alert alert-success">'.__('Password Reset. Please check your email.', 'flownews').'</p>'));
			}
	 	}

	 	wp_die();
}	
add_action('wp_ajax_nopriv_flownews_reset_password', 'flownews_reset_password');


function flownews_lostPassword_retrieve( $user_data ) {
		
		global $wpdb, $current_site, $wp_hasher;

		$errors = new WP_Error();

		if( empty($user_data) ){
			$errors->add( 'empty_username', __( 'Please enter a username or e-mail address.', 'flownews' ) );
		} elseif( strpos($user_data, '@') ){
			$user_data = get_user_by( 'email', trim( $user_data ) );
			if( empty($user_data)){
				$errors->add( 'invalid_email', __( 'There is no user registered with that email address.', 'flownews'  ) );
			}
		} else {
			$login = trim( $user_data );
			$user_data = get_user_by('login', $login);
		}

		if( $errors->get_error_code() ){
			return $errors;
		}

		if( !$user_data ){
			$errors->add('invalidcombo', __('Invalid username or e-mail.', 'flownews'));
			return $errors;
		}

		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;

		do_action('retrieve_password', $user_login);

		$allow = apply_filters('allow_password_reset', true, $user_data->ID);

		if( !$allow ){
			return new WP_Error( 'no_password_reset', __( 'Password reset is not allowed for this user', 'flownews' ) );
		} elseif ( is_wp_error($allow) ){
			return $allow;
		}

		$key = wp_generate_password(20, false);

		do_action('retrieve_password_key', $user_login, $key);

		if(empty($wp_hasher)){
			require_once ABSPATH.'wp-includes/class-phpass.php';
			$wp_hasher = new PasswordHash(8, true);
		}

		$hashed = $wp_hasher->HashPassword($key);

		$wpdb->update($wpdb->users, array('user_activation_key' => $hashed), array('user_login' => $user_login));
		
		$message = __('Someone requested that the password be reset for the following account:', 'flownews' ) . "\r\n\r\n";
		$message .= esc_url(network_home_url( '/' )) . "\r\n\r\n";
		$message .= sprintf( __( 'Username: %s', 'flownews' ), $user_login ) . "\r\n\r\n";
		$message .= __('If this was a mistake, just ignore this email and nothing will happen.', 'flownews' ) . "\r\n\r\n";
		$message .= __('To reset your password, visit the following address:', 'flownews' ) . "\r\n\r\n";
		$message .= '<' . esc_url(network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' )) . ">\r\n\r\n";
		
		if ( is_multisite() ) {
			$blogname = $GLOBALS['current_site']->site_name;
		} else {
			$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		}

		$title   = sprintf( __( '[%s] Password Reset', 'flownews' ), $blogname );
		$title   = apply_filters( 'retrieve_password_title', $title );
		$message = apply_filters( 'retrieve_password_message', $message, $key );

		if ( $message && ! wp_mail( $user_email, $title, $message ) ) {
			$errors->add( 'noemail', __( 'The e-mail could not be sent.<br />Possible reason: your host may have disabled the mail() function.', 'flownews' ) );

			return $errors;

			wp_die();
		}

		return true;
}