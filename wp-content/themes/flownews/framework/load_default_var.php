<?php
/**
 * FlowNews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 *
 */
 
 # Load Empty General Var 
 if(!isset($flownews_theme['layout-type'])) : $flownews_theme['layout-type'] = 'flownews-fullwidth'; endif;
 if(!isset($flownews_theme['layout-content'])) : $flownews_theme['layout-content'] = 'flownews-layout-default'; endif;
 if(!isset($flownews_theme['preloader'])) : $flownews_theme['preloader'] = true; endif;
 if(!isset($flownews_theme['preloader-type'])) : $flownews_theme['preloader-type'] = 'cube'; endif; 
 if(!isset($flownews_theme['favicon']['url'])) : $flownews_theme['favicon']['url'] = ''; endif;
 if(!isset($flownews_theme['logo']['url'])) : $flownews_theme['logo']['url'] = ''; endif;
 if(!isset($flownews_theme['logo-mobile']['url'])) : $flownews_theme['logo-mobile']['url'] = ''; endif;
 if(!isset($flownews_theme['pagination'])) : $flownews_theme['pagination'] = 'standard'; endif;
 if(!isset($flownews_theme['bg-types'])) : $flownews_theme['bg-types'] = 'color'; endif;
 if(!isset($flownews_theme['bg-color']['rgba'])) : $flownews_theme['bg-color']['rgba'] = 'rgba(255,255,255,1)'; endif;
 if(!isset($flownews_theme['bg-pattern'])) : $flownews_theme['bg-pattern'] = ''; endif;
 if(!isset($flownews_theme['bg-image']['url'])) : $flownews_theme['bg-image']['url'] = ''; endif;
 if(!isset($flownews_theme['rtl'])) : $flownews_theme['rtl'] = false; endif;
 if(!isset($flownews_theme['header-slider'])) : $flownews_theme['header-slider'] = false; endif;
 if(!isset($flownews_theme['header-slider-position'])) : $flownews_theme['header-slider-position'] = 'homepage'; endif;
 if(!isset($flownews_theme['header-slider-shortcode'])) : $flownews_theme['header-slider-shortcode'] = ''; endif;
 if(!isset($flownews_theme['header-slider-container'])) : $flownews_theme['header-slider-container'] = 'fullwidth'; endif;
 
 # Load Empty Header Var
 if(!isset($flownews_theme['header-top-active'])) : $flownews_theme['header-top-active'] = true; endif;
 if(!isset($flownews_theme['header-date-format'])) : $flownews_theme['header-date-format'] = 'l, F j, Y'; endif;
 if(!isset($flownews_theme['news-ticker-style'])) : $flownews_theme['news-ticker-style'] = 'style1'; endif;
 if(!isset($flownews_theme['news-ticker-autoplay'])) : $flownews_theme['news-ticker-autoplay'] = ''; endif;
 if(!isset($flownews_theme['news-ticker-num-posts'])) : $flownews_theme['news-ticker-num-posts'] = ''; endif;
 if(!isset($flownews_theme['news-ticker-posts-source'])) : $flownews_theme['news-ticker-posts-source'] = 'all_posts'; endif;
 if(!isset($flownews_theme['news-ticker-categories'])) : $flownews_theme['news-ticker-categories'] = ''; endif;
 if(!isset($flownews_theme['news-ticker-order'])) : $flownews_theme['news-ticker-order'] = 'DESC'; endif;
 if(!isset($flownews_theme['news-ticker-orderby'])) : $flownews_theme['news-ticker-orderby'] = 'date'; endif;
 if(!isset($flownews_theme['type-header-top-right'])) : $flownews_theme['type-header-top-right'] = 'social'; endif;
 if(!isset($flownews_theme['header-login-register'])) : $flownews_theme['header-login-register'] = '7'; endif;
 if(!isset($flownews_theme['facebook'])) : $flownews_theme['facebook'] = '#'; endif;
 if(!isset($flownews_theme['twitter'])) : $flownews_theme['twitter'] = '#'; endif;
 if(!isset($flownews_theme['googleplus'])) : $flownews_theme['googleplus'] = '#'; endif;
 if(!isset($flownews_theme['instagram'])) : $flownews_theme['instagram'] = '#'; endif;
 if(!isset($flownews_theme['linkedin'])) : $flownews_theme['linkedin'] = '#'; endif;
 if(!isset($flownews_theme['vimeo'])) : $flownews_theme['vimeo'] = '#'; endif;
 if(!isset($flownews_theme['youtube'])) : $flownews_theme['youtube'] = '#'; endif;
 if(!isset($flownews_theme['top-menu'])) : $flownews_theme['top-menu'] = ''; endif;
 if(!isset($flownews_theme['header-top-order'])) : 
	$flownews_theme['header-top-order'] = Array( 'enabled' => Array ( 	'placebo' => 'placebo',
																			'newsticker' 	=> 'News Ticker',
																			'date' 			=> 'Date',
																			'menu-social' 	=> 'Menu/Social',
																			'login' 		=> 'Login/Register'
																),
													'disable' => Array ( 'placebo' => 'placebo' )			
											);
 endif; 
 if(!isset($flownews_theme['header-top-align'])) : $flownews_theme['header-top-align'] = 'default'; endif;
 if(!isset($flownews_theme['header-middle-logo-potision'])) : $flownews_theme['header-middle-logo-potision'] = 'left'; endif;
 if(!isset($flownews_theme['header-middle-bg-types'])) : $flownews_theme['header-middle-bg-types'] = 'color'; endif;
 if(!isset($flownews_theme['header-middle-bg-color'])) : $flownews_theme['header-middle-bg-color'] = '#f6f6f6'; endif;
 if(!isset($flownews_theme['header-middle-bg-pattern'])) : $flownews_theme['header-middle-bg-pattern'] = ''; endif;
 if(!isset($flownews_theme['header-middle-bg-image'])) : $flownews_theme['header-middle-bg-image']['url'] = ''; endif;
 if(!isset($flownews_theme['search'])) : $flownews_theme['search'] = true; endif;
 if(!isset($flownews_theme['header-menu-align'])) : $flownews_theme['header-menu-align'] = 'left'; endif;
 if(!isset($flownews_theme['header-menu-style'])) : $flownews_theme['header-menu-style'] = 'header-menu-style1'; endif;
 if(!isset($flownews_theme['header-order'])) : 
	$flownews_theme['header-order'] = Array( 'enabled' => Array ( 	'placebo' => 'placebo',
																	'header-top' => 'Header Top',
																	'header-middle' => 'Header Middle',
																	'header-bottom' => 'Header Bottom'
																)
											); 
 endif;
 if(!isset($flownews_theme['header-sticky'])) : $flownews_theme['header-sticky'] = true; endif;
 if(!isset($flownews_theme['header-sticky-logo-position'])) : $flownews_theme['header-sticky-logo-position'] = 'left'; endif;
 if(!isset($flownews_theme['logo-sticky'])) : $flownews_theme['logo-sticky']['url'] = ''; endif;
 
 # Load Empty Footer Var
 if(!isset($flownews_theme['footer-top-active'])) : $flownews_theme['footer-top-active'] = false; endif;
 if(!isset($flownews_theme['footer-top-widget'])) : $flownews_theme['footer-top-widget'] = 'footer-top-widget-3'; endif;
 if(!isset($flownews_theme['back-to-top'])) : $flownews_theme['back-to-top'] = true; endif;
 if(!isset($flownews_theme['footer-bottom-active'])) : $flownews_theme['footer-bottom-active'] = true; endif;
 if(!isset($flownews_theme['footer-bottom-type'])) : 
	$flownews_theme['footer-bottom-type'] = Array( 'enabled' => Array ( 	'placebo' => 'placebo',
																			'text' => 'Text',
																			'social' => 'Social',
																			'menu' => 'Menu'
																),
													'disable' => Array ( 'placebo' => 'placebo' )			
											);
 endif;
 if(!isset($flownews_theme['footer-text'])) : $flownews_theme['footer-text'] = ''; endif;
 if(!isset($flownews_theme['footer-facebook'])) : $flownews_theme['footer-facebook'] = '#'; endif;
 if(!isset($flownews_theme['footer-twitter'])) : $flownews_theme['footer-twitter'] = '#'; endif;
 if(!isset($flownews_theme['footer-googleplus'])) : $flownews_theme['footer-googleplus'] = '#'; endif;
 if(!isset($flownews_theme['footer-instagram'])) : $flownews_theme['footer-instagram'] = '#'; endif;
 if(!isset($flownews_theme['footer-linkedin'])) : $flownews_theme['footer-linkedin'] = '#'; endif;
 if(!isset($flownews_theme['footer-vimeo'])) : $flownews_theme['footer-vimeo'] = '#'; endif;
 if(!isset($flownews_theme['footer-youtube'])) : $flownews_theme['footer-youtube'] = '#'; endif;
 if(!isset($flownews_theme['footer-top-menu'])) : $flownews_theme['footer-top-menu'] = ''; endif;
 if(!isset($flownews_theme['footer-image-background-active'])) : $flownews_theme['footer-image-background-active'] = false; endif;
 
 # Load Empty Post Var
 if(!isset($flownews_theme['flownews_panel_post_sidebar'])) : $flownews_theme['flownews_panel_post_sidebar'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_post_sidebar_name'])) : $flownews_theme['flownews_panel_post_sidebar_name'] = 'flownews-default'; endif;
 if(!isset($flownews_theme['flownews_panel_post_layout'])) : $flownews_theme['flownews_panel_post_layout'] = 'flownews-post-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_post_social_share'])) : $flownews_theme['flownews_panel_post_social_share'] = true; endif;
 if(!isset($flownews_theme['flownews_panel_post_pagination'])) : $flownews_theme['flownews_panel_post_pagination'] = true; endif;
 if(!isset($flownews_theme['flownews_panel_post_author_bio'])) : $flownews_theme['flownews_panel_post_author_bio'] = 'hidden'; endif;
 if(!isset($flownews_theme['flownews_panel_post_related_posts'])) : $flownews_theme['flownews_panel_post_related_posts'] = false; endif;
 if(!isset($flownews_theme['flownews_panel_post_article_info'])) : $flownews_theme['flownews_panel_post_article_info'] = false; endif;
 if(!isset($flownews_theme['flownews_panel_post_show_tags'])) : $flownews_theme['flownews_panel_post_show_tags'] = true; endif;
 
 # Load Empty General Var 
 if(!isset($flownews_theme['flownews_panel_404_sidebar_position'])) : $flownews_theme['flownews_panel_404_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_archive_sidebar_position'])) : $flownews_theme['flownews_panel_archive_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_archive_layout'])) : $flownews_theme['flownews_panel_archive_layout'] = 'flownews-posts-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_archive_columns'])) : $flownews_theme['flownews_panel_archive_columns'] = '2'; endif;
 if(!isset($flownews_theme['flownews_panel_archive_layout_type'])) : $flownews_theme['flownews_panel_archive_layout_type'] = 'grid'; endif;
 if(!isset($flownews_theme['flownews_panel_author_sidebar_position'])) : $flownews_theme['flownews_panel_author_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_author_layout'])) : $flownews_theme['flownews_panel_author_layout'] = 'flownews-posts-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_author_columns'])) : $flownews_theme['flownews_panel_author_columns'] = '2'; endif;
 if(!isset($flownews_theme['flownews_panel_author_layout_type'])) : $flownews_theme['flownews_panel_author_layout_type'] = 'grid'; endif;
 if(!isset($flownews_theme['flownews_panel_category_sidebar_position'])) : $flownews_theme['flownews_panel_category_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_category_layout'])) : $flownews_theme['flownews_panel_category_layout'] = 'flownews-posts-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_category_columns'])) : $flownews_theme['flownews_panel_category_columns'] = '2'; endif;
 if(!isset($flownews_theme['flownews_panel_category_layout_type'])) : $flownews_theme['flownews_panel_category_layout_type'] = 'grid'; endif;
 if(!isset($flownews_theme['flownews_panel_category_description'])) : $flownews_theme['flownews_panel_category_description'] = 'off'; endif;
 if(!isset($flownews_theme['flownews_panel_image_sidebar_position'])) : $flownews_theme['flownews_panel_image_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_search_sidebar_position'])) : $flownews_theme['flownews_panel_search_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_search_layout'])) : $flownews_theme['flownews_panel_search_layout'] = 'flownews-posts-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_search_columns'])) : $flownews_theme['flownews_panel_search_columns'] = '2'; endif;
 if(!isset($flownews_theme['flownews_panel_search_layout_type'])) : $flownews_theme['flownews_panel_search_layout_type'] = 'grid'; endif;
 if(!isset($flownews_theme['flownews_panel_tag_sidebar_position'])) : $flownews_theme['flownews_panel_tag_sidebar_position'] = 'sidebar-right'; endif;
 if(!isset($flownews_theme['flownews_panel_tag_layout'])) : $flownews_theme['flownews_panel_tag_layout'] = 'flownews-posts-layout1'; endif;
 if(!isset($flownews_theme['flownews_panel_tag_columns'])) : $flownews_theme['flownews_panel_tag_columns'] = '2'; endif;
 if(!isset($flownews_theme['flownews_panel_tag_layout_type'])) : $flownews_theme['flownews_panel_tag_layout_type'] = 'grid'; endif;
 
 # Load Empty Style Var 
 if(!isset($flownews_theme['preset'])) : $flownews_theme['preset'] = 'default'; endif;
 if(!isset($flownews_theme['main-color'])) : $flownews_theme['main-color'] = '#e7685d'; endif;
 if(!isset($flownews_theme['secondary-color'])) : $flownews_theme['secondary-color'] = '#c9564c'; endif;
 if(!isset($flownews_theme['header_top_background'])) : $flownews_theme['header_top_background'] = '#000000'; endif;
 if(!isset($flownews_theme['header_top_text'])) : $flownews_theme['header_top_text'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['header_top_line'])) : $flownews_theme['header_top_line'] = '#333333'; endif;
 if(!isset($flownews_theme['header_bottom_background'])) : $flownews_theme['header_bottom_background'] = '#282828'; endif;
 if(!isset($flownews_theme['header_bottom_line'])) : $flownews_theme['header_bottom_line'] = '#333333'; endif;
 if(!isset($flownews_theme['header_bottom_text_menu'])) : $flownews_theme['header_bottom_text_menu'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['header_bottom_main_text_menu'])) : $flownews_theme['header_bottom_main_text_menu'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['header_bottom_text_submenu'])) : $flownews_theme['header_bottom_text_submenu'] = '#333333'; endif;
 if(!isset($flownews_theme['header_bottom_background_submenu'])) : $flownews_theme['header_bottom_background_submenu'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['header_bottom_border_submenu'])) : $flownews_theme['header_bottom_border_submenu'] = '#f4f4f4'; endif;
 if(!isset($flownews_theme['content_background'])) : $flownews_theme['content_background'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['content_title'])) : $flownews_theme['content_title'] = '#333333'; endif;
 if(!isset($flownews_theme['content_text'])) : $flownews_theme['content_text'] = '#747474'; endif;
 if(!isset($flownews_theme['content_text_info'])) : $flownews_theme['content_text_info'] = '#646464'; endif;
 if(!isset($flownews_theme['content_navigation_background'])) : $flownews_theme['content_navigation_background'] = '#f4f4f4'; endif;
 if(!isset($flownews_theme['content_post'])) : $flownews_theme['content_post'] = '#ffffff'; endif;
 if(!isset($flownews_theme['footer_top_background'])) : $flownews_theme['footer_top_background'] = '#282828'; endif;
 if(!isset($flownews_theme['footer_top_title'])) : $flownews_theme['footer_top_title'] = '#FFFFFF'; endif;
 if(!isset($flownews_theme['footer_top_text'])) : $flownews_theme['footer_top_text'] = '#747474'; endif;
 if(!isset($flownews_theme['footer_top_line'])) : $flownews_theme['footer_top_line'] = '#333333'; endif;
 if(!isset($flownews_theme['footer_bottom_background'])) : $flownews_theme['footer_bottom_background'] = '#000000'; endif;
 if(!isset($flownews_theme['footer_bottom_text'])) : $flownews_theme['footer_bottom_text'] = '#b7b7b7'; endif;
 if(!isset($flownews_theme['footer_bottom_line'])) : $flownews_theme['footer_bottom_line'] = '#333333'; endif;
 if(!isset($flownews_theme['main-typography'])) : $flownews_theme['main-typography'] = array('font-style' => '','font-family' => 'lato', 'font-weight' => '', 'subsets' => '', 'google' => false); endif;
 if(!isset($flownews_theme['p-typography'])) : $flownews_theme['p-typography'] = array('font-style' => '','font-family' => 'lato', 'font-weight' => '', 'subsets' => '', 'google' => false); endif; 
 
 # Load Empty ADV Var 
 if(!isset($flownews_theme['advertisement-top'])) : $flownews_theme['advertisement-top'] = false; endif;
 if(!isset($flownews_theme['advertisement-top-type'])) : $flownews_theme['advertisement-top-type'] = 'banner-image'; endif;
 if(!isset($flownews_theme['advertisement-top-banner'])) : $flownews_theme['advertisement-top-banner']['url'] = ''; endif;
 if(!isset($flownews_theme['advertisement-top-banner-link'])) : $flownews_theme['advertisement-top-banner-link'] = '#'; endif;
 if(!isset($flownews_theme['advertisement-top-banner-link-target'])) : $flownews_theme['advertisement-top-banner-link-target'] = '_blank'; endif;
 if(!isset($flownews_theme['advertisement-top-banner-custom-code'])) : $flownews_theme['advertisement-top-banner-custom-code'] = ''; endif;
 if(!isset($flownews_theme['advertisement-content'])) : $flownews_theme['advertisement-content'] = 'all'; endif;
 if(!isset($flownews_theme['advertisement-content-bottom-type'])) : $flownews_theme['advertisement-content-bottom-type'] = 'banner-image'; endif;
 if(!isset($flownews_theme['advertisement-content-banner'])) : $flownews_theme['advertisement-content-banner']['url'] = ''; endif;
 if(!isset($flownews_theme['advertisement-content-banner-link'])) : $flownews_theme['advertisement-content-banner-link'] = '#'; endif;
 if(!isset($flownews_theme['advertisement-content-banner-link-target'])) : $flownews_theme['advertisement-content-banner-link-target'] = '_blank'; endif;
 if(!isset($flownews_theme['advertisement-content-bottom-banner-custom-code'])) : $flownews_theme['advertisement-content-bottom-banner-custom-code'] = ''; endif;
 if(!isset($flownews_theme['advertisement-content-top'])) : $flownews_theme['advertisement-content-top'] = 'disable'; endif;
 if(!isset($flownews_theme['advertisement-content-top-type'])) : $flownews_theme['advertisement-content-top-type'] = 'banner-image'; endif;
 if(!isset($flownews_theme['advertisement-content-top-banner'])) : $flownews_theme['advertisement-content-top-banner']['url'] = ''; endif;
 if(!isset($flownews_theme['advertisement-content-top-banner-link'])) : $flownews_theme['advertisement-content-top-banner-link'] = '#'; endif;
 if(!isset($flownews_theme['advertisement-content-top-banner-link-target'])) : $flownews_theme['advertisement-content-top-banner-link-target'] = '_blank'; endif;
 if(!isset($flownews_theme['advertisement-content-top-banner-custom-code'])) : $flownews_theme['advertisement-content-top-banner-custom-code'] = ''; endif;
 if(!isset($flownews_theme['advertisement-footer'])) : $flownews_theme['advertisement-footer'] = false; endif;
 if(!isset($flownews_theme['advertisement-footer-type'])) : $flownews_theme['advertisement-footer-type'] = 'banner-image'; endif;
 if(!isset($flownews_theme['advertisement-footer-banner'])) : $flownews_theme['advertisement-footer-banner']['url'] = ''; endif;
 if(!isset($flownews_theme['advertisement-footer-banner-link'])) : $flownews_theme['advertisement-footer-banner-link'] = '#'; endif;
 if(!isset($flownews_theme['advertisement-footer-banner-link-target'])) : $flownews_theme['advertisement-footer-banner-link-target'] = '_blank'; endif;
 if(!isset($flownews_theme['advertisement-footer-banner-custom-code'])) : $flownews_theme['advertisement-footer-banner-custom-code'] = ''; endif; 
 
 # Load Empty Sidebar Var
 if(!isset($flownews_theme['custom-sidebar-name'])) : $flownews_theme['custom-sidebar-name'] = ''; endif;

 # Load Empty Woocommerce Var
 if(!isset($flownews_theme['flownews_woocommerce_sidebar_position'])) : $flownews_theme['flownews_woocommerce_sidebar_position'] = 'sidebar-right'; endif; 
 if(!isset($flownews_theme['flownews_woocommerce_add_to_cart'])) : $flownews_theme['flownews_woocommerce_add_to_cart'] = false; endif;

 # Load Empty bbPress Var
 if(!isset($flownews_theme['flownews_bbpress_sidebar_position'])) : $flownews_theme['flownews_bbpress_sidebar_position'] = 'sidebar-right'; endif; 
 if(!isset($flownews_theme['flownews_bbpress_sidebar_name'])) : $flownews_theme['flownews_bbpress_sidebar_name'] = 'flownews-bbpress'; endif;

 # Load Empty BuddyPress Var
 if(!isset($flownews_theme['flownews_buddypress_sidebar_position'])) : $flownews_theme['flownews_buddypress_sidebar_position'] = 'sidebar-right'; endif; 
 if(!isset($flownews_theme['flownews_buddypress_sidebar_name'])) : $flownews_theme['flownews_buddypress_sidebar_name'] = 'flownews-buddypress'; endif;

 # Load Empty Custom Code Var 
 if(!isset($flownews_theme['css-custom-code'])) : $flownews_theme['css-custom-code'] = ''; endif;
 if(!isset($flownews_theme['js-custom-code'])) : $flownews_theme['js-custom-code'] = ''; endif;

 # Load Empty Speed Site
 if(!isset($flownews_theme['flownews_lazy_load'])) : $flownews_theme['flownews_lazy_load'] = false; endif;
 if(!isset($flownews_theme['flownews_min_assets'])) : $flownews_theme['flownews_min_assets'] = false; endif;
 
?>