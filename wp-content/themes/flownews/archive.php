<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */

 global $flownews_theme;
 get_header(); 
  
 $sidebar = $flownews_theme['flownews_panel_archive_sidebar_position'];
 if(!isset($sidebar) || $sidebar == '') : $sidebar = 'sidebar-right'; endif;
 
 $layout_class = 'blog-layout';
 $layout_type = 'blog-layout';
?>
 
 <!-- start:page section -->
 <section class="flownews-container flownews-wrap-container flownews-page <?php echo $layout_class; ?> flownews-<?php echo $sidebar; ?> element-no-padding">
 
	 <?php if($sidebar == 'sidebar-none') : ?> 
     <!-- start:sidebar none - full width -->
        <div class="flownews-content col-xs-12 post-full-width <?php echo $layout_type; ?>">
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php
                        if ( is_day() ) :
                            printf( esc_html__( 'Daily Archives: %s', 'flownews' ), get_the_date() );
                        elseif ( is_month() ) :
                            printf( esc_html__( 'Monthly Archives: %s', 'flownews' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'flownews' ) ) );
                        elseif ( is_year() ) :
                            printf( esc_html__( 'Yearly Archives: %s', 'flownews' ), get_the_date( _x( 'Y', 'yearly archives date format', 'flownews' ) ) );
                        elseif ( has_post_format('image') ) :
							esc_html_e( 'Format Image', 'flownews' );
                        elseif ( has_post_format('video') ) :
							esc_html_e( 'Format Video', 'flownews' );
                        elseif ( has_post_format('audio') ) :
							esc_html_e( 'Format Audio', 'flownews' );
                        elseif ( has_post_format('gallery') ) :
							esc_html_e( 'Format Gallery', 'flownews' );
                        elseif ( has_post_format('quote') ) :
							esc_html_e( 'Format Quote', 'flownews' );
                        elseif ( has_post_format('link') ) :
							esc_html_e( 'Format Link', 'flownews' );																																										
						else :
                            esc_html_e( 'Archives', 'flownews' );
                        endif;
                    ?></span>
            </h2>
            <!-- start:page content -->
            <?php get_template_part('elements/loop-general'); ?>
            <!-- end:page content -->	
        </div>
     <!-- end:sidebar none - full width -->
     <?php endif; ?>
 
	 <?php if($sidebar == 'sidebar-left') : ?> 
     <!-- start:sidebar left -->
        <?php get_template_part('sidebar'); ?> 
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>"> 
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php
                        if ( is_day() ) :
                            printf( esc_html__( 'Daily Archives: %s', 'flownews' ), get_the_date() );
                        elseif ( is_month() ) :
                            printf( esc_html__( 'Monthly Archives: %s', 'flownews' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'flownews' ) ) );
                        elseif ( is_year() ) :
                            printf( esc_html__( 'Yearly Archives: %s', 'flownews' ), get_the_date( _x( 'Y', 'yearly archives date format', 'flownews' ) ) );
                        elseif ( has_post_format('image') ) :
							esc_html_e( 'Format Image', 'flownews' );
                        elseif ( has_post_format('video') ) :
							esc_html_e( 'Format Video', 'flownews' );
                        elseif ( has_post_format('audio') ) :
							esc_html_e( 'Format Audio', 'flownews' );
                        elseif ( has_post_format('gallery') ) :
							esc_html_e( 'Format Gallery', 'flownews' );
                        elseif ( has_post_format('quote') ) :
							esc_html_e( 'Format Quote', 'flownews' );
                        elseif ( has_post_format('link') ) :
							esc_html_e( 'Format Link', 'flownews' );																																										
						else :
                            esc_html_e( 'Archives', 'flownews' );
                        endif;
                    ?></span>
            </h2>
            <!-- start:page content -->
			<?php get_template_part('elements/loop-general'); ?>
            <!-- end:page content --> 
        </div>
     <!-- end:sidebar left -->
     <?php endif; ?>
 


 
	 <?php if($sidebar == 'sidebar-right') : ?>    
     <!-- start:sidebar left -->
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>">
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php
                        if ( is_day() ) :
                            printf( esc_html__( 'Daily Archives: %s', 'flownews' ), get_the_date() );
                        elseif ( is_month() ) :
                            printf( esc_html__( 'Monthly Archives: %s', 'flownews' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'flownews' ) ) );
                        elseif ( is_year() ) :
                            printf( esc_html__( 'Yearly Archives: %s', 'flownews' ), get_the_date( _x( 'Y', 'yearly archives date format', 'flownews' ) ) );
                        elseif ( has_post_format('image') ) :
							esc_html_e( 'Format Image', 'flownews' );
                        elseif ( has_post_format('video') ) :
							esc_html_e( 'Format Video', 'flownews' );
                        elseif ( has_post_format('audio') ) :
							esc_html_e( 'Format Audio', 'flownews' );
                        elseif ( has_post_format('gallery') ) :
							esc_html_e( 'Format Gallery', 'flownews' );
                        elseif ( has_post_format('quote') ) :
							esc_html_e( 'Format Quote', 'flownews' );
                        elseif ( has_post_format('link') ) :
							esc_html_e( 'Format Link', 'flownews' );																																										
						else :
                            esc_html_e( 'Archives', 'flownews' );
                        endif;
                    ?></span>
            </h2>
            <!-- start:page content -->
			<?php get_template_part('elements/loop-general'); ?>
            <!-- end:page content --> 
        </div>    
        <?php get_template_part('sidebar'); ?>
     <!-- end:sidebar left -->
     <?php endif; ?>
     
 	<div class="clearfix"></div>
 </section>
 <!-- end:page section -->
 
 
 <?php get_footer(); ?>