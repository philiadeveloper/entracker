<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */

 global $flownews_theme;
 get_header(); 
  
 $sidebar = $flownews_theme['flownews_panel_search_sidebar_position'];
 if(!isset($sidebar) || $sidebar == '') : $sidebar = 'sidebar-right'; endif;
 
 $layout_class = 'blog-layout';
 $layout_type = 'blog-layout';
?>
 
 <!-- start:page section -->
 <section class="flownews-container flownews-wrap-container flownews-page <?php echo $layout_class; ?> flownews-<?php echo $sidebar; ?> element-no-padding">
 
	 <?php if($sidebar == 'sidebar-none') : ?> 
     <!-- start:sidebar none - full width -->
        <div class="flownews-content col-xs-12 post-full-width <?php echo $layout_type; ?>">
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php printf( esc_html__( 'Search Results for: %s', 'flownews' ), '<span>' . get_search_query() . '</span>' ); ?></span>
            </h2>		
            <!-- start:page content -->
            <?php get_template_part('elements/loop-search'); ?>
            <!-- end:page content -->	
        </div>
     <!-- end:sidebar none - full width -->
     <?php endif; ?>
 
	 <?php if($sidebar == 'sidebar-left') : ?> 
     <!-- start:sidebar left -->
        <?php get_template_part('sidebar'); ?> 
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>"> 
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php printf( esc_html__( 'Search Results for: %s', 'flownews' ), '<span>' . get_search_query() . '</span>' ); ?></span>
            </h2>
            <!-- start:page content -->
			<?php get_template_part('elements/loop-search'); ?>
            <!-- end:page content --> 
        </div>
     <!-- end:sidebar left -->
     <?php endif; ?>
 


 
	 <?php if($sidebar == 'sidebar-right') : ?>    
     <!-- start:sidebar left -->
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>">
		    <h2 class="flownews-title-page-container">
				<span class="flownews-title-page"><?php printf( esc_html__( 'Search Results for: %s', 'flownews' ), '<span>' . get_search_query() . '</span>' ); ?></span>
            </h2>		
            <!-- start:page content -->
			<?php get_template_part('elements/loop-search'); ?>
            <!-- end:page content --> 
        </div>    
        <?php get_template_part('sidebar'); ?>
     <!-- end:sidebar left -->
     <?php endif; ?>
     
 	<div class="clearfix"></div>
 </section>
 <!-- end:page section -->
 
 
 <?php get_footer(); ?>