<?php
/**
 * flownews Theme
 *
 * Theme by: AD-Theme
 * Our portfolio: http://themeforest.net/user/ad-theme/portfolio
 */

 global $flownews_theme;
 get_header(); 
  
 $sidebar = $flownews_theme['flownews_panel_image_sidebar_position'];
 if(!isset($sidebar) || $sidebar == '') : $sidebar = 'sidebar-right'; endif;
 
 $layout_class = 'blog-layout';
 $layout_type = 'blog-layout';
?>
 
 <!-- start:page section -->
 <section class="flownews-container flownews-wrap-container flownews-page <?php echo $layout_class; ?> flownews-<?php echo $sidebar; ?> element-no-padding">
 
	 <?php if($sidebar == 'sidebar-none') : ?> 
     <!-- start:sidebar none - full width -->
        <div class="flownews-content col-xs-12 post-full-width <?php echo $layout_type; ?>">	
            <!-- start:page content -->
			<?php if ( have_posts() ) : ?>
					<h2 class="flownews-title-page-container">
						<span class="flownews-title-page"><?php the_title(); ?></span>
					</h2>                                     
					<div class="attachment-image">
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt="image" >
					</div>
					<div class="text attachment-container">
						<?php $thumb_array = get_post( get_post_thumbnail_id() ); ?>
						<span class="attachment-image-id"><?php echo esc_html__('ID: ','flownews'); ?><?php echo $thumb_array->ID; ?></span>
						<span class="attachment-image-author"><?php echo esc_html__('Author: ','flownews'); ?><?php echo the_author_meta( 'display_name', $thumb_array->post_author ); ?></span>                          
						<span class="attachment-image-date"><?php echo esc_html__('Date: ','flownews'); ?><?php echo $thumb_array->post_date; ?></span>                          
						<span class="attachment-image-description"><?php echo esc_html__('Description: ','flownews'); ?><?php echo $thumb_array->post_content; ?></span>                          
						<span class="attachment-image-caption"><?php echo esc_html__('Caption: ','flownews'); ?><?php echo $thumb_array->post_excerpt; ?></span>                          
						<span class="attachment-image-type"><?php echo esc_html__('Type: ','flownews'); ?><?php echo $thumb_array->post_mime_type; ?></span>                
					</div>                           
				 <!-- end:page content -->
			<?php endif; ?>     
            <!-- end:page content -->	
        </div>
     <!-- end:sidebar none - full width -->
     <?php endif; ?>
 
	 <?php if($sidebar == 'sidebar-left') : ?> 
     <!-- start:sidebar left -->
        <?php get_template_part('sidebar'); ?> 
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>">
            <!-- start:page content -->
			<?php if ( have_posts() ) : ?>
					<h2 class="flownews-title-page-container">
						<span class="flownews-title-page"><?php the_title(); ?></span>
					</h2>                                     
					<div class="attachment-image">
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt="image" >
					</div>
					<div class="text attachment-container">
						<?php $thumb_array = get_post( get_post_thumbnail_id() ); ?>
						<span class="attachment-image-id"><?php echo esc_html__('ID: ','flownews'); ?><?php echo $thumb_array->ID; ?></span>
						<span class="attachment-image-author"><?php echo esc_html__('Author: ','flownews'); ?><?php echo the_author_meta( 'display_name', $thumb_array->post_author ); ?></span>                          
						<span class="attachment-image-date"><?php echo esc_html__('Date: ','flownews'); ?><?php echo $thumb_array->post_date; ?></span>                          
						<span class="attachment-image-description"><?php echo esc_html__('Description: ','flownews'); ?><?php echo $thumb_array->post_content; ?></span>                          
						<span class="attachment-image-caption"><?php echo esc_html__('Caption: ','flownews'); ?><?php echo $thumb_array->post_excerpt; ?></span>                          
						<span class="attachment-image-type"><?php echo esc_html__('Type: ','flownews'); ?><?php echo $thumb_array->post_mime_type; ?></span>                
					</div>                            
				 <!-- end:page content -->
			<?php endif; ?>     
            <!-- end:page content --> 
        </div>
     <!-- end:sidebar left -->
     <?php endif; ?>
 


 
	 <?php if($sidebar == 'sidebar-right') : ?>    
     <!-- start:sidebar left -->
        <div class="flownews-content col-xs-9 <?php echo $layout_type; ?>">
            <!-- start:page content -->
			<?php if ( have_posts() ) : ?>
					<h2 class="flownews-title-page-container">
						<span class="flownews-title-page"><?php the_title(); ?></span>
					</h2>                                     
					<div class="attachment-image">
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt="image" >
					</div>
					<div class="text attachment-container">
						<?php $thumb_array = get_post( get_post_thumbnail_id() ); ?>
						<span class="attachment-image-id"><?php echo esc_html__('ID: ','flownews'); ?><?php echo $thumb_array->ID; ?></span>
						<span class="attachment-image-author"><?php echo esc_html__('Author: ','flownews'); ?><?php echo the_author_meta( 'display_name', $thumb_array->post_author ); ?></span>                          
						<span class="attachment-image-date"><?php echo esc_html__('Date: ','flownews'); ?><?php echo $thumb_array->post_date; ?></span>                          
						<span class="attachment-image-description"><?php echo esc_html__('Description: ','flownews'); ?><?php echo $thumb_array->post_content; ?></span>                          
						<span class="attachment-image-caption"><?php echo esc_html__('Caption: ','flownews'); ?><?php echo $thumb_array->post_excerpt; ?></span>                          
						<span class="attachment-image-type"><?php echo esc_html__('Type: ','flownews'); ?><?php echo $thumb_array->post_mime_type; ?></span>                
					</div>                               
				 <!-- end:page content -->
			<?php endif; ?>     
            <!-- end:page content --> 
        </div>    
        <?php get_template_part('sidebar'); ?>
     <!-- end:sidebar left -->
     <?php endif; ?>
     
 	<div class="clearfix"></div>
 </section>
 <!-- end:page section -->
 
 
 <?php get_footer(); ?>