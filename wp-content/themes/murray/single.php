<?php
/**
* The template for displaying all single posts.
*
* @package murray
*/
get_header(); ?>
<div class="row" id="pMain">
	<div class="col-md-9">
	    <div style="height: 100vh;width: 100%;overflow: hidden;">
			<div style="width: 100%;height: 100%;overflow-y: scroll;padding-right: 17px;box-sizing: content-box;">
				<div <?php post_class("col-md-12");?> id="searchParent">
					<?php get_template_part( 'modules/search/search'); ?>
				</div>
		<div id="primary-mono" class="content-area">
			<main id="main" class="site-main" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'modules/content/content', 'single' ); ?>
				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>
				<?php endwhile; // end of the loop. ?>
				</main><!-- #main -->
				</div><!-- #primary -->
			</div>
			</div>
			</div>
			<?php get_template_part( 'modules/content/content', 'sidebar' ); ?>
		</div>
		<?php get_footer(); ?>