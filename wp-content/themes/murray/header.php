<?php
/**
* The header for our theme.
*
* Displays all of the <head> section and everything up till <div id="content">
							*
							* @package murray
							*/
	?>
	<?php get_template_part('modules/header/head');?>
	<body <?php body_class(); ?>>
		<div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'murray' ); ?></a>
			<?php get_template_part('modules/header/jumbosearch'); ?>
			<div id="social-icons-fixed" title="<?php _e('Follow us on Social Media','murray'); ?>">
				<?php get_template_part('modules/social/social', 'soshion'); ?>
			</div>
			<header id="masthead" class="site-header" role="banner">
				<div class="container-fluid" id="pDesktoptopnav">
					<div class="site-branding">
						<?php if ( has_custom_logo() ) : ?>
						<div id="site-logo">
							<?php the_custom_logo(); ?>
						</div>
						<?php endif; ?>
						<div id="text-title-desc">
							<h1 class="site-title title-font"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						</div>
					</div>
					<div id="pNav" style="display: inline-block;width: 80%;">
						<nav id="site-navigation" class="main-navigation single" role="navigation">
							<div class="nav-container container">
								<?php
								// Get the Appropriate Walker First.
								if (has_nav_menu(  'primary' ) && !get_theme_mod('murray_disable_nav_desc',true) ) :
								$walker = new Murray_Menu_With_Icon;
								else :
								$walker = '';
								endif;
								//Display the Menu.
								wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => $walker ) ); ?>
							</div>
						</nav>
					</div>
					<div style="display: inline-block;width: auto;position: absolute;margin-top:37px;margin-left: 40px;">
						<i class="fa fa-search" aria-hidden="true" style="color: white;font-size: 20px;" id="opensearch"></i>
					</div>
				</div>
				<div class="pMobiletopnav" style="display: none;">
					<div class="site-brandings" style="display: inline-block;">
						<?php if ( has_custom_logo() ) : ?>
						<div id="site-logo">
							<?php the_custom_logo(); ?>
						</div>
						<?php endif; ?>
						<h1><a  style="font-size: 35px;" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
					</div>
					<a href="javascript:void(0);" class="icon" onclick="openNav()">
						<i class="fa fa-bars" style="color:white;"></i>
					</a>
				</div>
				<div id="mySidenav" class="sidenav">
					<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					<?php
													if (has_nav_menu(  'primary' ) && !get_theme_mod('murray_disable_nav_desc',true) ) :
													$walker = new Murray_Menu_With_Icon;
													else :
													$walker = '';
													endif;
					wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => $walker ) ); ?>
					<form id="search-forms" role="search" method="get" class="search-form" action="<?php echo esc_url(home_url( '/' )); ?>">
						<!-- <img src="http://localhost/development/entracker/wp-content/uploads/triangle.png" style="z-index:-2;position: absolute;margin-top:-50px;right: 85px;"> -->
						<center>
						<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'murray' ) ?></span>
						<input style="width: 90%;height:30px;margin: auto;border-radius:0px;border:none;background:#1d1d1d;text-indent: 10px;color:white" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder', 'murray' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'murray' ) ?>" />
						</center>
					</form>
				</div>
				<script>
				function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
				}
				function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
				}
				</script>
			</header>
			<div class="mega-container">
				<div id="content" class="site-content container-fluid">