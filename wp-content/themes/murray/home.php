<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package murray
*/
get_header(); ?>
<?php
$settings = $wpdb->get_results("SELECT * FROM entracker_config WHERE id=1",ARRAY_A);
?>
<?php
$args = array(
	'posts_per_page' => 1,
	'post__in'  => get_option( 'sticky_posts' ),
	'ignore_sticky_posts' => 1
);
$sticky_query = new WP_Query( $args );
if ($sticky_query->have_posts()) {
	$sticky_query->the_post();
} ?>
<div class="row" id="pMain">
	<div class="col-md-9" style="margin-top: 10px;">
		<div style="height: 100vh;width: 100%;overflow: hidden;">
			<div style="width: 100%;height: 100%;overflow-y: scroll;padding-right: 17px;box-sizing: content-box;">
				<div id="sticky-post">
					<div <?php post_class("sticky-image col-md-12");?>>
						<?php get_template_part( 'modules/search/search'); ?>
						<h2 style="font-weight: bold;color:black">Stories</h2>
						<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/uploads/owl/css/owl.carousel.min.css">
						<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/uploads/owl/css/owl.theme.default.min.css">
						<script src="<?php echo get_site_url(); ?>/wp-content/uploads/owl/owl.carousel.js"></script>
						<div id="pSliderImage">
							<div class="owl-carousel owl-theme">
								<?php $limit = $settings[0]['top_slider_limit'];?>
								<?php $type = $settings[0]['top_slider_type'];?>
								<?php $repeat = $settings[0]['repeat_middle_adv'];?>
								<?php $front_page_post_count = $settings[0]['front_page_post_count'];?>
								<?php $pAdsStatus = $settings[0]['pAdsStatus'];?>
								<?php
														if($type=='Latest Posts'){
															query_posts("cat=-11&posts_per_page=$limit");
														}else{
															$category = $settings[0]['top_slider_category'];
															query_posts( "cat=-11&cat=$category&posts_per_page=$limit" );
														}
								?>
								<?php while (have_posts()) : the_post(); ?>
								<div class="item">
									<div class="pSliderImgs"> <a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('murray-thumb',array('style'=>'width: 100%;height:auto;margin-top:10px;',  'alt' => trim(strip_tags( $post->post_title )))); ?>
									</a> </div>
								</div>
								<?php endwhile;?>
							</div>
						</div>
						<div id="pSliderText">
							<div style="padding:0 0 0 20px;">
								<?php $i=1;while (have_posts()) : the_post(); ?>
								<h3 class="pHighlight" id="pHighlight<?php echo $i;?>"><a class="pHighlightA" id="pHighlightA<?php echo $i;?>" style="font-weight:<?php echo ($i==1)?'bold':'normal';?>;color:<?php echo ($i==1)?'black':'#6E6E6E';?>" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_title(); ?>
								</a></h3>
								<?php $i++;endwhile;?>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<?php if(($pAdsStatus == 1) AND $settings[0]['add_banner_code_body1']!=''){?>
				<?php echo $settings[0]['add_banner_code_body1'];?>
				<?php } ?>
				<?php wp_reset_postdata(); ?>
				<div id="primary" class="content-areas col-md-12">
					<?php
								for ($i = 1; $i < 3; $i++ ) :
									if (get_theme_mod('murray_featposts_enable'.$i) ) :
										murray_featured_posts(
											get_theme_mod('murray_featposts_title'.$i,
											__("Section Title","murray")),
											get_theme_mod('murray_featposts_cat'.$i,0),
											get_theme_mod('murray_featposts_icon'.$i,'fa-star')
										);
										endif;
								endfor;
					?>
					<main id="main" class="site-main" role="main">
						<?php query_posts("cat=-11&posts_per_page=$front_page_post_count"); ?>
						<?php if ( have_posts() ) : ?>
						<?php  $loop_c = 0;$count=1;$countCenterBox=1;$postCount=1; ?>
						<?php while ( have_posts() ) : the_post(); ?>
						<?php
											$loop_c++;
											if ( ($loop_c == 1) && is_sticky() )
											continue;
						?>
						<?php if(($pAdsStatus == 1) AND $count==$repeat){ ?>
						<article <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
							<div class="featured-thumb col-md-12">
								<?php if($settings[0]['add_banner_code_body2']!=''){?>
								<?php echo $settings[0]['add_banner_code_body2'];$count=1;?>
								<?php } ?>
							</div>
						</article>
						<?php } ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
							<div class="featured-thumb col-md-12">
								<?php if (has_post_thumbnail()) : ?>
								<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
									<?php the_post_thumbnail('murray-pop-thumb',array(  'alt' => trim(strip_tags( $post->post_title )))); ?>
								</a>
								<?php else: ?>
								<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><img alt="<?php the_title() ?>" src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
								<?php endif; ?>
							</div>
							<div class="out-thumb col-md-12">
								<header class="entry-header">
									<h3 class="entry-title title-font"><a href="<?php the_permalink(); ?>" rel="bookmark" style="font-weight:bold;">
										<?php the_title(); ?>
									</a></h3>
									<div class="postedon">
										<?php murray_posted_on(); ?>
									</div>
								</header>
							</div>
							<input type="hidden" class="countClass" value="<?php echo $postCount++;?>">
						</article>
						<?php $show_after = $settings[0]['show_after_n_post'];?>
						<?php if($countCenterBox == $show_after){ ?>
						<?php
										$category = $settings[0]['center_box_category'];
										$categoryCount = $settings[0]['center_box_post_count'];
										$columns = explode(",",$settings[0]['center_box_columns']);
						?>
						<?php
						global $post;
						$args = array( 'posts_per_page' => $categoryCount,'category' => $category,'category' => -11 );
						$myposts = get_posts( $args );
						?>
						<fieldset style="width:100%;border: 1px solid #A12828;box-sizing: border-box;padding: 15px;margin-bottom: 20px;">
							<legend style="width: auto;padding: 10px;border:none;color:#A12828;font-weight:bold;"><?php echo get_the_category_by_ID($category);?>:</legend>
							<div class="row">
								<?php $i=0;foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
								<div class="col-md-4 col-sm-<?php echo $columns[$i];?>">
									<center>
									<?php if (has_post_thumbnail()) : ?>
									<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
										<?php the_post_thumbnail('murray-pop-thumb',array(  'alt' => trim(strip_tags( $post->post_title )))); ?>
									</a>
									<?php else: ?>
									<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><img alt="<?php the_title() ?>" src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
									<?php endif; ?>
									<header class="entry-header">
										<h4 class="entry-title title-font"><a href="<?php the_permalink(); ?>" rel="bookmark" style="color:black;font-size: 19px;">
											<?php the_title(); ?>
										</a></h4>
									</header>
									</center>
								</div>
								<?php $i++;endforeach;?>
								<?php wp_reset_postdata();?>
							</div>
						</fieldset>
						<?php } ?>
						<?php $countCenterBox++;$count++;endwhile; ?>
						<div class="clearfix"></div>
						<?php //murray_pagination(); ?>
						<?php else : ?>
						<?php get_template_part( 'modules/content/content', 'none' ); ?>
						<?php endif; ?>
					</main>
				</div>
				<div class="col-md-12" style="margin-top: 10px;">
					<center>
					<div id="LoadMoreLoading"></div>
					<div id="LoadMoreButton" onclick="fetch()" style="
						margin-top: 50px;
						margin-bottom: 50px;
						background: #F1F1F1;
						width: 200px;
						text-align: center;
						padding: 10px 30px;
						cursor: pointer;"> <span style="color: #9A9A9A">Load More</span> </div>
						</center>
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part( 'modules/content/content', 'sidebar' ); ?>
	</div>
	<?php get_footer(); ?>