<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package murray
*/
?>
</div><!-- #content -->
<?php get_sidebar('footer'); ?>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="site-info container-fluid" style="text-align: left;padding-left: 35px;color:white;">
    <span class="sep"></span>
    <?php echo ( get_theme_mod('murray_footer_text') == '' ) ? sprintf( __( '&copy; %1$s %2$s. All Rights Reserved.', 'murray' ), date_i18n( 'Y' ), get_bloginfo( 'name' ) ) : esc_html(get_theme_mod('murray_footer_text')); ?>
    </div><!-- .site-info -->
    </footer><!-- #colophon -->
    </div><!-- #page -->
    <?php wp_footer(); ?>
    <script type="text/javascript">
    jQuery(document).ready(function() {
    var length = jQuery(".item").length;
    var c = 1;
    jQuery(".owl-dot").each(function() {
        jQuery(this).attr("data-id", c++);
    });
    var count = 1;
    var countMax = length;
    jQuery(".owl-next").on("click", function() {
        jQuery(".pHighlightA").css({
            "color": "#6E6E6E",
            "font-weight": "normal"
        });
        if (count <= countMax) {
            if (count < countMax) {
                count++;
            }
            jQuery("#pHighlightA" + count).css({
                "color": "black",
                "font-weight": "bold"
            });
        } else {
            return false;
        }
    });
    jQuery(".owl-prev").on("click", function() {
        jQuery(".pHighlightA").css({
            "color": "#6E6E6E",
            "font-weight": "normal"
        });
        if (count == 1) {
            jQuery("#pHighlightA" + count).css({
                "color": "black",
                "font-weight": "bold"
            });
            return false;
        } else {
            count--;
            jQuery("#pHighlightA" + count).css({
                "color": "black",
                "font-weight": "bold"
            });
        }
    });
    jQuery(".owl-dot").on("click", function() {
        jQuery(".pHighlightA").css({
            "color": "#6E6E6E",
            "font-weight": "normal"
        });
        var number = jQuery(this).attr("data-id");
        count = number;
        jQuery("#pHighlightA" + number).css({
            "color": "black",
            "font-weight": "bold"
        });
    });
    });
    </script>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <input type="hidden" id="SnippetsPostCount">
          <div class="modal-body">
            <div id="pSideNavDiv">
              <i class="fa fa-chevron-circle-left pull-left arrowPrev" aria-hidden="true" style="margin-left:-50px;font-size: 40px;color:white"></i>
              <i class="fa fa-chevron-circle-right pull-right arrowNext" aria-hidden="true" style="margin-right:-50px;font-size: 40px;color:white"></i>
            </div>
            <div id="pModalContainer">
              <div>
                <img id="snippetModalImage" style="width: 100%;" src="">
              </div>
              <div style="padding:0 30px 0px 30px;">
                <h3 id="snippetModalTitle" style="font-weight: bold"></h3>
                <div id="snippetModalContent"></div>
              </div>
            </div>
            <div id="pModalRightContainer">
              <div style="width: 100%;padding: 10px;overflow: hidden;">
                <i style="font-size: 30px;" data-dismiss="modal" aria-label="Close" class="fa fa-times-circle-o pull-right modalClose" aria-hidden="true"></i>
              </div>
              <div id="pModalAds">
                <img style="width: 100%;" src="<?php echo get_site_url(); ?>/wp-content/uploads/airtel-ad.png">
              </div>
              <div id="pNavContainer">
                <div id="pNavContainerChildOne">
                  <div id="pNavContainerNext">
                    <div id="snippetNextButton">
                      <div>Next&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </div>
                    <p id="snippetNextTitle">SoftBank keen to invest $1 Bn in Ola, founders resist</p>
                  </div>
                  <div id="pNavContainerPrev">
                    <div id="snippetPrevButton">
                      <div>Prev&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </div>
                    <p id="snippetPrevTitle">SoftBank keen to invest $1 Bn in Ola, founders resist</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>