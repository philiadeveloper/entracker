jQuery(document).ready(function() {
    jQuery('#searchicon').click(function() {
        jQuery('#jumbosearch').fadeIn();
        jQuery('#jumbosearch input').focus();
    });
    jQuery('#jumbosearch .closeicon').click(function() {
        jQuery('#jumbosearch').fadeOut();
    });
    jQuery('body').keydown(function(e) {
        if (e.keyCode == 27) {
            jQuery('#jumbosearch').fadeOut();
        }
    });
});
jQuery(window).load(function() {
    jQuery('#nivoSlider').nivoSlider({
        prevText: "<i class='fa fa-chevron-circle-left'></i>",
        nextText: "<i class='fa fa-chevron-circle-right'></i>",
    });
});
jQuery(function() {
    jQuery('.featured-section .item').hoverdir({
        hoverElem: '.featured-caption',
        speed: 500
    });
});
var width = jQuery(window).width();
if (width < 975) {
    jQuery("#pSidebar").css("height", "auto");
} else {
    jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
    var pk = 0;
    jQuery(document).scroll(function() {
        if (pk == 0) {
            if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
                pk++;
            }
        }
        //if (pk == 1) {
            if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
               // pk++;
            }
        //}
    });
}

jQuery(document).ready(function() {
    jQuery("#opensearch").on("click", function() {
        jQuery("#search-form").fadeToggle();
    });
    jQuery("#pClose").on("click", function() {
        jQuery("#search-form").fadeOut();
    });
    jQuery(window).resize(function() {
        var width = jQuery(window).width();
        var height = jQuery(window).height();
        console.log(width);
        if (width < 975) {
            jQuery("#pSidebar").css("height", "auto");
        } else {
            jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
    });
    
    jQuery('.snippetImage').click(function() {
        var count = jQuery(this).attr("data-count");
        var title = jQuery(this).attr("data-heading");
        var content = jQuery(this).attr("data-content");
        var img = jQuery(this).attr("src");
        var newCountNext = parseInt(count) + 1;
        var newCountPrev = parseInt(count) - 1;
        var next = jQuery("#snippetTitle" + newCountNext).html();
        var prev = jQuery("#snippetTitle" + newCountPrev).html();
        jQuery("#SnippetsPostCount").val(count);
        jQuery("#snippetModalTitle").html(title);
        jQuery("#snippetModalContent").html(content);
        if (next != undefined) {
            jQuery("#snippetNextButton").show();
            jQuery("#snippetNextTitle").html(next);
        } else {
            jQuery("#snippetNextButton").hide();
            jQuery("#snippetNextTitle").html("");
        }
        if (prev != undefined) {
            jQuery("#snippetPrevButton").show();
            jQuery("#snippetPrevTitle").html(prev);
        } else {
            jQuery("#snippetPrevTitle").html("");
            jQuery("#snippetPrevButton").hide();
        }
        jQuery("#snippetPrevTitle").html(prev);
        jQuery("#snippetModalImage").attr('src', img);
        jQuery('#myModal').modal('show');
    });
    jQuery('.arrowNext,#snippetNextButton').click(function() {
        var count = jQuery("#SnippetsPostCount").val();
        var newCountNext = parseInt(count) + 1;
        jQuery("#SnippetsPostCount").val(newCountNext);
        var title = jQuery("#snippetImage" + newCountNext).attr("data-heading");
        if (title == undefined) {
            jQuery("#SnippetsPostCount").val(count);
        }
        var content = jQuery("#snippetImage" + newCountNext).attr("data-content");
        var img = jQuery("#snippetImage" + newCountNext).attr("src");
        jQuery("#snippetTitle" + newCountNext).html();
        jQuery("#snippetModalTitle").html(title);
        jQuery("#snippetModalContent").html(content);
        jQuery("#snippetModalImage").attr('src', img);
        var newCountNextNew = parseInt(newCountNext) + 1;
        var newCountPrevNew = parseInt(newCountNext) - 1;
        var next = jQuery("#snippetTitle" + newCountNextNew).html();
        var prev = jQuery("#snippetTitle" + newCountPrevNew).html();
        if (next != undefined) {
            jQuery("#snippetNextButton").show();
            jQuery("#snippetNextTitle").html(next);
        } else {
            jQuery("#snippetNextButton").hide();
            jQuery("#snippetNextTitle").html("");
        }
        if (prev != undefined) {
            jQuery("#snippetPrevButton").show();
            jQuery("#snippetPrevTitle").html(prev);
        } else {
            jQuery("#snippetPrevTitle").html("");
            jQuery("#snippetPrevButton").hide();
        }
    });
    jQuery('.arrowPrev,#snippetPrevButton').click(function() {
        var count = jQuery("#SnippetsPostCount").val();
        var newCountPrev = parseInt(count) - 1;
        jQuery("#SnippetsPostCount").val(newCountPrev);
        var title = jQuery("#snippetImage" + newCountPrev).attr("data-heading");
        if (title == undefined) {
            jQuery("#SnippetsPostCount").val(count);
        }
        var content = jQuery("#snippetImage" + newCountPrev).attr("data-content");
        var img = jQuery("#snippetImage" + newCountPrev).attr("src");
        jQuery("#snippetTitle" + newCountPrev).html();
        jQuery("#snippetModalTitle").html(title);
        jQuery("#snippetModalContent").html(content);
        jQuery("#snippetModalImage").attr('src', img);
        var newCountNextNew = parseInt(newCountPrev) + 1;
        var newCountPrevNew = parseInt(newCountPrev) - 1;
        var next = jQuery("#snippetTitle" + newCountNextNew).html();
        var prev = jQuery("#snippetTitle" + newCountPrevNew).html();
        if (next != undefined) {
            jQuery("#snippetNextButton").show();
            jQuery("#snippetNextTitle").html(next);
        } else {
            jQuery("#snippetNextButton").hide();
            jQuery("#snippetNextTitle").html("");
        }
        if (prev != undefined) {
            jQuery("#snippetPrevButton").show();
            jQuery("#snippetPrevTitle").html(prev);
        } else {
            jQuery("#snippetPrevTitle").html("");
            jQuery("#snippetPrevButton").hide();
        }
    });
    jQuery('.owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        dots: true,
        items: 1,
        navText: ["<i class='fa fa-chevron-left' style='font-size:30px;color: white;' aria-hidden='true'></i>", "<i class='fa fa-chevron-right' style='font-size:30px;color: white;' aria-hidden='true'></i>"]
    });

});
function fetch(){
    var count = jQuery(".countClass").last().val();
    jQuery("#LoadMoreButton").hide();
    jQuery("#LoadMoreLoading").html('<img src="http://localhost/development/entracker/wp-content/uploads/loader.gif">');
    if(jQuery('.endPost').length==0) {
    jQuery.ajax({
      type:"POST",
      url: '/development/entracker/wp-admin/admin-ajax.php',
      data: {
          action: "my_action",
          data: count
      },
      success:function(response){
        jQuery("#LoadMoreButton").show();
        jQuery("#LoadMoreLoading").html("");
        jQuery("#main").append(response);
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
      },
      error: function(errorThrown){
          jQuery("#main").append(errorThrown);
      } 

    });
    }else{
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
       jQuery("#LoadMoreButton").show();
       jQuery("#LoadMoreLoading").html(""); 
    }
}
function fetch2(){
    var count = jQuery(".SideBarcountClass").last().val();
    jQuery("#LoadMoreButton2").hide();
    jQuery("#LoadMoreLoading2").html('<img src="http://localhost/development/entracker/wp-content/uploads/loader.gif">');
    if(jQuery('.endPost2').length==0) {
    jQuery.ajax({
      type:"POST",
      url: '/development/entracker/wp-admin/admin-ajax.php',
      data: {
          action: "my_action2",
          data: count
      },
      success:function(response){
        jQuery("#LoadMoreButton2").show();
        jQuery("#LoadMoreLoading2").html("");
        jQuery("#SideBarAppend").append(response);
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
      },
      error: function(errorThrown){
          jQuery("#SideBarAppend").append(errorThrown);
      } 

    });
    }else{
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
       jQuery("#LoadMoreButton2").show();
       jQuery("#LoadMoreLoading2").html(""); 
    }
}
function fetch3(){
    var count = jQuery(".countClass").last().val();
    var catID = jQuery(".categoryID").last().val();
    jQuery("#LoadMoreButton3").hide();
    jQuery("#LoadMoreLoading3").html('<img src="http://localhost/development/entracker/wp-content/uploads/loader.gif">');
    if(jQuery('.endPost3').length==0) {
    jQuery.ajax({
      type:"POST",
      url: '/development/entracker/wp-admin/admin-ajax.php',
      data: {
          action: "my_action3",
          data: count,
          catID: catID,
      },
      success:function(response){
        jQuery("#LoadMoreButton3").show();
        jQuery("#LoadMoreLoading3").html("");
        jQuery("#main").append(response);
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
      },
      error: function(errorThrown){
          jQuery("#main").append(errorThrown);
      } 

    });
    }else{
        if (jQuery("#pMain").height() != jQuery("#pSidebar").height()) {
                jQuery("#pSidebar").css("height", jQuery("#pMain").height() + "px");
        }
       jQuery("#LoadMoreButton3").show();
       jQuery("#LoadMoreLoading3").html(""); 
    }
}