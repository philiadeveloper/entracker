<?php
/**
* murray functions and definitions
*
* @package murray
*/
if ( ! function_exists( 'murray_setup' ) ) :
/**
* Sets up theme defaults and registers support for various WordPress features.
*
* Note that this function is hooked into the after_setup_theme hook, which
* runs before the init hook. The init hook is too late for some features, such
* as indicating support for post thumbnails.
*/
function murray_setup() {
	/*
	* Make theme available for translation.
	* Translations can be filed in the /languages/ directory.
	* If you're building a theme based on murray, use a find and replace
	* to change 'murray' to the name of your theme in all the template files
	*/
	load_theme_textdomain( 'murray', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	* Let WordPress manage the document title.
	* By adding theme support, we declare that this theme does not use a
	* hard-coded <title> tag in the document head, and expect WordPress to
	* provide it for us.
	*
	*/
	add_theme_support( 'title-tag' );
//Custom Logo
add_theme_support( 'custom-logo' );
	/**
	* Set the content width based on the theme's design and stylesheet.
	*/
	if ( ! isset( $content_width ) ) {
		$content_width = 640; /* pixels */
	}
	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	*/
	add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'murray' ),
	) );
	/*
	* Switch default core markup for search form, comment form, and comments
	* to output valid HTML5.
	*/
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
	/*
	* Enable support for Post Formats.
	* See http://codex.wordpress.org/Post_Formats
	*/
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'murray_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	//RT Slider Support
	add_theme_support( 'rt-slider' );
	add_image_size('murray-pop-thumb',542, 340, true );
	add_image_size('murray-featpost-thumb',542, 442, true );
	add_image_size('murray-thumb',870, 430, true );
}
endif; // murray_setup
add_action( 'after_setup_theme', 'murray_setup' );
/**
* Register widget area.
*
* @link http://codex.wordpress.org/Function_Reference/register_sidebar
*/
function murray_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'murray' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-font">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer 1', 'murray' ), /* Primary Sidebar for Everywhere else */
		'id'            => 'footer-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-font">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer 2', 'murray' ), /* Primary Sidebar for Everywhere else */
		'id'            => 'footer-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-font">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'murray' ), /* Primary Sidebar for Everywhere else */
		'id'            => 'footer-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-font">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'murray_widgets_init' );
/**
* Enqueue scripts and styles.
*/
function murray_scripts() {
	wp_enqueue_style( 'murray-style', get_stylesheet_uri() );
	wp_enqueue_style('murray-title-font', '//fonts.googleapis.com/css?family='.str_replace(" ", "+", esc_html(get_theme_mod('murray_title_font', 'Fjalla One') ).':100,300,400,700' ));
	wp_enqueue_style('murray-body-font', '//fonts.googleapis.com/css?family='.str_replace(" ", "+", esc_html(get_theme_mod('murray_body_font', 'Source Sans Pro') ).':100,300,400,700' ));
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'nivo-slider', get_template_directory_uri() . '/assets/ext-css/nivo-slider.css' );
	wp_enqueue_style( 'nivo-skin', get_template_directory_uri() . '/assets/ext-css/nivo-default/default.css' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'hover-style', get_template_directory_uri() . '/assets/ext-css/hover.min.css' );
	//wp_enqueue_style( 'murray-main-theme-style', get_template_directory_uri() . '/assets/css/main.css' );
wp_enqueue_style( 'murray-main-theme-style', get_template_directory_uri() . '/assets/theme_styles/css/default.css');
	wp_enqueue_script( 'murray-external', get_template_directory_uri() . '/js/external.js', array('jquery'), '20120206', true );
	wp_enqueue_script( 'murray-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'murray-custom-js', get_template_directory_uri() . '/js/custom.js' );
}
add_action( 'wp_enqueue_scripts', 'murray_scripts' );
//Backwards Compatibility FUnction
	function murray_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
function murray_has_logo() {
	if (function_exists( 'has_custom_logo')) {
		if ( has_custom_logo() ) {
			return true;
		}
	} else {
		return false;
	}
}
//Function to Trim Excerpt Length & more..
function murray_excerpt_length( $length ) {
	return 23;
}
add_filter( 'excerpt_length', 'murray_excerpt_length', 999 );
function murray_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'murray_excerpt_more' );
/**
* Enqueue Scripts for Admin
*/
function murray_custom_wp_admin_style() {
wp_enqueue_style( 'murray-admin_css', get_template_directory_uri() . '/assets/css/admin.css' );
wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css' );
}
add_action( 'customize_controls_print_styles', 'murray_custom_wp_admin_style' );
function custom_excerpt_length( $length ) {
return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
/**
* Include the Custom Functions of the Theme.
*/
require get_template_directory() . '/framework/theme-functions.php';
/**
* Implement the Custom CSS Mods.
*/
require get_template_directory() . '/inc/css-mods.php';
/**
* Custom template tags for this theme.
*/
require get_template_directory() . '/inc/template-tags.php';
/**
* Custom functions that act independently of the theme templates.
*/
require get_template_directory() . '/inc/extras.php';
/**
* Customizer additions.
*/
require get_template_directory() . '/framework/customizer/_init.php';
/**
* Load Jetpack compatibility file.
*/
require get_template_directory() . '/inc/jetpack.php';
/**
* Include TGM Plugin.
*/
require get_template_directory() . '/framework/tgmpa.php';
/*AJAX*/
add_action( 'wp_ajax_my_action', 'data_fetch' );
add_action( 'wp_ajax_nopriv_my_action', 'data_fetch' );
function data_fetch(){
$args = array( 'posts_per_page' => 6,'cat'=>-11, 'post_status' => 'publish','offset'=> $_POST['data']);
$the_query = new WP_Query( $args ); ?>
<?php if ( $the_query->have_posts() ) : ?>
<?php  $loop_c = 0;$countCenterBox=1;$postCount=1; ?>
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
	<div class="featured-thumb col-md-12">
		<?php if (has_post_thumbnail()) : ?>
		<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
			<?php the_post_thumbnail('murray-pop-thumb',array(  'alt' => trim(strip_tags( $post->post_title )))); ?>
		</a>
		<?php else: ?>
		<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><img alt="<?php the_title() ?>" src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
		<?php endif; ?>
	</div>
	<div class="out-thumb col-md-12">
		<header class="entry-header">
			<h3 class="entry-title title-font"><a href="<?php the_permalink(); ?>" rel="bookmark" style="font-weight:bold;">
				<?php the_title(); ?>
			</a></h3>
			<div class="postedon">
				<?php murray_posted_on(); ?>
			</div>
		</header>
	</div>
	<?php $offset = ++$_POST['data'];?>
	<input type="hidden" class="countClass" value="<?php echo $offset;?>">
</article>
<?php $loop_c++;endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<input type="hidden" class="endPost" value="1">
<div class="clearfix"></div>
<center><p style="width: 100%;padding:10px;background: #A12828;color:white;border-radius: 5px;"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p></center>
<?php endif;
die();
}
add_action( 'wp_ajax_my_action2', 'data_fetch2' );
add_action( 'wp_ajax_nopriv_my_action2', 'data_fetch2' );
function data_fetch2(){
		global $post;
		$number = 1;
		$args = array( 'posts_per_page' => $number,'cat' => 11,'offset'=>$_POST['data']);
		$myposts = get_posts( $args );
		if ( $myposts ) :
$i=$_POST['data'];$a=1;foreach ( $myposts as $recent_post ) : setup_postdata( $post ); ?>
<?php
$post_title = get_the_title( $recent_post->ID );
$title      = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
?>
<li>
	<img data-count="<?php echo $i;?>" data-id="<?php echo $recent_post->ID;?>" id="snippetImage<?php echo $i;?>" data-heading="<?php echo $title;?>" data-content="<?php echo get_post_field('post_content', $recent_post->ID);?>" class="snippetImage" src="<?php echo get_the_post_thumbnail_url($recent_post->ID,'full');?>" style="margin-bottom:10px;">
	<br>
	<span style="font-weight:bold;color:black;" id="snippetTitle<?php echo $i;?>"><?php echo $title;?></span>
	<?php $offset = ++$_POST['data'];?>
	<input type="hidden" class="SideBarcountClass" value="<?php echo $offset;?>">
</li>
<?php $a++;$i++;endforeach; ?>
<?php else : ?>
<input type="hidden" class="endPost2" value="1">
<div class="clearfix"></div>
<center><p style="width: 100%;padding:10px;background: #A12828;color:white;border-radius: 5px;"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p></center>
<?php endif;
die();
}
add_action( 'wp_ajax_my_action3', 'data_fetch3' );
add_action( 'wp_ajax_nopriv_my_action3', 'data_fetch3' );
function data_fetch3(){
$args = array( 'posts_per_page' => 6,'cat'=>$_POST['catID'],-11,'post_status' => 'publish','offset'=> $_POST['data']);
$the_query = new WP_Query( $args ); ?>
<?php if ( $the_query->have_posts() ) : ?>
<?php  $loop_c = 0;$countCenterBox=1;$postCount=1; ?>
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
	<div class="featured-thumb col-md-12">
		<?php if (has_post_thumbnail()) : ?>
		<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
			<?php the_post_thumbnail('murray-pop-thumb',array(  'alt' => trim(strip_tags( $post->post_title )))); ?>
		</a>
		<?php else: ?>
		<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><img alt="<?php the_title() ?>" src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
		<?php endif; ?>
	</div>
	<div class="out-thumb col-md-12">
		<header class="entry-header">
			<h3 class="entry-title title-font"><a href="<?php the_permalink(); ?>" rel="bookmark" style="font-weight:bold;">
				<?php the_title(); ?>
			</a></h3>
			<div class="postedon">
				<?php murray_posted_on(); ?>
			</div>
		</header>
	</div>
	<?php $offset = ++$_POST['data'];?>
	<input type="hidden" class="countClass" value="<?php echo $offset;?>">
</article>
<?php $loop_c++;endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<input type="hidden" class="endPost3" value="1">
<div class="clearfix"></div>
<center><p style="width: 100%;padding:10px;background: #A12828;color:white;border-radius: 5px;"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p></center>
<?php endif;
die();
}
?>