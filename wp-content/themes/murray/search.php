<?php
/**
* The template for displaying search results pages.
*
* @package murray
*/
get_header(); ?>
<?php
$settings = $wpdb->get_results("SELECT * FROM entracker_config WHERE id=1",ARRAY_A);
?>
<div class="row" id="pMain">
	<div id="primary" class="content-area col-md-9">
		<div style="height: 100vh;width: 100%;overflow: hidden;">
			<div style="width: 100%;height: 100%;overflow-y: scroll;padding-right: 17px;box-sizing: content-box;">
				<div <?php post_class("col-md-12");?> id="searchParent">
					<?php get_template_part( 'modules/search/search'); ?>
				</div>
				<main id="main" class="site-main" role="main">
					<?php if ( have_posts() ) : ?>
					<?php /* Start the Loop */ ?>
					<?php $limit = $settings[0]['top_slider_limit'];?>
					<?php $type = $settings[0]['top_slider_type'];?>
					<?php $repeat = $settings[0]['repeat_middle_adv'];?>
					<?php $front_page_post_count = $settings[0]['front_page_post_count'];?>
					<?php $pAdsStatus = $settings[0]['pAdsStatus'];?>
					<?php if(($pAdsStatus == 1) AND $settings[0]['add_banner_code_body1']!=''){?>
					<?php echo $settings[0]['add_banner_code_body1'];?>
					<?php } ?>
					<br>
					<?php  $loop_c = 0;$count=1;$countCenterBox=1; ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php
										$loop_c++;
										if ( ($loop_c == 1) && is_sticky() )
										continue;
					?>
					<?php if(($pAdsStatus == 1) AND $count==$repeat){ ?>
					<article <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
						<div class="featured-thumb col-md-12">
							<?php if($settings[0]['add_banner_code_body2']!=''){?>
							<?php echo $settings[0]['add_banner_code_body2'];$count=1;?>
							<?php } ?>
						</div>
					</article>
					<?php } ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 grid grid_2_column'); ?>>
						<div class="featured-thumb col-md-12">
							<?php if (has_post_thumbnail()) : ?>
							<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
								<?php the_post_thumbnail('murray-pop-thumb',array(  'alt' => trim(strip_tags( $post->post_title )))); ?>
							</a>
							<?php else: ?>
							<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><img alt="<?php the_title() ?>" src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
							<?php endif; ?>
						</div>
						<div class="out-thumb col-md-12">
							<header class="entry-header">
								<h3 class="entry-title title-font"><a href="<?php the_permalink(); ?>" rel="bookmark" style="font-weight:bold;">
									<?php the_title(); ?>
								</a></h3>
								<div class="postedon">
									<?php murray_posted_on(); ?>
								</div>
							</header>
						</div>
					</article>
					<?php $countCenterBox++;$count++;endwhile; ?>
					<div class="clearfix"></div>
					<?php else : ?>
					<?php get_template_part( 'modules/content/content', 'none' ); ?>
					<?php endif; ?>
					</main><!-- #main -->
				</div>
			</div>
		</div>
		<?php get_template_part( 'modules/content/content', 'sidebar' ); ?>
	</div>
	<?php get_footer(); ?>