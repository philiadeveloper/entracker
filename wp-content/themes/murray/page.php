<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package murray
 */

get_header(); ?>
<div class="row" id="pMain">
	<div id="primary-mono" class="content-area col-md-9 page">
		<div style="height: 100vh;width: 100%;overflow: hidden;">
<div style="width: 100%;height: 100%;overflow-y: scroll;padding-right: 17px;box-sizing: content-box;">
			<div <?php post_class("sticky-image col-md-12");?>>
				<?php get_template_part( 'modules/search/search'); ?>
			</div>
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'modules/content/content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div>
	</div>
	</div>
	<?php get_template_part( 'modules/content/content', 'sidebar' ); ?>
	</div>


<?php get_footer(); ?>
