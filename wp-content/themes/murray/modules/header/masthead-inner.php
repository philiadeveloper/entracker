<div class="site-branding">
    <?php if ( has_custom_logo() ) : ?>
            <div id="site-logo">
                <?php the_custom_logo(); ?>
            </div>
    <?php endif; ?>
    <div id="text-title-desc">
        <h1 class="site-title title-font"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
    </div>
</div>
<div style="display: inline-block;width: 80%;">
 <?php get_template_part('modules/navigation/menu-single'); ?>

</div>
<div style="display: inline-block;width: auto;position: absolute;margin-top:37px;margin-left: 40px;">
<i class="fa fa-search" aria-hidden="true" style="color: white;font-size: 20px;" id="opensearch"></i>

</div>

