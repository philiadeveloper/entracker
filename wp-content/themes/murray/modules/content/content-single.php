<?php
/**
 * @package murray
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div id="featured-image" style="width: 100%;position: relative;">
        <?php the_post_thumbnail('full'); ?>
        <div style="width: 100%;"><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?></div>
    </div>

    <header class="entry-header">
        <?php the_title( '<h1 class="entry-title title-font">', '</h1>' ); ?>


        <!-- <div class="entry-meta">
            <?php murray_posted_on(); ?>
        </div> -->
    </header>

    <div class="entry-content">
        <?php the_content(); ?>
        <div style="width: 100%;"><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?></div>
        <?php
        wp_link_pages( array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'murray' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php murray_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
