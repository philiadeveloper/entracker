
<center><form id="search-form" style="<?php echo (isset($_GET['s']))?'display: block':'display: none';?>" role="search" method="get" class="search-form" action="<?php echo esc_url(home_url( '/' )); ?>">
	<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'murray' ) ?></span>
	<input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search for anything.', 'placeholder', 'murray' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'murray' ) ?>" />
	<input type="submit" value="Search" id="pSubmit">
	<div id="pClose"><i class="fa fa-times-circle-o" aria-hidden="true"></i></div>
</form>
</center>
