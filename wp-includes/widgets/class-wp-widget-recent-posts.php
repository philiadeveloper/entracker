<?php
/**
* Widget API: WP_Widget_Recent_Posts class
*
* @package WordPress
* @subpackage Widgets
* @since 4.4.0
*/
/**
* Core class used to implement a Recent Posts widget.
*
* @since 2.8.0
*
* @see WP_Widget
*/
class WP_Widget_Recent_Posts extends WP_Widget {
	/**
	* Sets up a new Recent Posts widget instance.
	*
	* @since 2.8.0
	*/
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_recent_entries',
			'description' => __( 'Your site&#8217;s most recent Posts.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'recent-posts', __( 'Recent Posts' ), $widget_ops );
		$this->alt_option_name = 'widget_recent_entries';
	}
	/**
	* Outputs the content for the current Recent Posts widget instance.
	*
	* @since 2.8.0
	*
	* @param array $args     Display arguments including 'before_title', 'after_title',
	*                        'before_widget', and 'after_widget'.
	* @param array $instance Settings for the current Recent Posts widget instance.
	*/
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );
		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
		/**
		* Filters the arguments for the Recent Posts widget.
		*
		* @since 3.4.0
		* @since 4.9.0 Added the `$instance` parameter.
		*
		* @see WP_Query::get_posts()
		*
		* @param array $args     An array of arguments used to retrieve the recent posts.
		* @param array $instance Array of settings for the current widget.
		*/
		global $wpdb;
		$settings = $wpdb->get_results("SELECT * FROM entracker_config WHERE id=1",ARRAY_A);
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => ($settings[0]['snippets_box_post_count']!='')?$settings[0]['snippets_box_post_count']:'10',
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			'category' => $settings[0]['snippets_box_category'],
		), $instance ) );
		if ( ! $r->have_posts() ) {
			return;
		}
?>
<?php echo $args['before_widget']; ?>
<?php
if ( $title ) {
	echo $args['before_title'] . $title . $args['after_title'];
}
?>
<?php
		global $post;
		$number = ($settings[0]['snippets_box_post_count']!='')?$settings[0]['snippets_box_post_count']:10;
		$args = array( 'posts_per_page' => $number,'category' => $settings[0]['snippets_box_category']);
		$myposts = get_posts( $args );
?>
<div style="height: 100vh;width: 100%;overflow: hidden;">
<div style="width: 100%;height: 100%;overflow-y: scroll;padding-right: 17px;box-sizing: content-box;">
<ul id="SideBarAppend">
  <?php $i=0;$a=1;$k=1;foreach ( $myposts as $recent_post ) : setup_postdata( $post ); ?>
  <?php
	$post_title = get_the_title( $recent_post->ID );
	$title      = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
	?>
  <li>
    <?php if($a==$settings[0]['snippets_ads_repeat']){ $a = 1;?>
    <?php echo $settings[0]['add_banner_code_snippet'];?>
    <?php } ?>
    <img data-count="<?php echo $i;?>" data-id="<?php echo $recent_post->ID;?>" id="snippetImage<?php echo $i;?>" data-heading="<?php echo $title;?>" data-content="<?php echo get_post_field('post_content', $recent_post->ID);?>" class="snippetImage" src="<?php echo get_the_post_thumbnail_url($recent_post->ID,'full');?>" style="margin-bottom:10px;"> <br>
    <span style="font-weight:bold;color:black;" id="snippetTitle<?php echo $i;?>"><?php echo $title;?></span>
    <input type="hidden" class="SideBarcountClass" value="<?php echo $k;?>">
  </li>
  <?php $a++;$k++;$i++;endforeach; ?>
</ul>

<div class="clearfix"></div>
<div class="col-md-12" style="margin-top: 10px;">
  <center>
    <div id="LoadMoreLoading2"></div>
    <div id="LoadMoreButton2" onclick="fetch2()" style="
		margin-top: 50px;
		margin-bottom: 50px;
		background: #ffffff;
		width: 200px;
		text-align: center;
		padding: 10px 30px;
		cursor: pointer;"> <span style="color: #9A9A9A">Load More</span> </div>
  </center>
</div>
</div>
</div>
<?php
	echo $args['after_widget'];
	}
	/**
	* Handles updating the settings for the current Recent Posts widget instance.
	*
	* @since 2.8.0
	*
	* @param array $new_instance New settings for this instance as input by the user via
	*                            WP_Widget::form().
	* @param array $old_instance Old settings for this instance.
	* @return array Updated settings to save.
	*/
	public function update( $new_instance, $old_instance ) {
	$instance = $old_instance;
	$instance['title'] = sanitize_text_field( $new_instance['title'] );
	$instance['number'] = (int) $new_instance['number'];
	$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
	return $instance;
	}
	/**
	* Outputs the settings form for the Recent Posts widget.
	*
	* @since 2.8.0
	*
	* @param array $instance Current settings.
	*/
	public function form( $instance ) {
	$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
	$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
	$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
	?>
<p>
  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
    <?php _e( 'Title:' ); ?>
  </label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'number' ); ?>">
    <?php _e( 'Number of posts to show:' ); ?>
  </label>
  <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" />
</p>
<p>
  <input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
  <label for="<?php echo $this->get_field_id( 'show_date' ); ?>">
    <?php _e( 'Display post date?' ); ?>
  </label>
</p>
<?php
		}
	}
